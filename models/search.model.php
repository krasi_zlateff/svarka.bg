<?php 
	class searchModel extends Models{
		function getUsersByWord($s_word, $limit = 12){
			$s_word = mysql_real_escape_string($s_word);
			return $this->pagingQuery("
									SELECT u.id, u.email, u.username, u.name, u.family, u.avatar, u.birthdate, u.city_id, u.sex, u.date_entered, u.IP, u.usertype, u.icq, u.skype, u.msn, u.yahoo, u.google_talk, u.for_me, (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(u.birthdate)), '%Y')+0) AS age, c.name as city_name, MONTH(u.birthdate) as b_month, DAY(u.birthdate) as b_day, YEAR(u.birthdate) as b_year 
									FROM users u 
									LEFT JOIN cities c ON c.id = u.city_id
									WHERE u.username LIKE '%{$s_word}%'
									", $limit);
		}
	}
?>