<?
class usersModel extends Models {


	function getUsers($method = "getAll", $limit = 0){
		$sql = "SELECT u.id, u.email, u.username, CONCAT(u.name, ' ', u.family) as name, u.birthdate, c.name as city_name, u.sex, u.date_entered, u.IP, u.usertype, u.avatar
				FROM users u
				LEFT JOIN cities c ON u.city_id = c.id
				ORDER BY id DESC
				";
		if($method == "pagingQuery")
			$users =  $this->pagingQuery($sql, $limit);
		else
			$users = $this->db->getAll($sql);

		if (isset($_SESSION['UserID']))
		{
			$friends = $this->getFriendsIDs();
			foreach ($users as $k => $v)
			{
				if (in_array($v['id'], $friends))
				{
					$users[$k]['fr'] = 1;
				}
			}
		}

		return $users;
	}

	function sendNewPassword($email)
	{
		$pass = substr(md5(time()), 0, 5);
		$this->db->Execute('UPDATE users SET password="'.md5($pass).'" WHERE email="'.$email.'"');

		$headers  = "MIME-Version: 1.0\n" ;
		$headers .= "Content-Type: text/html; charset=\"utf-8\"\n";
		$headers .= 'From: noreply@sv.tara-soft.net' . "\r\n" .
						'X-Mailer: PHP/' . phpversion();
		$subject  = "Svarka - zabravena parola";
		$text 	  = "<html><body>Вашата нова парола е - ".$pass."<br/>Vashata nova paorla e - ".$pass."</body></html>";

		mail($email, $subject, $text, $headers);

	}

	function edit($arr, $id) {
		if($arr['year'] != "" && $arr['month'] != "" && $arr['day'] != ""){
			$arr['birthdate'] = $arr['year']."-".$arr['month']."-".$arr['day'];
		}

		$this->update("users", $arr, "id = '{$id}'", false, true, true);

	}

	function username($user, $withoutId = 0) {
		if($withoutId > 0) $sqlPart = "AND id != '{$withoutId}'";
		$user = addslashes($user);
		$user = strtolower($user);
		return $this->db->getOne("SELECT COUNT(id) FROM users WHERE username='{$user}' $sqlPart");
	}

	function email($mail, $withoutId = 0) {
		if($withoutId > 0) $sqlPart = "AND id != '{$withoutId}'";
		$mail = addslashes($mail);
		return $this->db->getOne("SELECT COUNT(id) FROM users WHERE email='{$mail}' $sqlPart");
	}

	function getAvatar($id){
		return $this->db->getOne("SELECT avatar FROM users WHERE id = '{$id}'");
	}

	function deleteUser($user_id){

		$this->db->Execute("DELETE FROM users WHERE id = '{$user_id}'");

		$this->db->Execute("DELETE FROM tourn_members WHERE user_id = '{$user_id}'");
		//prodalji kato se uslojnqt ne6tata i ima novi ne6ta pri4isleni kam potrebitel

	}

	function getUsersById($id){
		return $this->db->getRow("SELECT id, username, email, usertype, avatar FROM users WHERE id = '{$id}'");
	}

	function getUsersForApproveTourn($method = "getAll", $limit = 0){
		$sql = "SELECT tm.id, u.username, CONCAT(u.name, ' ', u.family) as name, t.name as tourn_name, u.id as user_id, u.avatar
									FROM tourn_members tm
									INNER JOIN users u ON u.id = tm.user_id
									INNER JOIN tournaments t ON t.id = tm.tourn_id
									WHERE tm.status = 0";
		if($method == "pagingQuery") return $this->pagingQuery($sql, $limit);

		return $this->db->getAll($sql);
	}

	function add_user($arr) {
		global $now;
		$arr['date_entered'] = $now;
		$arr['last_login'] = $now;
		$arr['online'] = 1;
		$arr['password'] = md5(mysql_escape_string($arr['password']));
		$arr['username'] = mysql_escape_string($arr['username']);
		$arr['email'] = mysql_escape_string($arr['email']);
		$arr['IP'] = Controllers::GetIP();
		$this->insert("users", $arr, false, true, true);
		return $this->insertID();
	}

	function getUserInfoSimple($user_id){
		return $this->db->getRow("SELECT id, username, email FROM users WHERE id = '{$user_id}'");
	}

	function setLogout($id) {
		$this->db->Execute("UPDATE users SET online = 0, md5_login = '' WHERE id = '{$id}'");
	}

	function getFullUserById($id){
		return $this->db->getRow("SELECT u.id, u.email, u.username, u.name, u.family, u.avatar, u.birthdate, u.city_id, u.sex, u.date_entered, u.IP, u.usertype, u.icq, u.skype, u.msn, u.yahoo, u.google_talk, u.for_me, (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(u.birthdate)), '%Y')+0) AS age, c.name as city_name, MONTH(u.birthdate) as b_month, DAY(u.birthdate) as b_day, YEAR(u.birthdate) as b_year
									FROM users u
									LEFT JOIN cities c ON c.id = u.city_id
									WHERE u.id = '{$id}'");
	}

	function isFriend($id)
	{
		$ct = $this->db->getOne('SELECT id FROM friends WHERE sender_id='.$_SESSION['UserID'].' AND reciever_id='.$id.' AND approved=1');

		if (isset($ct) && $ct > 0)
			return true;

		return false;
	}

	function addFriend($id)
	{
		$ct = $this->db->getOne('SELECT id FROM friends WHERE sender_id='.$_SESSION['UserID'].' AND reciever_id='.$id);

		if (isset($ct) && $ct > 0);
		else
		{
			$this->insert('friends', array('sender_id' => $_SESSION['UserID'], 'reciever_id' => $id), true, true, true);
			$this->sendFriendRequestMessage($_SESSION['UserID'], $id, mysql_insert_id());
		}
	}

	function delFriend($id)
	{
		$this->db->Execute('DELETE FROM friends WHERE (sender_id='.$_SESSION['UserID'].' AND reciever_id='.$id.') OR (sender_id='.$id.' AND reciever_id='.$_SESSION['UserID'].')');
	}

	function sendFriendRequestMessage($sender, $reciever, $id)
	{
		$senderName = $this->getUsername($sender);

		$msg = array(
			'date_sent' => date('Y-m-d H:i:s'),
			'sender_id' => 0,
			'reciever_id' => $reciever,
			'title' => 'Покана за приятелство',
			'text' => 'Имате покана за приятелство от <a href="/users/profile/'.$sender.'" title="">'.$senderName.'</a>. Можете да я потвърдите <a href="/users/confirm/'.$id.'" title="">тук</a>.',
		);

		$this->insert('messages', $msg, true, true, true);
	}

	function getFriends()
	{
		$friends = $this->db->getAll('SELECT * FROM friends WHERE approved=1 AND (sender_id='.$_SESSION['UserID'].' OR reciever_id='.$_SESSION['UserID'].')');

		foreach ($friends as $k => $v)
		{
			if ($v['sender_id'] != $_SESSION['UserID'])
				$friends[$k] = $this->getFullUserById($v['sender_id']);
			else
				$friends[$k] = $this->getFullUserById($v['reciever_id']);
		}

		return $friends;
	}

	function getFriendsIDs()
	{
		$friends = $this->db->getAll('SELECT sender_id, reciever_id FROM friends WHERE approved=1 AND (sender_id='.$_SESSION['UserID'].' OR reciever_id='.$_SESSION['UserID'].')');
		$fr = array();

		foreach ($friends as $k => $v)
		{
			$fr[] = ($v['reciever_id'] == $_SESSION['UserID'])?$v['sender_id']:$v['reciever_id'];
		}

		return $fr;
	}

	function confirmFriendRequest($id)
	{
		$this->db->Execute('UPDATE friends SET approved=1 WHERE id='.$id.' AND reciever_id='.$_SESSION['UserID']);
	}

	function setPassword( $to_hash, $userid ) {
		$hash = md5($to_hash);
		$this->db->Execute("UPDATE users SET password = '{$hash}' WHERE id = '{$userid}'");
	}

	function login($arr) {
		$arr['password'] = md5(mysql_escape_string($arr['password']));
		$arr['email'] = mysql_escape_string($arr['email']);
		return $this->db->getRow("SELECT id, username, email, usertype FROM users WHERE email = '{$arr['email']}' AND password = '{$arr['password']}'");
	}

	function checkMail($email){
		return $this->db->getOne("SELECT id FROM users WHERE email = '{$email}'");
	}

	function setLostPassCode($to, $code) {
		$this->db->Execute("UPDATE users SET activate = '{$code}' WHERE email = '{$to}' ");
	}

	function chekLostPasscode($code){
		$code = mysql_escape_string($code);
		return $this->db->getOne("SELECT id FROM users WHERE activate = '{$code}'");
	}

	function setNewPass($new, $user_id) {
		$this->db->Execute("UPDATE users SET password = '{$new}', activate = '' WHERE id = '{$user_id}'");
	}

	function getEmailById($user_id){
		return $this->db->getOne("SELECT email FROM users WHERE id = '{$user_id}'");
	}

	function setOnline($id) {
		$this->db->Execute("UPDATE users SET last_login = NOW(), online = 1 WHERE id = '{$id}'");
	}

	function SetSaveMe($code){
		$this->db->Execute("UPDATE users SET md5_login = '$code' WHERE id = '{$_SESSION['UserID']}'");
	}

	function checkPassword ( $to_hash, $userid ) {
		$hash = md5($to_hash);
		$ret = $this->db->getOne("SELECT id FROM users WHERE password = '{$hash}' AND id ='{$userid}'");
		if ( $ret != "" ) return true; else return false;
	}

	function insertQuestion($arr){//topsnimki
			global $now;
			$arr['date_entered'] = $now;
			$arr['IP'] = Controllers::GetIP();
			if($_SESSION['UserID'] > 0) $arr['user_id'] = $_SESSION['UserID'];

			$this->insert("contacts", $arr, false, true,true );
	}

	function searchFriends($search)
	{
		$frst = array();
		$frs = $this->db->getAll('SELECT u.username, u.id FROM friends f LEFT JOIN users u ON (u.id = reciever_id OR u.id = sender_id) WHERE u.username LIKE "%'.$search.'%" AND (f.reciever_id = '.$_SESSION['UserID'].' OR f.sender_id = '.$_SESSION['UserID'].') AND f.approved = 1 AND u.id != '.$_SESSION['UserID']);

		return $frs;
	}

	function searchUsers($search)
	{
		$frs = array();
		if ($search != '')
			$frs = $this->db->getAll('SELECT username, id FROM users WHERE username LIKE "%'.$search.'%" AND id != '.$_SESSION['UserID']);

		return $frs;
	}
}

?>