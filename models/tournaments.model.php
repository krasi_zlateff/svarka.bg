<?php 
	class tournamentsModel extends Models{
		
		function insertTournament($arr)
		{
			global $now;
			$arr['date_entered'] = $now;
			$this->insert("tournaments", $arr, false, true ,true);
			return $this->insertID();
		}
		
		function editTournament($arr, $tourn_id)
		{
			$this->update("tournaments", $arr, "id = '{$tourn_id}'", false, true, true);
		}
		
		function getTournamentById($tourn_id)
		{
			return $this->db->getRow("SELECT id, name, description, date_entered, start_date, end_date FROM tournaments WHERE id = '{$tourn_id}'");
		}
		
		function getAllTournaments($method = "getAssoc", $limit = 0)
		{
			if($method == "pagingQuery") return $this->$method("SELECT id as id2, id, name, description, date_entered, start_date, end_date FROM tournaments ORDER BY name ASC", $limit);
			return $this->db->$method("SELECT id as id2, id, name, description, date_entered, start_date, end_date FROM tournaments ORDER BY name ASC");
		}
		
		function getAllTournamentsFull($method = "getAssoc", $limit = 0)
		{
			if($method == "pagingQuery") 
				$tourn = $this->$method("SELECT id as id2, id, name, description, date_entered, start_date, end_date FROM tournaments ORDER BY name ASC", $limit);
			else
				$tourn = $this->db->$method("SELECT id as id2, id, name, description, date_entered, start_date, end_date FROM tournaments ORDER BY name ASC");
				
			foreach ($tourn as $k => $v)
			{
				$tourn[$k]['members'] = $this->countTournamentMembers($v['id']);
				$tourn[$k]['member'] = $this->isMember($_SESSION['UserID'], $v['id']);
			}
			
			return $tourn;
		}
		
		function isMember($user, $tourn)
		{
			$ct = $this->db->getOne('SELECT status FROM tourn_members WHERE user_id='.$user.' AND tourn_id='.$tourn);

			if (!isset($ct) || $ct == null)
			{
				return -1;
			}
			
			 return $ct;
		}
		
		function countTournamentMembers($id)
		{
			return $this->db->getOne('SELECT COUNT(id) FROM tourn_members WHERE tourn_id='.$id.' AND status=1');
		}
		
		function setApprove($ids)
		{
			$this->db->Execute("UPDATE tourn_members SET status = 1 WHERE id IN ($ids)");
		}
		
		function ForceInTournament($tourn_id, $user_id)
		{
			$this->db->Execute("INSERT INTO tourn_members (tourn_id, user_id, status, forced) VALUES('{$tourn_id}', '{$user_id}', 1, 1)
								ON DUPLICATE KEY
								UPDATE status = 1");
		}
		
		function setStatusTournamentForUserId($user_id, $status = 0)
		{
			$this->db->Execute("DELETE FROM tourn_members WHERE user_id IN($user_id) AND forced=1");
			$this->db->Execute("UPDATE tourn_members SET status = '{$status}' WHERE user_id IN ($user_id)");
		}
		
		function getMyTournaments($status = 1, $user_id)
		{
			return $this->db->getAssoc("SELECT d.id as id2, d.id, d.name, d.description, d.date_entered 
										FROM tournaments d 
										INNER JOIN tourn_members tm ON tm.tourn_id = d.id
										WHERE tm.status = '{$status}' AND tm.user_id = '{$user_id}'");
		}
		
		function checkStatusTournMembers($tourn_id, $user_id)
		{
			return $this->db->getOne("SELECT status FROM tourn_members WHERE tourn_id = '{$tourn_id}' AND user_id = '{$user_id}'");
		}
		
		function addRequestJoin($tourn_id, $user_id)
		{
			$this->db->Execute("INSERT INTO tourn_members (tourn_id, user_id, status) VALUES('{$tourn_id}','{$user_id}', 0)");
		}
		
		function removeRequestJoin($tourn_id, $user_id)
		{
			$this->db->Execute("DELETE FROM tourn_members WHERE tourn_id = '{$tourn_id}' AND user_id =  '{$user_id}'");
		}
		
		function removeMembers($tourn_id, $user_ids)
		{
			$this->db->Execute("DELETE FROM tourn_members WHERE tourn_id = '{$tourn_id}' AND user_id IN ($user_ids)");
		}
		
		function getMemebersOfTournId($tourn_id, $method = "getAll", $limit = 0)
		{
			$sql = "SELECT tm.id, u.username, CONCAT(u.name, ' ', u.family) as name, u.id as user_id, u.avatar
										FROM users u 
										INNER JOIN tourn_members tm ON tm.user_id = u.id
										INNER JOIN tournaments d ON d.id = tm.tourn_id
										WHERE tm.status = 1 AND d.id = '{$tourn_id}'";
			if($method == "pagingQuery") return $this->pagingQuery($sql, $limit);
			return $this->db->getAll($sql);
		}
		
		function getTournamentNameById($tourn_id)
		{
			return $this->db->getOne("SELECT name FROM tournaments WHERE id = '{$tourn_id}'");
		}
		
		function getMembersShareInfoByTournId($tourn_id, $doc_id, $withoutOwner = true)
		{
			if($withoutOwner){
				$sqlPart = "AND s.is_owner = 0";
				$sqlPart2 = "AND u.id != '{$_SESSION['UserID']}'";
				$sqlJoin = "LEFT";
			}else{
				$sqlJoin = "INNER";
			}
			$sql = "SELECT d.id as tourn_id, u.username, d.name as tourn_name, u.id as user_id, s.doc_id as shared, s.edit_right, s.del_right, CONCAT(u.name, ' ', u.family) as full_name, u.avatar
										FROM users u 
										INNER JOIN tourn_members tm ON tm.user_id = u.id
										INNER JOIN tournaments d ON d.id = tm.tourn_id
										$sqlJoin JOIN shared_docs s ON s.user_id = u.id $sqlPart AND s.doc_id = $doc_id 
										WHERE tm.status = 1 AND d.id = '{$tourn_id}' $sqlPart2";
			return $this->db->getAll($sql);
		}
		
		function getMyTournId()
		{
			return $this->db->getAssoc("SELECT tourn_id as id2, tourn_id FROM tourn_members WHERE user_id = '{$_SESSION['UserID']}' AND status = 1");
		}
		
		function join($id)
		{
			$ct = $this->db->getOne('SELECT COUNT(id) FROM tourn_members WHERE user_id='.$_SESSION['UserID'].' AND tourn_id='.$id);
			
			if (!isset($ct) || $ct <= 0)
			{
				$this->insert('tourn_members', array('user_id' => $_SESSION['UserID'], 'tourn_id' => $id, 'status' => 0, 'forced' => 0), true, true, true);
			}
		}
		
		function part($id)
		{
			$this->db->Execute('DELETE FROM tourn_members WHERE user_id='.$_SESSION['UserID'].' AND tourn_id='.$id);
		}
	}
?>