<?
class pullingModel extends Models
{
	function sendGameInvitation($sender, $reciever, $gameID, $position)
	{
		$username = $this->getUsername($sender);

		if ($sender != 0)
		{
			$this->insert('instant_messages', array('sender'=>$sender, 'reciever'=>$reciever, 'type'=>1, 'data'=>"Имате покана за игра от ".$username." - <a href=\"/game/play/".$gameID."\">приеми</a>", 'object' => $gameID), true, true, true);
			$this->insert('instant_messages_mem', array('sender'=>$sender, 'reciever'=>$reciever, 'type'=>1, 'data'=>"Имате покана за игра от ".$username." - <a href=\"/game/play/".$gameID."\">приеми</a>", 'object' => $gameID), true, true, true);

			$gameData = array('pos'=>$position, 'msg'=>$_SESSION['userData']['username'].' изпрати покана за игра до '.$this->getUsername($reciever));
		}
		else
			$gameData = array('pos'=>$position, 'msg'=>$_SESSION['userData']['username'].' се свързва с играта.');

		$this->insert('game_'.$gameID, array('code'=>4, 'target'=> $reciever, 'player'=>$_SESSION['UserID'], 'data'=>serialize($gameData)), true, true, true);
	}

	function getPendingMessages()
	{
		$msgs = $this->db->getAll('SELECT * FROM instant_messages_mem WHERE reciever='.$_SESSION['UserID'].' AND `read`=0');

		if (is_array($msgs))
		{
			foreach ($msgs as $k => $v)
			{
				if ($v['type'] == 1)
					;
			}
		}

		$arr = array();
		$arr['count'] = count($msgs);
		$arr['messages'] = $msgs;

		if ( $arr['count'] > 0 && is_array($msgs))
			$arr['last'] = array_pop($msgs);

		return $arr;
	}

	function getLastBet($game)
	{
		$roundAndStep = $this->getRoundAndStep($game);
		$tmp = $this->db->getOne('SELECT data FROM game_'.$game.' WHERE code=9 AND player='.$_SESSION['UserID'].' AND round='.$roundAndStep['round'].' ORDER BY id DESC');
		$tmp = unserialize($tmp);
		$tmp = (int)$tmp['lastBet'];

		if (isset($tmp) && $tmp > 0)
			return $tmp;

		return 0;
	}

	function getRealLastBet($game)
	{
		$roundAndStep = $this->getRoundAndStep($game);
		$tmp = (int)$this->db->getOne('SELECT pot FROM game_'.$game.' WHERE round='.$roundAndStep['round'].' AND code=10 ORDER BY id DESC');

		return $tmp;
	}

	function getLastBetID($game)
	{
		$roundAndStep = $this->getRoundAndStep($game);
		$tmp = $this->db->getOne('SELECT id FROM game_'.$game.' WHERE code=9 AND round='.$roundAndStep.' ORDER BY id DESC');

		if (isset($tmp) && $tmp > 0)
			return $tmp;

		return -1;
	}

	function getPlayersCredits($game)
	{
		$credits = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));

		foreach ($credits as $k=>$v)
		{
			if ($v['id'] == $_SESSION['UserID'])
				return $v['credits'];
		}

		return 0;
	}

	function fromThisRound($game, $id)
	{
		$roundAndStep = $this->getRoundAndStep($game);

		if ($this->db->getOne('SELECT COUNT(id) as "ct" FROM game_'.$game.' WHERE code=1 AND player='.$id.' AND round='.$roundAndStep['round']) > 0)
			return true;

		return false;
	}

	function getPlayersDataAdv($game)
	{
		$credits = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));
		$temp = array();

		if (is_array($credits))
		{
			foreach ($credits as $k => $v)
			{
				if (!$this->fromThisRound($game, $v['id']))
					$temp[] = $v;
			}
		}

		return $temp;
	}

	function getPlayersData($game)
	{
		$players = array();
		$players = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));

//		foreach ($tmpPlayers as $k => $v)
//		{
//			if (!isset($v['left']) || $v['left'] == 0)
//				$players[] = $v;
//		}

		return $players;
	}

	function getAvatar($id){
		$avatar = $this->db->getOne("SELECT avatar FROM users WHERE id = '{$id}'");
		$path = '/f/users/'.(int)($id/1000).LOCALSLASH."av_".$avatar.'.jpg';

		if (is_file(USER_IMG_PATH_PHYSICAL.LOCALSLASH.(int)($id/1000).LOCALSLASH."av_".$avatar.'.jpg'))
			return $path;
		else
			return '/i/design/no-avatar.png';
	}

	function playerLeft($game, $player)
	{
		$lastEntry = $this->db->getOne('SELECT id FROM game_'.$game.' WHERE code=1 AND player='.$player.' ORDER BY id DESC');
		$lastLeaving = $this->db->getOne('SELECT id FROM game_'.$game.' WHERE code=2 AND player='.$player.' ORDER BY id DESC');

		if (!isset($lastLeaving))
			return false;

		if ($lastEntry < $lastLeaving)
			return true;

		return false;
	}

	function getLastRejoinTime($game, $id)
	{
		$time = $this->db->getOne('SELECT time FROM game_'.$game.' WHERE code=1 AND player='.$id.' ORDER BY id DESC');

		if (!isset($time) || $time == '')
			return null;

		return $time;
	}

	function getGameMessages($game, $last)
	{
		$msgs = $this->db->getAll('SELECT * FROM game_'.$game.' WHERE id>'.$last);
        $roundAndStep = $this->getRoundAndStep($game);
		$players = $this->getPlayersDataAdv($game);

		if (is_array($msgs))
		{
			foreach ($msgs as $k => $v)
			{
				$msgs[$k]['data'] = unserialize($msgs[$k]['data']);
				$msgs[$k]['username'] = $this->getUsername($v['player']);
				$msgs[$k]['time'] = substr($v['time'], 11);

				if ($v['code'] == 6 )
				{
					$msgs[$k]['data']['cards'] = unserialize($msgs[$k]['data']['cards']);
					$msgs[$k]['data']['cards'] = $msgs[$k]['data']['cards'][$_SESSION['UserID']];
					require('../root/cards.php');

					if (isset($msgs[$k]['data']['cards']))
					{
						foreach ($msgs[$k]['data']['cards'] as $ck => $cv)
						{
							$msgs[$k]['data']['pile'][$cv] = $cards[$cv];
						}
					}
				}

				if ($v['code'] == 9)
				{
					$msgs[$k]['players'] = $this->getPlayersData($game);
				}
			}
		}

		$msgs_ = array();
		$ct = count($msgs) - 1;
		$lastBet = $this->getLastBetID($game);

		if (is_array($msgs))
		{
			foreach ($msgs as $k => $v)
			{
				if ((!in_array($v['code'], array(7)) || $v['round'] >= $msgs[$ct]['round']))
				{
					if ($v['code'] == 9 && ($roundAndStep['round'] - 3) > $v['round']);
					else if ($v['code'] == 9)
					{
						if ( $v['id'] != $lastBet)
						{
//							error_log("l-212 - here");
							if ($roundAndStep['round'] == $v['round'])
							{
								if ($this->fromThisRound($game, $v['player']))
								{
									$time = $this->getLastRejoinTime($game, $v['player']);
								}
								else
									$time = $v['time'];

								if ($time == null)
									$time = time();

									$v['timer'] = true;
									$v['sec'] = PUB_PLAYER_FIRST_TIME_LIMIT - abs(strtotime($time) - time());
							}
							else
								$v['timer'] = false;

							$msgs_[] = $v;
						}
					}
					else if ($v['code'] == 1)
					{
						if (!$this->playerLeft($game, $v['player']))
						{
							$v['avatar'] = $this->getAvatar($v['player']);
							$msgs_[] = $v;
						}
					}
					else if ($v['code'] == 6 && $roundAndStep['round'] > $v['round']);
					else if ($v['code'] == 6)
					{
						if ($this->checkIfPlayerViwedCards($game, $players))
						{
							$v['data']['viewed'] = true;
						}
						else
						{
							$v['data']['viewed'] = false;
							$v['data']['cards'] = null;
							$v['data']['cards']['sum'] = '';
						}

							$msgs_[] = $v;
					}
					else if ($v['code'] == 8 && (($roundAndStep['round'] - 1) > $v['round'] || ($roundAndStep['round'] != $v['round'] && $roundAndStep['round'] > 1)));
					else if ($v['code'] == 13 && (($roundAndStep['round'] - 1) != $v['round']));
					else if ($v['code'] == 13)
					{
						require('../root/cards.php');

						if (($roundAndStep['round'] - 1) > 0)
						{
							$cs = $this->getPlayersCardsFromRound($game, $v['round']);
							$tmpCards = array();
							$winner = $this->getRoundWinner($game, $v['round'], $cs);

							foreach ($players as $pk => $pv)
							{
								if (isset($cs[$pv['id']]["c3"]))
								{
									$tmpCards['pos'] = $pv['position'];
									$tmpCards["c1"]  = $cards[$cs[$pv['id']]["c1"]];
									$tmpCards["c2"]  = $cards[$cs[$pv['id']]["c2"]];
									$tmpCards["c3"]  = $cards[$cs[$pv['id']]["c3"]];
									$tmpCards["sum"] = $cs[$pv['id']]['sum'];
									$pCards[] = $tmpCards;
								}
							}

							$msgs[$k]['cards'] = $pCards;
							$msgs[$k]['winner'] = $winner;

							$msgs_[] = $msgs[$k];
						}
					}
					else if ($v['code'] == 18 && $roundAndStep['round'] > $v['round']);
					else if ($v['code'] == 19 && $roundAndStep['round'] > $v['round']);
					else if ($v['code'] == 24 && ($roundAndStep['round'] - 3) > $v['round']);
					else if ($v['code'] == 15 && ($roundAndStep['round'] - 3) > $v['round']);
					else if ($v['code'] == 19)
					{
						require('../root/cards.php');

						$cs = $this->getPlayerCards($game, $v['player']);

						$msgs[$k]['pos'] = $this->getPlayerPosition($v['player'], $game);
						$msgs[$k]['data']['pile']["c1"] = $cards[$cs["c1"]];
						$msgs[$k]['data']['pile']["c2"] = $cards[$cs["c2"]];
						$msgs[$k]['data']['pile']["c3"] = $cards[$cs["c3"]];

						$msgs_[] = $msgs[$k];
					}
					else
						$msgs_[] = $v;

				}
			}
		}

		$arr = array();
		$arr['count'] = count($msgs_);
		$arr['messages'] = $msgs_;

		if ( $arr['count'] > 0)
			$arr['last'] = array_pop($msgs_);

		return $arr;
	}

	function getRoundWinner($game, $round, $cards)
	{
		$winner = $this->db->getOne('SELECT player FROM game_'.$game.' WHERE round='.$round.' AND code=13 ORDER BY id DESC');
		$temp = array();
		$temp['user'] = $this->getUsername($winner);
		$temp['sum'] = $cards[$winner]['sum'];

		return $temp;
	}

	function getPlayerPositionFromInvitation($user, $game)
	{
		$tmp = $this->db->getOne('SELECT data FROM game_'.$game.' WHERE code= 4 AND target='.$user);
		$data = unserialize($tmp);

		return $data['pos'];
	}

	function getPlayerPosition($user, $game)
	{
		$tmp = $this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game);
		$data = unserialize($tmp);

		foreach ($data as $k=>$v)
		{
			if ($v['id'] == $user)
				return $v['position'];
		}

		return 0;
	}

	function getTablePlayers($game)
	{
		$pls = $this->db->getRow('SELECT players_list, players_cap FROM games_list_mem WHERE id='.$game);

		$pls['players_list'] = unserialize($pls['players_list']);

		$in = false;
		$pos = 0;

		foreach ($pls['players_list'] as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
				$in = true;
		}

		if (!$in)
		{
			$pos = $this->getPlayerPositionFromInvitation($_SESSION['UserID'], $game);

			if ($this->db->getOne('SELECT COUNT(id) AS "ct" FROM game_'.$game.' WHERE code = 8') > 0)
				$fold = 1;
			else
				$fold = 0;

			$pls['players_list'][] = array('id'=>$_SESSION['UserID'], 'name'=>$_SESSION['userData']['username'], 'credits'=>100, 'position'=>$pos, 'fold'=>$fold);

			$this->db->Execute('UPDATE games_list_mem SET players_list = \''.serialize($pls['players_list']).'\' WHERE id='.$game);
			$this->db->Execute('UPDATE games_list SET players_list = \''.serialize($pls['players_list']).'\' WHERE id='.$game);
			$this->addPlayerJoinMessage($game);
		}

		return $pls;
	}

	function addPlayerJoinMessage($gameID)
	{
		$msg = $this->db->getOne('SELECT data FROM game_'.$gameID.' WHERE code=4 AND target='.$_SESSION['UserID']);
		$msg = unserialize($msg);
		$position = $msg['pos'];
		$gameData = array('pos'=>$position, 'id'=>$_SESSION['UserID'], 'msg'=>$_SESSION['userData']['username'].' зае мястото си на масата.');

		$roundAndStep = $this->getRoundAndStep($gameID);

		$this->insert('game_'.$gameID, array('code'=>1, 'round'=>$roundAndStep['round'], 'target'=> 0, 'player'=>$_SESSION['UserID'], 'data'=>serialize($gameData)), true, true, true);
	}

	function getStateOnEnter($game)
	{
        
		$msgs = $this->db->getAll('SELECT * FROM game_'.$game.' ORDER BY id DESC');

		foreach ($msgs as $k => $v)
		{
			$msgs[$k]['data'] = unserialize($v['data']);
		}

		$arr = array();
		$arr['count'] = count($msgs);
		$arr['messages'] = $msgs;
		$arr['players'] = $this->getTablePlayers($game);

		if ( $arr['count'] > 0)
			$arr['last'] = array_pop($msgs);

		return $arr;
	}

	function getAllMessages($game, $last, $lastSite)
	{
		$msgs = array();
		$msgs['site'] = $this->getPendingMessages();
		$msgs['game'] = $this->getGameMessages($game, $last);

		return $msgs;
	}

	function setRead($time)
	{
		$this->db->Execute('UPDATE instant_messages SET `read`=1 WHERE reciever='.$_SESSION['UserID'].' AND time<="'.$time.'"');
		$this->db->Execute('UPDATE instant_messages_mem SET `read`=1 WHERE reciever='.$_SESSION['UserID'].' AND time<="'.$time.'"');

		echo $time;
	}

	function checkIfGameOwner($game, $user)
	{
		$host = $this->db->getOne('SELECT host_id FROM games_list WHERE id='.$game);

		if (isset($user) && ($host == $user))
			return true;

		return false;
	}

	function checkIfPlayerIsInvited($game, $user)
	{
		if (0 == $this->db->getOne('SELECT private FROM games_list_mem WHERE id='.$game))
			return true;

		$ct = $this->db->getOne('SELECT id FROM instant_messages_mem WHERE reciever='.$user.' AND type=1 AND object='.$game);

		if (isset($ct) && $ct > 0)
			return true;

		return false;
	}

	function getGamesList()
	{
		$games = $this->db->getAll('SELECT * FROM games_list_mem ORDER BY id DESC');

		foreach ($games as $k => $v)
		{
			$games[$k]['pl_count'] = count(unserialize($v['players_list']));
			$games[$k]['host'] = $this->getUsername($v['host_id']);
		}

		return $games;
	}

	function checkGameState($id)
	{
		$this->db->getOne('DESC game_'.$id);

		$state = array();

		if (mysql_errno() == 1146)
		{
			$state['error_code'] = 1;
		}
		else
		{
			$state['error_code'] = 0;

			$owner = $this->checkIfGameOwner($id, $_SESSION['UserID']);

			if ($owner)
			{
				$state['host'] = 1;
			}
			else
			{
				$state['host'] = 0;
				$state['invited'] = $this->checkIfPlayerIsInvited($id, $_SESSION['UserID']);

				if ($state['invited'] && !$this->playerInGame($id))
				{
					if ($this->db->getOne('SELECT COUNT(id) AS "ct" FROM game_'.$id.' WHERE code = 8') > 0)
					{
//						$state['error_code'] = 2;

					}
				}
			}
		}

		return $state;
	}

	function playerInGame($game)
	{
		$pls = $this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game);
		$pls = unserialize($pls);

		if (is_array($pls))
		{
			foreach ($pls as $k => $v)
				if ($v['id'] == $_SESSION['UserID'])
					return true;
		}

		return false;
	}

	function openTable($arr)
	{
		$this->insert('games_list', array('host_id'=>$_SESSION['UserID'], 'min_entry'=>addslashes($_POST['val']), 'players_cap'=>$arr['players_cap'], 'private'=>$arr['private'], 'players_list'=>serialize(array(array('id' => $_SESSION['UserID'], 'name' => $_SESSION['userData']['username'], 'credits' => 100, 'position' => 8, "fold"=>0)))), true, true, true);

		$id = mysql_insert_id();
		$this->insert('games_list_mem', array('id'=>$id, 'host_id'=>$_SESSION['UserID'], 'min_entry'=>addslashes($_POST['val']), 'players_cap'=>$arr['players_cap'], 'private'=>$arr['private'], 'players_list'=>serialize(array(array('id' => $_SESSION['UserID'], 'name' => $_SESSION['userData']['username'], 'credits' => 100, 'position' => 8, "fold"=>0)))), true, true, true);

		$this->db->Execute('CREATE TABLE IF NOT EXISTS `game_'.$id.'` (
							  `id` int(11) NOT NULL auto_increment,
							  `round` int(11) NOT NULL,
							  `code` tinyint(3) NOT NULL,
							  `target` int(11) NOT NULL,
							  `step` int(11) NOT NULL,
							  `player` int(11) NOT NULL,
							  `pot` int(11) NOT NULL default 0,
							  `data` varchar(1000) NOT NULL,
							  `bet_status` tinyint(3) NOT NULL default 0,
							  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
							  PRIMARY KEY  (`id`)
							)
							ENGINE=MEMORY DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
		$data = array(
			'msg' => 'Масата е отворена'
		);
		$this->insert('game_'.$id, array('round'=>0, 'step'=>0, 'code'=>0, 'target'=>0, 'player'=>$_SESSION['UserID'], 'data'=>serialize($data)), true, true, true);
		$this->insert('game_'.$id, array('round'=>0, 'step'=>0, 'code'=>1, 'target'=>0, 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('pos'=>8, 'host'=>1, 'id'=>$_SESSION['UserID'], 'msg'=>$this->getUsername($_SESSION['UserID']).' отвори масата.'))), true, true, true);

		return $id;
	}

	function getGameData($game)
	{
		$tmp = $this->db->getRow('SELECT * FROM games_list_mem WHERE id='.$game);
		$tmp['players_list'] = unserialize($tmp['players_list']);

		if (is_array($tmp['players_list']))
			foreach ($tmp['players_list'] as $k => $v)
			{
				if ($v['id'] == $_SESSION['UserID'])
					$tmp['position'] = $v['position'];
			}

		return $tmp;
	}

	function getTablePlayersIDs($game)
	{
		$tmp = $this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game);
		$tmp = unserialize($tmp);

		foreach ($tmp as $k => $v)
		{
			$players[] = $v['id'];
		}

		return $players;
	}

	function calculateHand($dealt, $p)
	{
		include('../root/cards.php');

		$c1['c'] = $dealt[$p]['c1'];
		$c1['w'] = $cards[$dealt[$p]['c1']]['w'];
		$c1['cl'] = $cards[$dealt[$p]['c1']]['color'];
		$c1['t'] = substr($c1['c'], 1);

		$c2['c'] = $dealt[$p]['c2'];
		$c2['w'] = $cards[$dealt[$p]['c2']]['w'];
		$c2['cl'] = $cards[$dealt[$p]['c2']]['color'];
		$c2['t'] = substr($c2['c'], 1);

		$c3['c'] = $dealt[$p]['c3'];
		$c3['w'] = $cards[$dealt[$p]['c3']]['w'];
		$c3['cl'] = $cards[$dealt[$p]['c3']]['color'];
		$c3['t'] = substr($c3['c'], 1);

		// TODO: calculate max value according to plausible combinations of cards - done ..needs testing


		// DON`T TOUCH INFERIOR MINIONS

		$maxSum = array();

		if ($c1['t'] == $c2['t'] && $c2['t'] == $c3['t'])	// Tri ednakvi karti
		{
			if ($c1['t'] == 'a')	// Tri asa
			{
				$maxSum[] = 33;
			}
			else if ($c1['t'] == '7' && ($c1['cl'] == 'clubs' || $c2['cl'] == 'clubs' || $c3['cl'] == 'clubs'))	// Dve 7ci i 7 spatiq
			{
				$maxSum[] = 32.5;
			}
			else if ($c1['t'] == '7')	// Tri 7ci
			{
				$maxSum[] = 32;
			}
			else
				$maxSum[] = $c1['w']*3;
		}
		else if ($c1['t'] == $c2['t'] || $c2['t'] == $c3['t'] || $c1['t'] == $c3['t'])	// Dve ednakvi karti
		{
			if (($c1['t'] == $c2['t'] && $c1['t'] == 'a' && $c3['t'] == '7' && $c3['cl'] == 'clubs') || ($c2['t'] == $c3['t'] && $c2['t'] == 'a' && $c1['t'] == '7' && $c1['cl'] == 'clubs') || ($c1['t'] == $c3['t'] && $c1['t'] == 'a' && $c2['t'] == '7' && $c2['cl'] == 'clubs'))
			{
				$maxSum[] = 33;	// Dve asa i sedmica spatiq
			}
			else if (($c1['t'] == $c2['t'] && $c1['t'] == 'a') || ($c2['t'] == $c3['t'] && $c2['t'] == 'a') || ($c1['t'] == $c3['t'] && $c1['t'] == 'a'))
			{
				$maxSum[] = 22;	// Dve asa
			}
			else
			{
				if (($c1['t'] == $c2['t'] && $c3['t'] == '7' && $c3['cl'] == 'clubs') || ($c2['t'] == $c3['t'] && $c1['t'] == '7' && $c1['cl'] == 'clubs') || ($c1['t'] == $c3['t'] && $c2['t'] == '7' && $c2['cl'] == 'clubs'))
				{
					$maxSum[] = $c1['w'] + $c2['w'] + $c3['w'];   // Teoreticno pri 2 ednakvi i 7mak spatiq
				}
			}
		}

		if ($c1['cl'] == $c2['cl'] && $c2['cl'] == $c3['cl'])	// Tri ednakvi cvqta
		{
			$maxSum[] = $c1['w'] + $c2['w'] + $c3['w'];	//Sumata ot kartite s ednakyv cvqt
		}
		else if ($c1['cl'] == $c2['cl'] || $c2['cl'] == $c3['cl'] || $c1['cl'] == $c3['cl'])	// 2 ednakvi cvqta
		{
            $c1w = 0; $c2w = 0; $c3w = 0;
			if ($c3['t'] == '7' && $c3['cl'] == 'clubs')
            {
                
                if($c1['cl'] == $c2['cl'])
                {
                    $maxSum[] = $c1['w'] + $c2['w'] + $c3['w'];
                }
                else
                {
                    $tmp_max = max( array( $c1['w'], $c2['w']  ) );
                    $maxSum[] = $tmp_max + $c3['w'];
                }
 
            }

			else if ($c2['t'] == '7' && $c2['cl'] == 'clubs')
            {

                if($c1['cl'] == $c3['cl'])
                {
                    $maxSum[] = $c1['w'] + $c2['w'] + $c3['w'];
                }
                else
                {
                    $tmp_max = max( array( $c1['w'], $c3['w']  ) );
                    $maxSum[] = $tmp_max + $c2['w'];                    
                }
                
            }
			else if ($c1['t'] == '7' && $c1['cl'] == 'clubs')
            {
                
                if($c2['cl'] == $c3['cl'])
                {
                    $maxSum[] = $c1['w'] + $c2['w'] + $c3['w'];
                }
                else
                {
                    $tmp_max = max( array( $c1['w'], $c2['w']  ) );
                    $maxSum[] = $tmp_max + $c1['w'];                    
                }
                
            }
            else
            {
                
            }
            
			if ($c1['cl'] == $c2['cl'])
				$maxSum[] = $c1['w'] + $c2['w'] + $c3w;
			else if ($c2['cl'] == $c3['cl'])
				$maxSum[] = $c2['w'] + $c3['w'] + $c1w;
			else if ($c1['cl'] == $c3['cl'])
				$maxSum[] = $c1['w'] + $c3['w'] + $c2w;
		}
        // 2 razli4ni + 7 spatiq
        else if( ($c1['t'] == '7' && $c1['cl'] == 'clubs') || ($c2['t'] == '7' && $c2['cl'] == 'clubs') || ($c3['t'] == '7' && $c3['cl'] == 'clubs') )
        {
            if($c1['t'] == '7' && $c1['cl'] == 'clubs')
            {
                $tmp_max = max(array( $c2['w'], $c3['w'] ));
                
            }
            else if($c2['t'] == '7' && $c2['cl'] == 'clubs')
            {
                $tmp_max = max(array( $c1['w'], $c3['w'] ));
                
            }
            else if($c3['t'] == '7' && $c3['cl'] == 'clubs')
            {
                $tmp_max = max(array( $c1['w'], $c2['w'] ));

            }
            $maxSum[] = $tmp_max + 11;
        }
      
		else	// Nai-silna karta
		{
			if ($c1['t'] == '7' && $c1['cl'] == 'clubs')
            {
				$maxSum[] = (max(array($c2['w'], $c3['w']))) + $c1['w'];
                
            }
			else if ($c2['t'] == '7' && $c2['cl'] == 'clubs')
            {
				$maxSum[] = (max(array($c1['w'], $c3['w']))) + $c2['w'];
                
            }
            else if($c3['t'] == '7' && $c3['cl'] == 'clubs')
            {
				$maxSum[] = (max(array($c2['w'], $c1['w']))) + $c3['w'];
                
            }
			$maxSum[] = max(array($c1['w'], $c2['w'], $c3['w']));
		}

		// Problema s tejesta na rykata (dali e po-tejka kombinaciqta ili stoinostta) - v momenta e stoinostta ( i taka trqbva da ostane)

		return max($maxSum);

	}

	function dealCards($deck, $game)
	{
		$tmp_cards = $deck;

		if (is_array($tmp_cards))
			shuffle($tmp_cards);
           
		$dealt = array();

		$pls = $this->getTablePlayersIDs($game);

		for ($j = 1; $j < 4; ++$j)
		{
			foreach ($pls as $p)
			{
				if (is_array($tmp_cards))
					$dealt[$p]['c'.$j] = array_pop($tmp_cards);
			}
		}

		foreach ($pls as $p)
		{
			$dealt[$p]['sum'] = $this->calculateHand($dealt, $p);
		}

		return $dealt;
	}

	function findNextPlayer($position, $players)
	{
		$next = false;
		foreach ($players as $k => $v)
		{
			if ($next)
			{
				return $v['position'];
			}

			if ($v['position'] == $position)
				$next = true;
		}
		return $players[0]['position'];
	}

	function preorderPlayers($game, $winner = 0)
	{
		$playersList = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));

		$tmp = array();

		foreach ($playersList as $k => $v)
		{
			$tmp[] = $v['position'];
		}

		sort($tmp, SORT_NUMERIC);
		$ct = count($tmp);

		if ($winner > 0)
		{
			$next = $this->findNextPlayer($winner, $playersList);
			$tmp_ = array();
			$tmp_[] = $next;

			// MAGIC X_X -- reorder players so the next after the winner is first next round

			for ($i = 0; $i < $ct; ++$i)
			{
				if ($tmp[$i] > $next)
				{
					$tmp_[] = $tmp[$i];
				}
			}

			for ($i = 0; $i < $ct; ++$i)
			{
				if ($tmp[$i] < $next && isset($tmp[$i]))
				{
					$tmp_[] = $tmp[$i];
				}
			}
			$tmp = $tmp_;
		}
		else
		{
			sort($tmp, SORT_NUMERIC);
		}



		for ($i = 0; $i < $ct; ++$i)
		{
			foreach ($playersList as $k => $v)
			{
				$v['dark'] = 1;
				if ($v['position'] == $tmp[$i])
					$players[] = $v;
			}
		}

		// END OF MAGIC SO FAR

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);
	}

	function procLightedPlayers($game, $winner)
	{
		$players = $this->getPlayersData($game);

		foreach ($players as $k => $v)
		{
			if ($v['lighted'] == 1)
			{
				if ($v['id'] == $winner)
				{
					$v['lighted'] = 0;
					$players[$k] = $v;
				}
			}
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);
	}

	function getPlayerIDByPosition($pos, $players)
	{
		error_log("p:".$pos);
		foreach ($players as $k => $v)
		{
			if ($v['position'] == $pos)
				return $v['id'];
		}

		return 0;
	}

	function startGame($game, $deck, $round = 1, $winner = 0, $player = 0)
	{
		if ($winner > 0)
		{
			$this->procLightedPlayers($game, $winner);
		}

		if ($player == 0)
			$player = $_SESSION['UserID'];

		$this->preorderPlayers($game, $winner);

		$players = $this->getPlayersData($game);

		if (count($players) > 1)
		{
			$this->insert('game_'.$game, array('round'=>$round, 'step'=>0, 'code'=>8, 'target'=>0, 'player'=>$player, 'data'=>serialize(array('msg'=>'Започва раздаване на картите.'))), true, true, true);
			$cards = $this->dealCards($deck, $game);
			$this->insert('game_'.$game, array('round'=>$round, 'step'=>1, 'code'=>6, 'target'=>0, 'player'=>0, 'data'=>serialize(array('cards'=>serialize($cards),'msg'=>'Картите са раздадени. Започва залагането.'))), true, true, true);

			$roundAndStep = $this->getRoundAndStep($game);

			foreach($players as $k => $v)
			{
				if ($v['id'] == $player)
				{
					$v['credits'] -= DEALER_GIVING;

					if ($v['credits'] < 0)
					{
						$v['credits'] = 0;
						$credits = $v['credits'];
					}

					$players[$k] = $v;
				}
			}

			$dealer = $this->getLastWinner($game);

			$this->insert('game_'.$game, array('code'=>24, 'target'=>0, 'pot'=>DEALER_GIVING, 'round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'player'=>$dealer, 'data'=>serialize(array('credits'=>$credits, 'position'=>$this->getPlayerPosition($dealer, $game) ,'msg'=>$this->getUsername($dealer).' даде '.DEALER_GIVING.' кредита.'))), true, true, true);

			$this->firstPlayerTurn($game, 0, false, 0, $player);

			$this->db->Execute('UPDATE games_list SET state=2, players_list=\''.serialize($players).'\' WHERE id='.$game);
			$this->db->Execute('UPDATE games_list_mem SET state=2, players_list=\''.serialize($players).'\' WHERE id='.$game);

			return array('stat'=>1);
		}
		else
			return array('stat'=>0, 'msg'=>'За да стартирате играта трябва да има поне двама играчи.');
	}

	function getLastWinner($game)
	{
		$last = 0;
		$last = $this->db->getOne('SELECT player FROM game_'.$game.' WHERE code=13 ORDER BY id DESC');

		if (!isset($last) || $last == 0)
		{
			$last = $this->getHost($game);
		}

		return $last;
	}

	function getHost($game)
	{
		return $this->db->getOne('SELECT player FROM game_'.$game.' WHERE code=0 ORDER BY id DESC');
	}

	function startGameWithSvarka($game, $deck, $round = 1, $winner = 0, $player = 0)
	{
		if ($player == 0)
			$player = $_SESSION['UserID'];

		$this->preorderPlayers($game, $winner);
		$this->insert('game_'.$game, array('round'=>$round, 'step'=>0, 'code'=>8, 'target'=>0, 'player'=>$player, 'data'=>serialize(array('msg'=>'Започва раздаване на картите.'))), true, true, true);
		$cards = $this->dealCards($deck, $game);
		$this->insert('game_'.$game, array('round'=>$round, 'step'=>1, 'code'=>6, 'target'=>0, 'player'=>0, 'data'=>serialize(array('cards'=>serialize($cards),'msg'=>'Картите са раздадени. Започва залагането.'))), true, true, true);
		$this->firstPlayerTurn($game, 0, false, 0, $player);
	}

	function playerIsFolded($players, $id)
	{
		foreach ($players as $k => $v)
		{
			if ($v['id'] == $id && $v['fold'] == 1)
				return true;
		}

		return false;
	}

	function getCurrentPlayerLastBetID($game, $round, $player)
	{
		$last = $this->db->getRow('SELECT id, pot FROM game_'.$game.' WHERE round='.$round.' AND code=10 AND player='.$player.' ORDER BY id DESC LIMIT 0, 1');

		if (isset($last['id']) && $last['id'] > 0)
			return $last;

		return array(0, 0);
	}

	function firstPlayerTurn($game, $iterator = 0, $last = false, $itStep = 0, $player = 0)
	{
		$players = $this->getPlayersData($game);
		$roundAndStep = $this->getRoundAndStep($game);

		if ($player == 0)
			$player = $_SESSION['UserID'];

		$nextPlayer = $this->getNextPlayerId($game, $player);
		$prevPlayer = $this->db->getOne('SELECT player FROM game_'.$game.' WHERE code=9 ORDER BY id DESC');

		foreach ($players as $k =>$v)
		{
			if ($v['fold'] != 1 && (!isset($v['open']) || $v['open'] == 0))
			{
				$player = $v;
				break;
			}
		}

		$thisPlayerTemp = $this->getCurrentPlayerLastBetID($game, $roundAndStep['round'], $player);
		$thisPlayerLastBetID = $thisPlayerTemp['id'];
		$thisPlayerPrevBet = $thisPlayerTemp['pot'];
		$bets = $this->db->getAll('SELECT * FROM game_'.$game.' WHERE round='.$roundAndStep['round'].' AND code=10 AND id>'.$thisPlayerLastBetID.' ORDER BY id ASC');
		$bet = 0;
		$first = true;
		$prev = 0;

		if (is_array($bets))
			foreach ($bets as $k=>$v)
			{
				if(!$first)
				{
					$bet += abs($prev-$v['pot']);
				}
				else
				{
					$bet += abs($thisPlayerPrevBet-$v['pot']);
					$first = false;
				}

				$prev = $v['pot'];
			}

		if ($itStep > 0)
			$step = $roundAndStep['step'] + 1;
		else
			$step = 1;

		$this->insert('game_'.$game, array('round'=>$roundAndStep['round'] + $iterator, 'step'=>$step, 'code'=>9, 'target'=>0, 'player'=>$player['id'], 'data'=>serialize(array('last'=> false, 'lastBet'=>$bet, 'pos'=>$player['position'], 'msg'=>$this->getUsername($player['id']).' започна своя ред.'))), true, true, true);

		return false;
	}

	function nextPlayerTurn($game, $pl = 0)
	{
		$roundAndStep = $this->getRoundAndStep($game);
		$players = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));
		$lastPlayer = $this->db->getOne('SELECT player FROM game_'.$game.' WHERE code=10 OR code=15 OR code=19 AND round='.$roundAndStep['round'].' ORDER BY id DESC');
		$lastPlayerBet = $this->db->getOne('SELECT pot FROM game_'.$game.' WHERE code=10 AND round='.$roundAndStep['round'].' ORDER BY id DESC');

		if (!isset($lastPlayerBet) || $lastPlayer == '')
		{
			$lastPlayerBet = 0;
		}

		$found = false;
		$last = false;
		$i = 0;

		foreach ($players as $k => $v)
		{
			if ($found && $v['fold'] != 1 && (!isset($v['open'])))
			{
				$player = $v;
				break;
			}

			if ($v['id'] == $lastPlayer)
				$found = true;

			++$i;
		}

		if ($player == null)
		{
			$player = $players[0];
		}


		if ($i == count($players) - 1)
		{
			$last = true;
		}

		if ($pl > 0)
		{
			$player['id'] = $pl;
		}

		$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'] + 1, 'code'=>9, 'target'=>0, 'player'=>$player['id'], 'data'=>serialize(array('last'=>$last, 'lastBet'=>$lastPlayerBet, 'pos'=>$player['position'], 'msg'=>$this->getUsername($player['id']).' започна своя ред.'))), true, true, true);
	}

	function extractPlayerData($array)
	{
		if (is_array($array))
		{
			foreach ($array as $k => $v)
				if ($v['id']==$_SESSION['UserID'])
					return $v;
		}
		else
			return null;
	}

	function checkBalance($game, $bet)
	{
		$tmp = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));
		$playerData = $this->extractPlayerData($tmp);
		unset($tmp);

		if (($playerData['credits'] - $bet) >= 0)
			return true;

		return false;
	}

	function getRoundAndStep($game)
	{
		$tmp = $this->db->getRow('SELECT round, step FROM game_'.$game.' ORDER BY id DESC LIMIT 1');

		return $tmp;
	}

	function getRoundAndStepLeaving($game)
	{
		$tmp = $this->db->getRow('SELECT round, step FROM game_'.$game.' WHERE code=8 OR code=1 ORDER BY id DESC LIMIT 1');

		return $tmp;
	}

	function endCycle($game)
	{
		$roundAndStep = $this->getRoundAndStep($game);

		$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>11, 'target'=>0, 'player'=>0, 'data'=>serialize(array('msg'=>'Разиграването приключи - обработване на ръцете.'))), true, true, true);

		$this->resolveRound($game);
	}

	function getPlayersCards($game)
	{
		$tmp = unserialize($this->db->getOne('SELECT data FROM game_'.$game.' WHERE code = 6 ORDER BY id DESC'));
		$tmp = unserialize($tmp['cards']);

		return $tmp;
	}

	function getPlayersCardsFromRound($game, $round)
	{
		$tmp = unserialize($this->db->getOne('SELECT data FROM game_'.$game.' WHERE code = 6 AND round='.$round.' ORDER BY id DESC'));
		$tmp = unserialize($tmp['cards']);

		return $tmp;
	}

	function getPlayerCards($game, $player)
	{
		$cards = $this->getPlayersCards($game);

		return $cards[$player];
	}

	function openCards($game, $player)
	{
		$pls = $this->getPlayersData($game);

		foreach ($pls as $k => $v)
		{
//			if ($v['id'] == $player)
				$pls[$k]['dark'] = 0;
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($pls).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($pls).'\' WHERE id='.$game);
	}

	function setViewdCards($player, $cards, $game)
	{
		$roundAndStep = $this->getRoundAndStep($game);

		$data = array("cards"=> $cards, "msg"=>$this->getUsername($player)." обърна кратите си.");
		$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>18, 'target'=>0, 'player'=>$player, 'data'=>serialize($data)), true, true, true);
	}

	function resolveRound($game)
	{
		require('../root/cards.php');

		$hands = $this->getPlayersCards($game);
		$players = $this->getPlayersData($game);


		foreach ($players as $k=>$v)
		{
			if ($v['fold'] == 0)
				$playerHand[$v['id']] = $hands[$v['id']]['sum'];
		}

		$max = 0;
		$maxID = 0;
		$ct = 1;

		foreach ($playerHand as $k => $v)
		{
			if ($v > $max)
			{
				$max = $v;
				$maxID = $k;

				$ct = 1;
			}
			else if ($v == $max)
				++$ct;
		}

		$pos = $this->getPlayerPosition($maxID, $game);
		$roundAndStep = $this->getRoundAndStep($game);
		$pot = $this->getRoundPot($game, $roundAndStep['round']);

		if ($ct > 1)
		{
			$this->foldBellowVal($game, $max, $players, $playerHand);
			$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>20, 'target'=>0, 'player'=>0, 'data'=>serialize(array('pot'=>$pot, 'floor'=>($pot/$ct), 'msg'=>'СВАРКА!!!.'))), true, true, true);
			$this->beginNextRoundWithSvarka($game, $deck, $roundAndStep['round']);
		}
		else
		{

			$this->sendPotToPlayer($game, $maxID, $pot);
			$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>13, 'target'=>0, 'player'=>$maxID, 'data'=>serialize(array('pos'=>$pos, 'pot'=>$pot, 'msg'=>$this->getUsername($maxID).' е победител в рунда.'))), true, true, true);
			$this->beginNextRound($game, $deck, $roundAndStep['round'] + 1, $pos, $maxID);
		}
	}

	function clearLeftPlayers($game)
	{
//		$players = $this->getPlayersData($game);
//		$roundAndStep = $this->getRoundAndStep($game);
//		$tmpPlayers = array();
//
//		foreach ($players as $k => $v)
//		{
//			if (isset($v['left']) && $v['left'] == 1)
//			{
//				$pos = $this->getPlayerPosition($v['id'], $game);
//				$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>2, 'target'=>0, 'player'=>(int)$v['id'], 'data'=>serialize(array('pos' => $pos, 'msg'=>$this->getUsername((int)$v['id']).' напусна играта.'))), true, true, true);
//			}
//			else
//			{
//				$tmpPlayers[] = $v;
//			}
//		}
//
//		error_log(json_encode($tmpPlayers));
//
//		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($tmpPlayers).'\' WHERE id='.$game);
//		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($tmpPlayers).'\' WHERE id='.$game);

		;
	}

	function getRoundPot($game, $round)
	{
		$pot = $this->db->getOne('SELECT SUM(pot) FROM game_'.$game.' WHERE round='.$round);

		return $pot;
	}

	function checkTimeout($game, $player)
	{
		$time = $this->db->getRow('SELECT time, player, id FROM game_'.$game.' WHERE code=9 AND player='.$player.' ORDER BY id DESC LIMIT 0, 1');

		error_log("checkTimeout: ".(time() - strtotime($time['time']))." - ".PUB_PLAYER_FIRST_TIME_LIMIT);

		if ((time() - strtotime($time['time'])) >= PUB_PLAYER_FIRST_TIME_LIMIT)
		{
//			$left = $this->db->getOne('SELECT id FROM game_'.$game.' WHERE code=15 AND player='.$time['player'].' ORDER BY id DESC LIMIT 0, 1');

			$players = $this->getPlayersData($game);
			$left = 0;

			error_log("checkTimeout: in cond");

//			foreach ($players as $k => $v)
//			{
//				if (($v['id'] == $time['player']) && ($v['left'] == 1) && ($v['fold'] == 1))
//					$left = 1;
//			}

			error_log("checkTimeout: left = ".$left);

			if ($left == 0)
			{
				if ($_SESSION['UserID'] == $time['player'])
				{
					error_log("checkTimeout: this player check");
					$this->playerLeave($game, $_SESSION['UserID']);
				}
				else
				{
					error_log("checkTimeout: other player check");
					$this->setLeaveFlag($game, $time['player']);
				}
			}
		}
	}

	function setLeaveFlag($game, $player)
	{
		if (!$this->playerSetLeft($game, $player))
		{
			$this->playerFold($game, $player);
			$this->insert('left_players', array('game_id'=>$game, 'player_id'=>$player), true, true, true);
			$roundAndStep = $this->getRoundAndStep($game);
			$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'bet_status'=>0, 'pot'=>0, 'code'=>2, 'target'=>0, 'player'=>$player, 'data'=>serialize(array( 'msg'=>$this->getUsername($player).' е неактивен.'))), true, true, true);
		}
	}

	function playerSetLeft($game, $player)
	{
		$ct = $this->db->getOne('SELECT COUNT(id) AS "ct" FROM left_players WHERE game_id='.$game.' AND player_id='.$player);

		if ($ct > 0)
			return true;

		return false;
	}

	function sendPotToPlayer($game, $player, $pot)
	{
		$playersData = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));

		foreach ($playersData as $k=>$v)
		{
			if ($v['id'] == $player)
				$playersData[$k]['credits'] += $pot;
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($playersData).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($playersData).'\' WHERE id='.$game);
	}

	function clearFoldings($game)
	{
		$players = $this->getPlayersData($game);

		foreach ($players as $k => $v)
		{
			$players[$k]['fold'] = 0;
			if (isset($players[$k]['open']))
				unset($players[$k]['open']);

			if (isset($players[$k]['sfold']))
				unset($players[$k]['sfold']);
		}

		error_log("clearFoldings: ".json_encode($players));
		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\', time=NOW() WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\', time=NOW() WHERE id='.$game);
		error_log("sdfsdf".mysql_error());
	}

	function clearFoldingsSvarka($game)
	{
		$players = $this->getPlayersData($game);

		foreach ($players as $k => $v)
		{
			$players[$k]['fold'] = 0;
			if (isset($players[$k]['open']))
				unset($players[$k]['open']);
		}


		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\', time=NOW() WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\', time=NOW() WHERE id='.$game);
	}

	function beginNextRound($game, $deck, $round, $winner, $player = 0)
	{
//		$this->clearLeftPlayers($game);
		$this->clearFoldings($game);
		$this->startGame($game, $deck, $round, $winner, $player);
	}

	function beginNextRoundWithSvarka($game, $deck, $round)
	{
		$this->clearLeftPlayers($game);
		$this->clearFoldingsSvarka($game);
		$this->startGame($game, $deck, $round);
	}

	function stripFoldedPlayers($players)
	{
		$tmp = array();

		foreach ($players as $k => $v)
		{
			if ($v['fold'] == 0)
				$tmp[$k] = $v;
		}

		return $tmp;
	}

	function checkIfEndOfRound($game, $round, $bet)
	{
		$players = $this->getPlayersData($game);
		$players = $this->stripFoldedPlayers($players);
		$ctPlayers = count($players);
		$tempPlayers = array();

		$tmp = $this->db->getAll('SELECT player FROM game_'.$game.' WHERE round = '.$round.' AND code = 10 AND pot='.$bet.' AND bet_status = 1 ORDER BY id DESC LIMIT 0, '.$ctPlayers);

		foreach ($tmp as $k => $v)
		{
			$tempPlayers[] = $v['player'];
		}
		unset($tmp);

		if (count($tempPlayers > 0))
		{
			foreach ($players as $k=>$v)
			{
				if (in_array($v['id'], $tempPlayers))
				{
					$answered[$v['id']] = true;
				}
			}
		}

		if ($ctPlayers == count($answered))
		{
			// All have answered
			return true;
		}

		return false;
	}

	function getPlayerBet($game, $round, $player)
	{
		$bet = $this->db->getOne('SELECT pot FROM game_'.$game.' WHERE player='.$player.' AND round='.$round.' ORDER BY id DESC');

		return $bet;
	}

	function checkIfBetIsEqual($game, $round, $step, $prevBet = 0)
	{
		$players = $this->getPlayersData($game);
		$ct = 0;

		$prevBets = $this->db->getAll('SELECT pot FROM game_'.$game.' WHERE code=10 ORDER BY id DESC LIMIT 0, 2');

		$prevBetTemp = $prevBets[0]['pot'];
		$prevPrevBet = $prevBets[1]['pot'];

		$prevBet = abs($prevPrevBet - $prevBetTemp);

		foreach ($players as $k=>$v)
		{
			if ($v['fold'] == 0)
			{
				$bet = $this->getPlayerBet($game, $round, $v['id']);

				if ($prevBet != $bet)
					return false;

				++$ct;
			}
		}

		return true;
	}

	function getPrevPlayerID($game)
	{
		$players = $this->getPlayersData($game);
		$prev = null;

		foreach($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
				break;

			$prev = $v;
		}

		if ($prev != null)
			return $prev;
		else
			return array_pop($players);
	}

	function getPrevPlayerIDInGame($game)
	{
		$players = $this->getPlayersData($game);
		$prev = null;

		// SOME ISSUE

		foreach($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
				break;

			if (($v['fold'] != 1) && (!isset($v['open'])))
				$prev = $v;
		}

		if ($prev != null)
			return $prev;
		else
		{
			while (($temp = array_pop($players)) != null)
				if ($temp['id'] != $_SESSION['id'] && $temp['fold'] != 1 && (!isset($temp['open']) || $temp['open'] == 0))
					return $temp;
		}
	}

	function getLastToAnswer($game, $round)
	{
		$last = $this->db->getAll('SELECT * FROM game_'.$game.' WHERE round='.$round.' AND code=10 ORDER BY id DESC');
		$svarka = $this->db->getOne('SELECT * FROM game_'.$game.' WHERE round='.$round.' AND code=20 ORDER BY id DESC');


		foreach ($last as $k => $v)
		{
			$v['data'] = unserialize($v['data']);

			if (isset($v['data']['lta']) && $v['id'] > $svarka)
				return $v;
		}

		return null;
	}

	function doBetting($game, $bet, $last, $lastBet, $answer = false)
	{
		$roundAndStep = $this->getRoundAndStep($game);
		$pos = $this->getPlayerPosition($_SESSION['UserID'], $game);
		$betEq = false;

		$stat = 1;

		if ($lastBet == 'NaN')
			$lastBet = 0;
		if ($bet > $lastBet)
		{
			$lastToAnswer = $this->getPrevPlayerIDInGame($game);
			$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'bet_status'=>$stat, 'pot'=>$bet, 'code'=>10, 'target'=>0, 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('pos'=>$pos, 'lta'=>$lastToAnswer['id'], 'bet'=>$bet, 'msg'=>$this->getUsername($_SESSION['UserID']).' заложи '.$bet.' чипa.'))), true, true, true);
		}
		else
		{
			$stat = 2;
			$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'bet_status'=>$stat, 'pot'=>$bet, 'code'=>10, 'target'=>0, 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('pos'=>$pos, 'bet'=>$bet, 'msg'=>$this->getUsername($_SESSION['UserID']).' заложи '.$bet.' чипa.'))), true, true, true);
		}

		$lastPlayerData = $this->getLastToAnswer($game, $roundAndStep['round']);

		if ($last == 'true')
		{
			$last = true;
			/*
			 * not all other players have folded so resolve round on betting players
			 * excluding all folded players (the hard part)
			 */
//			if ($this->checkIfBetIsEqual($game, $roundAndStep['round'], $roundAndStep['step'], $lastBet))
//			{
//				error_log("813: bet is equal");
//				$betEq = true;
//			}
//			else
//			{
//				error_log("819: bet not equal");
//				$betEq = false;
//			}
		}
		else if ($last == 'false')
		{
			$last = false;
		}


		if ($bet == $lastBet && $lastPlayerData['data']['lta'] == $_SESSION['UserID'])
		{
			$this->endCycle($game);
		}
		else
		{
			if (!$last)
			{
				$next = $this->nextPlayerTurn($game);
			}
			else
			{
				if ($lastBet > 0)
					$next = $this->firstPlayerTurn($game, 0, $last, 1);
				else
					$next = $this->firstPlayerTurn($game, 0, $last);
			}
		}

		if (mysql_affected_rows() > 0)
		{
			$playersData = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));
			foreach ($playersData as $k=>$v)
			{
				if ($v['id'] == $_SESSION['UserID'])
					$playersData[$k]['credits'] -= $bet;
			}

			$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($playersData).'\' WHERE id='.$game);
			$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($playersData).'\' WHERE id='.$game);
		}

		return $next;
	}

	function bet($game, $bet, $last, $lastBet, $answer = false)
	{
		$return = array();
		$return['error'] = 0;

		if ($this->checkBalance($game, $bet))
		{
			$return['lastPlayer'] = $this->doBetting($game, $bet, $last, $lastBet, $answer);
		}
		else
		{
			$return['error'] = 1;
			$return['msg'] = 'Нямате достатъчно чипове.';
		}

		return $return;
	}

	function playerFold($game, $player = 0)
	{
		$players = $this->getPlayersData($game);

		if ($player == 0)
			$player = $_SESSION['UserID'];

		foreach ($players as $k => $v)
		{
			if ($v['id'] == $player)
			{
				$players[$k]['fold'] = 1;
			}
		}

		error_log(json_encode($players));

		$roundAndStep = $this->getRoundAndStep($game);
		$pos = $this->getPlayerPosition($player, $game);
		$lastToAnswer = $this->getLastToAnswer($game, $roundAndStep['round']);

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'pot'=>0, 'code'=>15, 'target'=>0, 'player'=>$player, 'data'=>serialize(array('pos'=>$pos, 'msg'=>$this->getUsername($player).' избяга от залагането.'))), true, true, true);

		if ($this->allPlayersFolded($game, $players, $player))
		{
			$this->resolveFoldedRound($game);
		}
		else
		{
			error_log("test");
			// Resolve round for players who haven't folded
			if ($this->playerIsLast($players, $player) || ($lastToAnswer != null && $lastToAnswer['data']['lta'] == $player))
			{
				if ($this->moreThanOneBetted($game, $roundAndStep['round'], $roundAndStep['step']))
				{
					if ($this->checkIfBetIsEqual($game, $roundAndStep['round'], $roundAndStep['step']) || ($lastToAnswer != null && $lastToAnswer['data']['lta'] == $player))
					{
						$this->endCycle($game);
					}
					else
					{
						$this->firstPlayerTurn($game, 0, true);
					}
				}
				else
				{
					$player_ = $this->getOnlyBettedPlayer($game, $roundAndStep['round']);
					$pot = $this->getRoundPot($game, $roundAndStep['round']);
					$this->sendPotToPlayer($game, $player_, $pot);
					$pos = $this->getPlayerPosition($player_, $game);
					$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>13, 'target'=>0, 'player'=>$player_, 'data'=>serialize(array('pos'=>$pos, 'pot'=>$pot, 'msg'=>$this->getUsername($player_).' е победител в рунда.'))), true, true, true);
					$this->beginNextRound($game, $deck, $roundAndStep['round'] + 1, $player_);
				}
			}
			else
			{
				if ($this->checkIfNextPlayerIsLast($game, $player) && $this->noBetsThisRound($game, $roundAndStep['round'], $players))
				{
					require('../root/cards.php');
					$player_ = $this->getNextPlayerId($game, $player);
					$pot = $this->getRoundPot($game, $roundAndStep['round']);
					$this->sendPotToPlayer($game, $player_, $pot);
					$pos = $this->getPlayerPosition($player_, $game);
					$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>13, 'target'=>0, 'player'=>$player_, 'data'=>serialize(array('pos'=>$pos, 'pot'=>$pot, 'msg'=>$this->getUsername($player_).' е победител в рунда.'))), true, true, true);
					$this->beginNextRound($game, $deck, $roundAndStep['round'] + 1, $player_);
				}
				else
				{
					$this->nextPlayerTurn($game);
				}
			}

		}
	}

	function noBetsThisRound($game, $round, $players)
	{
		$bettedPlayers = $this->db->getAll('SELECT * FROM game_'.$game.' WHERE round='.$round.' AND code=10');
		$ct = 0;

		if (count($bettedPlayers) > 0)
		{
			foreach ($bettedPlayers as $k => $v)
			{
				foreach ($players as $kp => $p)
				{
					if ($v['player'] == $p['id'] && $v['pot'] > 0 && $p['fold'] == 0)
						$ct++;
				}
			}
		}
		if ($ct > 0) // Predi beshe edno, no sled novite promeni za lovene na krai na rund e 0. Eventualno moje da vyznikne problem no shte se lovi v posledstvie zaradi promenite s novata navigaciq
			return false;

		return true;
	}

	function getNextPlayerId($game, $lastPlayer)
	{
		$players = $this->getPlayersData($game);

		$found = false;
		$next = false;
		$player = 0;

		foreach ( $players as $k => $v)
		{
			if ($found && $v['fold'] == 0)
			{
				$player = $v['id'];
			}

			if ($v['id'] == $lastPlayer)
				$found = true;
		}

		return $player;
	}

	function checkIfNextPlayerIsLast($game, $player)
	{
		$players = $this->getPlayersData($game);
		$found = false;
		$next = false;

		if ($player == 0)
			$player = $_SESSION['UserID'];

		$i = 0;
		while(count($players) > 0)
		{
			if ($players[count($players)-1]['id'] != $player)
			{
				++$i;
				array_pop($players);
			}
			else
				break;
		}

		if ($i == 1)
			return true;

		return false;
	}

	function getOnlyBettedPlayer($game, $round)
	{
		$players = $this->getPlayersData($game);
		$bets = $this->db->getAll('SELECT player, pot FROM game_'.$game.' WHERE round='.$round.' AND code=10 ORDER BY id DESC');
		$ct = 0;
		foreach ($players as $k => $v)
		{
			foreach ($bets as $kb=>$b)
			{
				if (($v['id'] == $b['player']) && ($v['fold'] == 0))
					return $v['id'];
			}
		}
	}

	function moreThanOneBetted($game, $round, $step)
	{
		$players = $this->getPlayersData($game);
		$bets = $this->db->getAll('SELECT player, pot FROM game_'.$game.' WHERE round='.$round.' AND code=10 ORDER BY id DESC');
		$ct = 0;

		foreach ($players as $k => $v)
		{
			foreach ($bets as $kb=>$b)
			{
				if (($v['id'] == $b['player']) && ($v['fold'] == 0))
					++$ct;
			}
		}

		if ($ct > 1)
			return true;

		return false;
	}

	function resolveFoldedRound($game, $player = 0)
	{
		require('../root/cards.php');

		// Depricated ->

		if ($player == 0)
			$player = $_SESSION['UserID'];

		// End depricated

		$players = $this->getPlayersData($game);

		foreach ($players as $k => $v)
		{
			if ($v['fold'] != 1)
				$player = $v['id'];
		}

		$this->resetPlayersFolds($game);

		$pos = $this->getPlayerPosition($player, $game);
		$roundAndStep = $this->getRoundAndStep($game);
		$pot = $this->getRoundPot($game, $roundAndStep['round']);
		$this->sendPotToPlayer($game, $player, $pot);

		$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'code'=>13, 'target'=>0, 'player'=>$player, 'data'=>serialize(array('pos'=>$pos, 'pot'=>$pot, 'msg'=>$this->getUsername($player).' е победител в рунда.'))), true, true, true);

		error_log("p2:".$player);
		$this->beginNextRound($game, $deck, $roundAndStep['round'] + 1, $pos, $player);
	}

	function resetPlayersFolds($game)
	{
		$players = unserialize($this->db->getOne('SELECT players_list FROM games_list_mem WHERE id='.$game));

		foreach ($players as $k=>$v)
		{
			$players[$k]['fold'] = 0;
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);
	}

	function allPlayersFolded($game, $players, $player = 0)
	{
		$init = true;
		$ct = 0;

		if ($player == 0)
		{
			$init = false;
			$player = $_SESSION['UserID'];
		}

		foreach ($players as $k => $v)
		{
			if ($v['fold'] == 0)
				++$ct;

			if ($ct > 1)
			{
				error_log(json_encode($players));
				return false;
			}
		}

		return true;

		// TODO: EVENTUAL FAIL WITH INNER IF .. check if next player is last if initial $player != 0

//		if ($init)
//		{
//			if ($this->checkIfNextPlayerIsLast($game, $this->getNextPlayerId($game, $player)))
//				return true;

//		foreach ($players as $k=>$v)
//		{
//			if ($v['id'] != $player && $v['fold'] != 1)
//			{
//				if ($init)
//					return false;
//				else
//				{
//					if ($this->checkIfNextPlayerIsLast($game, $this->getNextPlayerId($game, $player)))
//					{
//						return true;
//					}
//				}
//			}
//		}

		return true;
	}

	function playerIsLast($players, $player = 0)
	{
		$last = true;
		$found = false;

		if ($player == 0)
			$player = $_SESSION['UserID'];

		foreach($players as $k => $v)
		{
			if ($found)
				$last = false;

			if ($v['id'] == $player)
			{
				$found = true;
			}
		}

		return $last;
	}

	function getGameInvitations($game)
	{
		$inv = $this->db->getAll('SELECT * FROM game_'.$game.' WHERE code=4 GROUP BY target');

		return $inv;
	}

	function getGamePlaces($game)
	{
		$cap = $this->db->getOne('SELECT players_cap FROM games_list_mem WHERE id='.$game);

		return $cap;
	}

	function findFreePosition($game)
	{
		$invitations = $this->getGameInvitations($game);
		$places = $this->getGamePlaces($game);
		$found = false;

		if ($places > count($invitations))
		{
			for ($i = 1; $i <= $places; ++$i)
			{
				$taken = false;
				$found = false;

				foreach ($invitations as $k => $v)
				{
					$v['data'] = unserialize($v['data']);

					if ($v['target'] == $_SESSION['UserID'])
					{
						$found = true;
						return true;
					}

					if ($v['data']['pos'] == $i)
						$taken = true;
				}

				if (!$found && !$taken)
				{
					$this->sendGameInvitation(0, $_SESSION['UserID'], $game, $i);  // dummy invitation
					$gameData = array('pos'=>$i, 'msg'=>$_SESSION['userData']['username'].' се присъедини към играта.');
					$this->insert('game_'.$game, array('code'=>4, 'target'=>$_SESSION['UserID'], 'player'=>$_SESSION['UserID'], 'data'=>serialize($gameData)), true, true, true);

					return true;
				}
			}
		}

		return false;
	}

	function chat($game, $msg)
	{
		$this->insert('game_'.$game, array('code'=>16, 'target'=>0, 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('msg'=>$_SESSION['userData']['username'].' каза: '.$msg))), true, true, true);
	}

	function playOnTheDark($game)
	{
		$roundAndStep = $this->getRoundAndStep($game);
		$this->insert('game_'.$game, array('code'=>17, 'target'=>0, 'round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('msg'=>$_SESSION['userData']['username'].' избра да играе на тъмно.'))), true, true, true);

		$players = $this->getPlayersData($game);

		foreach ($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
			{
				$tmp = $v;
				$players[$k] = array_pop($players);
				$players[] = $tmp;
			}
		}
	}

	function checkIfLastPlayerPlayedOnDark($game, $players)
	{
		$last = null;

		foreach ($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
			{
				break;
			}
			else
				$last = $v;
		}

		if ($last != null && $last['dark'] == 1)
		{
			return true;
		}
		elseif ($players[0]['id'] == $_SESSION['UserID'])
		{
			if ($players[count($players) - 1]['dark'] == 1)
				return true;
		}

		return false;
	}

	function checkIfPlayerIsFirst($players)
	{
		$first = true;

		foreach ($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
			{
				return $first;
			}

			$first = false;
		}
	}

	function checkIfThereAreBets($game, $round)
	{
		$bets = $this->db->getAll('SELECT * FROM game_'.$game.' WHERE round='.$round.' AND code=10 AND player!='.$_SESSION['UserID'].' ORDER BY id DESC');

		if (count($bets) > 0)
			return true;

		return false;
	}

	function checkIfPlayerHasEnoughMoneyForBet($game, $player, $enter = false)
	{
		$lastBet = $this->getLastBet($game);
		$credits = $this->getPlayersCredits($game);
		$roundAndStep = $this->getRoundAndStep($game);
		$players = $this->getPlayersData($game);
		$dark = $this->checkIfLastPlayerPlayedOnDark($game, $players);
		$viewed = $this->checkIfPlayerViwedCards($game, $players);

		if ($dark && !$viewed)
			$lastBet *= 2;

		if ($enter)
		{
			$fl = $this->getLastRoundSvarkaFloor($game, $roundAndStep['round']);

			if ($fl > $credits)
				return false;
		}
		else
		{
			if ($lastBet <= 0)
			{
				$lastBet = 10;	// MIN BET - must be dynamic ...-it is now
			}

			if ($lastBet > $credits)
				return false;
		}

		return true;
	}

	function checkIfPlayerViwedCards($game, $players)
	{
		foreach ($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
			{
				if ($v['dark'] == 0)
					return true;
			}
		}

		return false;
	}

	function getPlayerCredits($players)
	{
		foreach ($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
				return $v['credits'];
		}

		return 0;
	}

	function getLastRoundSvarkaFloor($game, $round)
	{
		$dt = $this->db->getOne('SELECT data FROM game_'.$game.' WHERE round='.$round.' AND code=20 ORDER BY id DESC');
		$dt = unserialize($dt);
		return $dt['floor'];
	}

	function enterRound($game, $bet)
	{
		$roundAndStep = $this->getRoundAndStep($game);

		$players = $this->getPlayersData($game);

		foreach ($players as $k=>$v)
		{
			if ($v['id'] == $_SESSION['UserID'])
			{
				$players[$k]['credits'] = $v['credits'] - $bet;
				unset($players[$k]['sfold']);
			}
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);

		$this->insert('game_'.$game, array('code'=>21, 'target'=>0, 'pot'=>$bet, 'round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('msg'=>$_SESSION['userData']['username'].' влезна в рунда с '.$bet.' кредита.'))), true, true, true);
		$this->nextPlayerTurn($game, $_SESSION['UserID']);
	}

	function getGameCondition($game)
	{
		$roundAndStep = $this->getRoundAndStep($game);
		$players = $this->getPlayersData($game);
		foreach ($players as $k => $v)
			if ($v['id'] == $_SESSION['UserID'])
				$player = $v;

		// CONTROL BOX UNITS
		$buttons = array(
			"answer" 	=> "btn_answer",
			"rise" 		=> "btn_rise",
			"bdark" 	=> "btn_dark",
			"bet" 		=> "btn_bet",
			"view" 		=> "btn_view",
			"rslider" 	=> array (
							"name" 	=> "rslider",
							"start" => 0,
							"end" 	=> 100
						),
			"fold" 		=> "btn_fold",
			"open"		=> "btn_open",
			"enter"		=> array (
							"name" 	=> "enter",
							"val" => 0
						)
		);

		// SELECTED CONTROLS ACCORDING TO GAME CONDITIONS
		$selectedButtons = array();

		if ($roundAndStep['step'] > 1)
			$playerIsFirst = false;
		else
			$playerIsFirst = $this->checkIfPlayerIsFirst($players);

		$playerHasEnoughMoney = $this->checkIfPlayerHasEnoughMoneyForBet($game, $_SESSION['UserID']);
		$playerViewedCards = $this->checkIfPlayerViwedCards($game, $players);

		// USED IN JS TO CHECK IF PLAYER VIEWED CARDS AND SHOW HIM BETTING CONTROLS DIRECTLY
		$selectedButtons['viewed'] = array("name"=>"viewed", "val"=>$playerViewedCards);

		if (isset($player['sfold']) && $player['sfold'] == 1)
		{
			$playerHasEnoughMoney = $this->checkIfPlayerHasEnoughMoneyForBet($game, $_SESSION['UserID'], true);
			$selectedButtons[] = $buttons['view'];
			$selectedButtons[] = $buttons['fold'];

			if ($playerHasEnoughMoney)
			{
				$selectedButtons['enter'] = $buttons['enter'];
				$selectedButtons['enter']['val'] = $this->getLastRoundSvarkaFloor($game, $roundAndStep['round']);
			}

			echo json_encode($selectedButtons);

			exit();
		}

		if (!$playerIsFirst)
		{
			$thereAreBets = $this->checkIfThereAreBets($game, $roundAndStep['round']);
			$prevPlayedOnDark = $this->checkIfLastPlayerPlayedOnDark($game, $players);
			if ($prevPlayedOnDark)
			{
				if ($playerHasEnoughMoney)
				{
					if ($playerViewedCards)
					{
						$selectedButtons[] = $buttons['answer'];
						$selectedButtons['rslider'] = $buttons['rslider'];
						$selectedButtons['rslider']['start'] = $this->getLastBet($game);
						$selectedButtons['rslider']['end'] = $this->getPlayerCredits($players);
						$selectedButtons[] = $buttons['bet'];

					}
					else
					{
						$selectedButtons[] = $buttons['bdark'];
						$selectedButtons[] = $buttons['view'];
						$selectedButtons['rslider'] = $buttons['rslider'];
						$selectedButtons['rslider']['start'] = $this->getLastBet($game)*2;
						$selectedButtons['rslider']['end'] = $this->getPlayerCredits($players);
						$selectedButtons[] = $buttons['bet'];
					}
				}
				else
				{
					if ($roundAndStep['step'] <= count($players))
					{
						if ( !isset($player['lighted']) || $player['lighted'] == 0)
							$selectedButtons[] = $buttons['open'];
					}
					$selectedButtons[] = $buttons['view'];
				}
			}
			else if ($thereAreBets)
			{
				if ($playerHasEnoughMoney)
				{
					$selectedButtons[] = $buttons['answer'];
					$selectedButtons['rslider'] = $buttons['rslider'];
					$selectedButtons['rslider']['start'] = $this->getLastBet($game);
					$selectedButtons['rslider']['end'] = $this->getPlayerCredits($players);
					$selectedButtons[] = $buttons['bet'];
				}
				else
				{
					if ($roundAndStep['step'] <= count($players))
					{
						if ( !isset($player['lighted']) || $player['lighted'] == 0)
							$selectedButtons[] = $buttons['open'];
					}
				}
				$selectedButtons[] = $buttons['view'];
			}
			else
			{
				if ($playerHasEnoughMoney)
				{
					$selectedButtons['rslider'] = $buttons['rslider'];
					$selectedButtons['rslider']['start'] = $this->getLastBet($game);
					$selectedButtons['rslider']['end'] = $this->getPlayerCredits($players);
					$selectedButtons[] = $buttons['bet'];
				}
				else
				{
					if ($roundAndStep['step'] <= count($players))
					{
						if ( !isset($player['lighted']) || $player['lighted'] == 0)
							$selectedButtons[] = $buttons['open'];
					}
				}
				$selectedButtons[] = $buttons['view'];
			}
		}
		else	// NE SE ZNAE DALI SHTE SMQTA KATO HORATA AKO PREDISHNITE SA IGRALI NA TYMNO
		{
				if ($playerHasEnoughMoney)
				{
					$selectedButtons['rslider'] = $buttons['rslider'];

					if (!$playerViewedCards)
					{
						$selectedButtons[] = $buttons['bdark'];
						$selectedButtons['rslider']['start'] = $this->getLastBet($game)*2;
					}
					else
						$selectedButtons['rslider']['start'] = $this->getLastBet($game);

					$selectedButtons['rslider']['end'] = $this->getPlayerCredits($players);
					$selectedButtons[] = $buttons['bet'];
				}
				else
				{
					if ($roundAndStep['step'] <= count($players))
					{
						if ( !isset($player['lighted']) || $player['lighted'] == 0)
							$selectedButtons[] = $buttons['open'];
					}
				}

				$selectedButtons[] = $buttons['view'];
		}

		$selectedButtons[] = $buttons['fold'];

		echo json_encode($selectedButtons);
	}

	function foldBellowVal($game, $val, $players, $hands)
	{
		$tmpID = array();
		foreach ($hands as $k => $v)
		{
			if ($v < $val)
				$tmpID[] = $k;
		}

		foreach ($players as $k => $v)
		{
			if (in_array($v['id'], $tmpID))
				$players[$k]['sfold'] = 1;
		}

		if (count($tmpID) > 0)
		{
			$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
			$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		}
	}

	function foldPlayersOnLightedRound($game, $players)
	{
		$cards = $this->getPlayersCards($game);
		$tmp = array();

		foreach ($cards as $k => $v)
		{
			if ($k != $_SESSION['UserID'])
				if ($cards[$_SESSION['UserID']]['sum'] > $v['sum'])
					$tmp[] = $k;
		}

		foreach ($players as $k => $v)
		{
			if (in_array($v['id'], $tmp))
				$players[$k]['fold'] = 1;
		}

		return $players;
	}

	function playOnLight($game)
	{
		$roundAndStep = $this->getRoundAndStep($game);

		$players = $this->getPlayersData($game);
		$credits = $this->getPlayerCredits($players);

		$this->insert('game_'.$game, array('code'=>19, 'target'=>0, 'round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('msg'=>$_SESSION['userData']['username'].' избра да играе на светло.'))), true, true, true);

		$stat = 2;
		$pos = $this->getPlayerPosition($_SESSION['UserID'], $game);
		$this->insert('game_'.$game, array('round'=>$roundAndStep['round'], 'step'=>$roundAndStep['step'], 'bet_status'=>$stat, 'pot'=>$credits, 'code'=>10, 'target'=>0, 'player'=>$_SESSION['UserID'], 'data'=>serialize(array('pos'=>$pos, 'bet'=>$credits, 'msg'=>$this->getUsername($_SESSION['UserID']).' заложи '.$credits.' чипa.'))), true, true, true);

		foreach ($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
			{
				$players[$k]['open'] = 1;
				$players[$k]['lighted'] = 1;
				$players[$k]['credits'] -= $credits;
			}
		}

		$players = $this->foldPlayersOnLightedRound($game, $players);

		if ($this->checkIfPlayerIsFirst($players))
		{
			$fl = 0;
			foreach ($players as $k => $v)
			{
				if ($v['fold'] == 1)
					++$fl;
			}

			if ($fl == (count($players) - 1))
			{
				$this->clearFoldings($game);
				$this->endCycle($game);
			}
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);

		$last = $this->getLastToAnswer($game, $roundAndStep['round']);

		if ($last['data']['lta'] == $_SESSION['UserID'])
		{
			$this->resolveRound($game);
		}
		else
		{
			$this->nextPlayerTurn($game);
		}
	}

	function playerLeave($game, $player)
	{
		$data = array();
		$tmpPlayers = array();

		$players = $this->getPlayersData($game);
		$roundAndStep = $this->getRoundAndStepLeaving($game);
		$pos = $this->getPlayerPosition($player, $game);

		foreach ($players as $k => $v)
		{
			if ($v['id'] == $_SESSION['UserID'])
			{
				$players[$k]['left'] = 1;
//				$players[$k]['fold'] = 1;
			}
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);

		if ($this->thisPlayerTurn($game, $player))
		{
			$this->playerFold($game, $player);
			error_log("playerLeave: first fold");

//			if ($this->thisPlayerTurn($game, $player))
//			{
//				$this->playerFold($game, $player);
//				error_log("playerLeave: second fold");
//			}
		}

		$this->db->Execute('UPDATE games_list SET players_list=\''.serialize($players).'\' WHERE id='.$game);
		$this->db->Execute('UPDATE games_list_mem SET players_list=\''.serialize($players).'\' WHERE id='.$game);
	}

	function thisPlayerTurn($game, $player)
	{
		$currentPlayer = $this->db->getOne('SELECT player FROM game_'.$game.' WHERE code=9 ORDER BY id DESC');

		if (isset($currentPlayer))
		{
			if ($currentPlayer == $player)
				return true;

			return false;
		}

		return false;
	}
}
?>