<?php 
	class staticModel extends Models{

		function getAllStatic($method = "getAssoc", $limit = 0){
			$sql = "SELECT id as id2, id, title, content, date_entered FROM static ORDER BY title ASC";
			if($method == "pagingQuery") return $this->$method($sql, $limit);
			return $this->db->$method($sql);
		}
		
		function insertStatic($arr){
			global $now;
			$arr['date_entered'] = $now;
			$this->insert("static", $arr, false, true ,true);
			return $this->insertID();
		}
		
		function editStatic($arr, $static_id){
			$this->update("static", $arr, "id = '{$static_id}'", false, true, true);
		}
		
		function getStaticById($static_id){
			return $this->db->getRow("SELECT id, title, content, date_entered FROM static WHERE id = '{$static_id}'");
		}
		
	}