<?
class messagesModel extends Models {
	function getReplyMessage($msg_id, $user_id)
	{
		$msg = $this->db->getRow('SELECT title, date_sent, text FROM messages WHERE id='.$msg_id.' AND sender_id='.$user_id);

		return $msg;
	}

	function sendMessage($arr)
	{
		$arr['title'] = addslashes($arr['title']);
		$arr['text'] = addslashes($arr['text']);
		$arr['sender_id'] = $_SESSION['UserID'];
		$arr['date_sent'] = date('Y-m-d H:i:s');

		$this->insert('messages', $arr, true, true);
	}

	function getMessages($type = 0)
	{
		if ($type == 0)
			$msgs = $this->db->getAll('SELECT * FROM messages WHERE reciever_id='.$_SESSION['UserID'].' ORDER BY id DESC');
		else
			$msgs = $this->db->getAll('SELECT * FROM messages WHERE sender_id='.$_SESSION['UserID'].' ORDER BY id DESC');

		foreach ($msgs as $k => $v)
		{
			$msgs[$k]['username'] = $this->getUsername($v['sender_id']);
		}

		return $msgs;
	}

	function getAvatar($id){
		return $this->db->getOne("SELECT avatar FROM users WHERE id = '{$id}'");
	}

	function parseEmotes($text = '')
	{
		global $emotes;

		foreach($emotes as $k => $v)
		{
			$text = str_replace($k, '<img src="/i/emoticons/'.$v.'" alt="'.$k.'" />', $text);
		}

		return $text;
	}

	function read($id)
	{
		$msg = $this->db->getRow('SELECT * FROM messages WHERE id='.$id.' AND (reciever_id='.$_SESSION['UserID'].' OR sender_id='.$_SESSION['UserID'].')');

		if ( $_SESSION['UserID'] == $msg['reciever_id'])
		{
			$av = $this->getAvatar($msg['sender_id']);

			if (isset($av) && strlen($av) > 0)
				$msg['avatar'] = USER_IMG_PATH.intval($msg['sender_id']/1000).'/l_'.$av.'.jpg';

			if ( $msg['sender_id'] > 0)
				$msg['user'] = $this->getUsername($msg['sender_id']);
			else
				$msg['user'] = 'Администрация';
		}
		else
		{
			$av = $this->getAvatar($msg['reciever_id']);

			if (isset($av) && strlen($av) > 0)
				$msg['avatar'] = USER_IMG_PATH.intval($msg['reciever_id']/1000).'/l_'.$av.'.jpg';

			$msg['user'] = $this->getUsername($msg['reciever_id']);
		}

		$msg['text'] = $this->parseEmotes($msg['text']);

		$this->markAsRead($id);

		return $msg;
	}

	function markAsRead($id)
	{
		$this->db->Execute('UPDATE messages SET `read`=1 WHERE id='.$id);
	}
}

?>