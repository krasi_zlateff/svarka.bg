<?php

require_once('adodb/adodb.inc.php');

class SQLConnection extends ADOConnection {

    function SQLConnection($server = DB_HOST, $user = DB_USER, $pwd = DB_PASS, $db = DB_NAME, $type = 'mysqli') {
        global $ADODB_CACHE_DIR;
        $GLOBALS['db'] = & ADONewConnection($type);
        $GLOBALS['db']->Connect($server, $user, $pwd, $db);
        $GLOBALS['db']->Execute("SET NAMES " . DEFAULT_CHARSET);
        $ADODB_CACHE_DIR = CACHE_PATH . 'db/';
    }

}

?>