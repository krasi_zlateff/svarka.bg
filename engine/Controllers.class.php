<?php
//COMMON METHODS FOR Controllers ARE HERE

if($_POST['no_smarty'] == 1){
	class Smarty{}
}else {
    require_once(dirname(__FILE__)."/smarty/libs/Smarty.class.php");
}


class Controllers extends Smarty {
	function Controllers() {
		$this->template_dir = TEMPLATE_DIR;
		$this->cache_dir = CACHE_DIR;
		$this->compile_dir = COMPILE_DIR;
		if($_POST['no_smarty']) $this->no_smarty = 1;
	}
	
	public function __get($var){
		
		if(!is_object($this->$var)){
			if(is_file(LOCALPATH.'/models/'.$var.'.model.php')){
				require_once(LOCALPATH.'/models/'.$var.'.model.php');
				$modelName = $var.'Model';
				$this->$var = new $modelName;
				return $this->$var;
			}
		}else{
			return $this->$var;
		}
		
	}
	
	function assignVariables($core){
		
		$this->assign('controllerRun', $core->controller);
		$this->assign('methodRun', $core->method);
		$this->assign('message', $core->Message());
		$this->assign('WEBPATH', WEBPATH);
		$this->assign('SITENAME', SITENAME);
		$this->assign('IMAGEPATH', IMAGEPATH);
		$this->assign('USER_HOME_PAGE', USER_HOME_PAGE);
		$this->assign('MAIN_DIR_LABEL', MAIN_DIR_LABEL);
		$this->assign('USER_IMG_PATH', USER_IMG_PATH);
		$this->assign('users_with_avatar', $this->getUsersWithAvatar(7));
		$this->assign('tournamentsCount', $this->countTournaments());
		if (isset($this->PageTitle)) $this->PageTitle[0] = strtoupper($this->PageTitle[0]);
		$this->assign('PAGE_TITLE', (isset($this->PageTitle) ? $this->PageTitle : DEFAULT_PAGE_TITLE));
		//ASSIGN VARIABLE 'body' WHICH CONTAINS THE TEMPLATE
		if (isset($_SESSION['UserID'])) $this->assign('countInbox', $this->countInbox());
		$this->assign('body', $this->fetch($this->templatePath, md5($_SERVER['REQUEST_URI'].$_SESSION['SID'])));
		
	}

	function GetIP(){
      	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		    //check ip from share internet
		    {
		      $ip = $_SERVER['HTTP_CLIENT_IP'];
		    }
    	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    		//to check ip is pass from proxy
		    {
		      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		    }
    	else
		    {
		      $ip = $_SERVER['REMOTE_ADDR'];
		    }
		return $ip;
	}
	
	function isAdmin() {
		return ( $_SESSION['admin'] == 1 ) ? true: false;
	}

	function GetAutoLogin($code){
		return $GLOBALS['db']->GetRow("SELECT id, username, email, usertype, active, submenu, avatar FROM users WHERE md5_login = '{$code}' AND usertype <> 2");
	}
	
	function countTournaments()
	{
		return $GLOBALS['db']->getOne('SELECT COUNT(id) FROM tournaments');
	}
	
	function setOnline($id) {
		$GLOBALS['db']->Execute("UPDATE users SET last_login = NOW(), online = 1 WHERE id = '{$id}'");
	}
	
	function countInbox()
	{
		return $GLOBALS['db']->getOne('SELECT COUNT(id) FROM messages WHERE reciever_id='.$_SESSION['UserID'].' AND `read`=0');
	}
	
	function setLogedSession($user_info = array()){
		if(!is_array($user_info) || !(count($user_info) > 0)) return false;
		$_SESSION['UserID'] = $user_info['id'];
		$_SESSION['username'] = $user_info['username'];
		$_SESSION['email'] = $user_info['email'];
		$_SESSION['userData'] = $user_info;
		
	}
	
	function updateLastActivity(){
		global $now;
		$GLOBALS['db']->Execute("UPDATE users SET last_activity = '{$now}', online = 1 WHERE id = '{$_SESSION['UserID']}'");
	}
	
	function tryLogin(){
		if(!$_SESSION['try_login']){
			if(!$_SESSION['UserID']){
				
				$auto_login_q = mysql_escape_string($_COOKIE['user_md5']);
				if($auto_login_q){
					 $user_info = $this->GetAutoLogin($auto_login_q);
					 if(is_array($user_info) && count($user_info) > 0){
					 		if($this->setLogedSession($user_info)) $this->setOnline($user_info['id']);
					 }
			    }
			    
			}
			$_SESSION['try_login'] = 1;
		}
	}
	
	function isLogged() {
		return ( $_SESSION['UserID'] > 0 && isset($_SESSION['UserID']) ) ? true: false;
	}
	
	function checkLoggedAndRedirect($must_be_loged = true, $message = ""){
		
		if (!$this->isLogged() && $must_be_loged){
			if($message == "") $message = 'Трябва да сте логнат потребител, за да имате достъп до тази страница!';
			Core::Message($message, MSG_ERROR);
			$curUrl = $this->curPageURL();
			($curUrl != "" && $curUrl != WEBPATH) ? Core::Redirect(LOGIN_PAGE."?return_to={$curUrl}") : Core::Redirect(LOGIN_PAGE);
		}elseif($this->isLogged() && !$must_be_loged){
			if($message == "") $message = 'Вие вече сте логнат. Първо трябва да излезете за да имате достъп до тази страница!';
			Core::Message($message, MSG_ERROR);
			Core::Redirect(USER_HOME_PAGE);
		}
		
	}
	
	function curPageURL() {
	 $pageURL = 'http';
	 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 } else {
	  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 }
	 return $pageURL;
	}
	
	
	function deleteIds($table, $field, $ids){
		$GLOBALS['db']->Execute("DELETE FROM $table WHERE $field IN ($ids)");	
	}
	
	function is_email($email){
    	return preg_match("/[[:alnum:]_.-]+[@][[:alnum:]_.-]{2,}\\.[[:alnum:]_.-]{2,}/",$email);
	}
	
	function delPhotoPhysicaly($photo_name){
		global $picSizeArr;
		if($photo_name != ""){
			$directory = USER_IMG_PATH_PHYSICAL.LOCALSLASH.(int)($_SESSION['UserID']/1000);
			if(is_array($picSizeArr) && count($picSizeArr) > 0){
				foreach ($picSizeArr as $prefix=>$whArr){
					if(is_file($directory.LOCALSLASH.$prefix.$photo_name.".jpg")) unlink($directory.LOCALSLASH.$prefix.$photo_name.".jpg");
				}
			}
		}else{
			return false;
		}
	}
	
	function testArray(&$arr){
		return (is_array($arr) && count($arr) > 0 && !empty($arr)) ? true : false;
	}
	
	function checkDate($date, &$mktime = 0){
		$aDate_parts = preg_split("/[\s-]+/", $date);

		if(!checkdate(
	        $aDate_parts[1], // Month
	        $aDate_parts[2], // Day
	        $aDate_parts[0] // Year
    	)) return false;
    	
    	$time = preg_split("/[\s:]+/", $aDate_parts[3]);
    	if ($time[0] > -1 && $time[0] < 24 && $time[1] > -1 && $time[1] < 60){
    		$mktime = mktime(intval($time[0]), intval($time[1]), intval($time[2]), intval($aDate_parts[1]), intval($aDate_parts[2]), intval($aDate_parts[0]));
    		return true;
    	}
    	return false; 
	}
	
	function getCities() {
		return $GLOBALS['db']->getAssoc("SELECT	id, name FROM cities ORDER BY name ASC");
	}
	
	function getUsersWithAvatar($limit = 7){
		return $GLOBALS['db']->getAll("SELECT id, username, avatar FROM users WHERE avatar != '' ORDER BY last_activity DESC LIMIT $limit");
	}
	
}
?>