<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty lower modifier plugin
 *
 * Type:     modifier<br>
 * Name:     lower<br>
 * Purpose:  convert string to lowercase
 * @link http://smarty.php.net/manual/en/language.modifier.lower.php
 *          lower (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */
function smarty_modifier_url($text)
{
    //funkciq za premahvane na space-ovete ...za URL-tata

	$text = str_replace(" ","-",$text);
	$text = str_replace("%","-",$text);
	$text = str_replace(".","-",$text);
	$text = str_replace("\"","",$text);
	$text = str_replace("\'","",$text);
	$text = str_replace("----","-",$text);
	$text = str_replace("---","-",$text);
	$text = str_replace("--","-",$text);
	$text = str_replace(" ","-",$text);
	$text = strtolower($text);
	//$text = mb_strtolower($text,"WINDOWS-1251");
	$text = urlencode($text);
	return $text ;

}

?>
