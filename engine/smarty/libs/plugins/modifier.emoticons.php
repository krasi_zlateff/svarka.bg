<?
	   
function smarty_modifier_emoticons($string)
{
		global $_SMILESArray, $_SMILESImgArray;
		$target = htmlspecialchars(stripslashes($string));
		$target = nl2br($target);
		
		if ( isset($_SMILESArray) ) {
			foreach ($_SMILESArray as $key=>$smile) {
				if ( stristr($target, $smile) != "" ) {
					$smile_begin = $smile;
					$tmp_target = str_ireplace($smile, '<img src="/i/emotes/'.$_SMILESImgArray[$key].'" style="float: none" />', $target, $count);
					$smile = str_replace(array("(", ")", "|", '*', '^'), array("\(", "\)", "\|", '\*', '\^'), $smile);
					if ( $count >= 3 ) $target = preg_replace('/'.$smile.'/i', '<img src="/i/emotes/'.$_SMILESImgArray[$key].'" style="float: none" />', $target, 3);
					else{
						
						$target = preg_replace('/'.$smile.'/i', '<img src="/i/emotes/'.$_SMILESImgArray[$key].'" style="float: none" />', $target);
						
					}
					
					$target = str_replace($smile_begin, '', $target);
				}
			}			 
		}
		
$target = str_replace(array("&lt;", "&gt;"), array("<", ">"), $target);		
return $target;
	
}

?>