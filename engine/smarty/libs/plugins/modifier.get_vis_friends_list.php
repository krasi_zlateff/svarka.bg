<?php
/*
$allowed_people_user_ids - allowed_user_ids na snimkata
$user_id - potrebitelskoto id na pritejatelq na snimkata
*/
function smarty_modifier_get_vis_friends_list($allowed_people_user_ids, $user_id)
{
   if($_SESSION['UserID'] == $user_id && $allowed_people_user_ids != ""){
   	
   		$allowed_people_user_ids = rtrim($allowed_people_user_ids, ",");
   
   		$allowedUsersArr = $GLOBALS['db']->getAll("SELECT id, username FROM users WHERE id IN ($allowed_people_user_ids)");
   		if(is_array($allowedUsersArr) && count($allowedUsersArr) > 0){
	   		
	   		return $allowedUsersArr;
   		}
   	
   }
   return false;
   
}

?>
