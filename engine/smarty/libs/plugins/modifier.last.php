<?php

function smarty_modifier_last($str)
{
	$num = substr($str, -1);
	if ($num % 2 == 0){
		return 0;
	} else {
		return 1;
	}
	
}

/* vim: set expandtab: */

?>
