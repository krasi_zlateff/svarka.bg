<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty count_characters modifier plugin
 *
 * Type:     modifier<br>
 * Name:     count_characteres<br>
 * Purpose:  count the number of characters in a text
 * @link http://smarty.php.net/manual/en/language.modifier.count.characters.php
 *          count_characters (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @param boolean include whitespace in the character count
 * @return integer
 */
function smarty_modifier_specialURL($text, $include_spaces = false)
{
	 $text = trim($text);
	 $text= str_replace("/","",$text);
	 $text = str_replace("\\","",$text);
	 $text = str_replace(":","",$text);
	 $text = str_replace('"',"",$text);
	 $text = str_replace(",","",$text);
	 $text = str_replace("!","",$text);
	 $text = str_replace('"',"",$text);
	 $text = str_replace("'","",$text);
	 $text = str_replace("”","",$text);
	 $text = str_replace("“","",$text);
	 $text = str_replace(")","",$text);
	 $text = str_replace("(","",$text);
	 $text = str_replace("„","",$text);
	 $text = str_replace(".","",$text);
	 $text = str_replace("?","",$text);
	 $text = str_replace("&quot;","",$text);
	 $text = str_replace("&","",$text);
	 $text = str_replace("$","",$text);
	 
	 $text = str_replace(" ","-",$text);
	 $text = str_replace("----","-",$text);
	 $text = str_replace("---","-",$text);
	 $text = str_replace("--","-",$text);
	 $text = str_replace("%","-",$text);
	 $text = strtolower($text);
	 $text = mb_strtolower($text,"utf-8");
	 //$text = urlencode($text);
	 return $text ;

	//$str = str_replace(' ', '-', $string);
	//return (strtolower($text));
}

/* vim: set expandtab: */

?>
