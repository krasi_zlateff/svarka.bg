<?php

/**

* Smarty {html_select} plugin

*

* Type: function<br>

* Name: html_selectbr>

* Purpose: Prints the dropdowns for a <select> input box. Supports multiple

* selections as well.

*

* @link (none)

* @version 1.0 (12/31/05)

* @author Jason Stark

* @param array

* @param Smarty

* @return string

*

* ----------------

*

* Parameters: Those without default values should be required.

* name = The name attribute of the select tag: <select name="$name">

* size = The number of visible rows to show: <select size="$size">

* default: is not included

* blank = Whether to include a blank/null option at the beginning

* default: false

* extra = Any "extra" html attributes to include in the select tag.

* example: class="selectclass" style="width: 100%"

* default: is not included

* multiple = Whether this selection box allows multiple items to be

* selected. i.e.: <select multiple>

* default: false

* options = An array of keys and values which will be listed as options

* selected = The array key of the option that should be selected initially.

* If multiple=true, this can be an array of values, which

* would cause multiple values to be initially selected

* default: no option is listed as selected

*

* ----------------

*

* Examples:

*

* Assuming $a = array( [0] => 'zero',

* [1] => 'one',

* [2] => 'two',

* ['other'] => 'other' );

* $s = 1;

*

* {html_select name="varname" options=$a selected=$s}

*

* OUTPUT: <select name="varname">

* <option value="0">zero</option>

* <option value="1" selected>one</option>

* <option value="2">two</option>

* <option value="other">other</option>

* </select>

*

* $s = array(1,2);

*

* {html_select name="varname" options=$a selected=$s multiple="true"

* blank="true" extra="class=\"red\"" size="4"}

*

* OUTPUT: <select name="varname" multiple class="red" size="4">

* <option value="null"></option>

* <option value="0">zero</option>

* <option value="1" selected>one</option>

* <option value="2" selected>two</option>

* <option value="other">other</option>

* </select>

*

* You get the idea...

*/

function smarty_function_html_select($params, &$smarty)
{

require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

/* Default values. */

/* */

$name = "select_box";

/* <select size> of the <select> tag.

If not set, uses default dropdown.

Note that this is the number of rows the select will include. */

$size = null;

/* Whether to include a blank option (none) */

$blank = false;

/* Unparsed attributes for the <select> tag.

An example might be in the template: extra ='class ="foo"'. */

$extra = null;

/* Whether the select allows multiple items to be selected. */

$multiple = false;

/* Array of options in the select menu

* <option value="$key">$options[$key]</option> */

$options = null;

$only_first = 0;
$only_last = 0;
/* Array key of the option to be initially selected

* If multiple=true, this can be an array */

$selected = null;

foreach ($params as $_key=>$_value) {

switch ($_key) {

case 'name':

case 'size':

case 'extra':

$$_key = (string)$_value;

break;

case 'multiple':

case 'blank':

$$_key = (bool)$_value;

break;

case 'options':

case 'selected':

$$_key = $_value;

break;

case 'def':
$$_key = $_value;
break;

case 'only_first':
$$_key = $_value;
break;

case 'only_last':
$$_key = $_value;
break;


default:


if(!is_array($_value)) {

$extra_attrs .= ' '.$_key.'="'.smarty_function_escape_special_chars($_value).'"';

} else {

$smarty->trigger_error("select: extra attribute '$_key' cannot be an array", E_USER_NOTICE);

}

break;

}

}

$result = '<select name="'.$name.'"';

if ($multiple) {

$result .= ' multiple';

}

if (isset($size)) {

$result .= ' size="'.$size.'"';

}

if (isset($extra)) {

$result .= ' '.$extra;

}

$result .= '>';

if ( $def != "" ) {
	$result .= '<option value="0">'.$def.'</option>';
}
if ($blank) {

$result .= '<option value="null"';

if ($selected = null) {

$result .= ' selected';

}

$result .= '></option>';

}

$count = 1;
$total = @count($options);
$start_at = ($total-$only_last)+1;

foreach ($options AS $value => $text) {
$c++;
if ($c < $start_at && $start_at-1 != $total) continue;
$result .= '<option value="'.$value.'"';

if ( (is_array($selected) && array_search($value, $selected) !== FALSE)

|| (!is_array($selected) && $value == $selected) ) {

$result .= ' selected';

}

$result .= '>'.$text.'</option>';
if ( $only_first == $count ) break;

$count++;
 
}

$result .= '</select>';

return $result;
}

/* vim: set expandtab: */

?>