<?php

function smarty_modifier_get_main_photo_info($main_photo, $photo)
{
	
	$photo = unserialize(base64_decode($photo));
	
    if(is_array($photo)){
    	foreach ($photo as $v){
    		if($v['photo'] == $main_photo){
    			$main_photo_info_arr['id'] = $v['id'];
    			$main_photo_info_arr['url_id'] = $v['url_id'];
    			return $main_photo_info_arr;
    		}
    		
    	}	
    }
  
    return;
}


?>
