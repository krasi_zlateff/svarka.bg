<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty count_characters modifier plugin
 *
 * Type:     modifier<br>
 * Name:     count_characteres<br>
 * Purpose:  count the number of characters in a text
 * @link http://smarty.php.net/manual/en/language.modifier.count.characters.php
 *          count_characters (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @param boolean include whitespace in the character count
 * @return integer
 */

function FormatDate($year, $month, $day, $hour, $minute)
		     {	
			$mdays = array (
					"",
					"Януари",
					"Февруари",
					"Март",
					"Април",
					"Май",
					"Юни",
					"Юли",
					"Август",
					"Септември",
					"Октомври",
					"Ноември",
					"Декември");
		     if($minute < 10)
		     {
		     	$minute = "0".$minute;
		     }
		     $hour = ($hour < 10) ? "0".$hour : $hour;
		       if($hour && $minute){
		        $return = "$day ".$mdays[$month]." $year / $hour:$minute";
		       } else {
		       	$return = "$day ".$mdays[$month]." $year";
		       }
		        
		 return ($return);
}
function smarty_modifier_format_date($date_entered)
{

	    $date_entered = explode(" ", $date_entered);
	    $date = explode("-", $date_entered['0']);
	    $time = explode(":", $date_entered['1']);
        $month = $date['1'];
          if($month['0'] == 0){
          	$month = $month['1'];
          }
		return FormatDate($date['0'], $month, $date['2'], $time['1'], $time['2']);
}

/* vim: set expandtab: */

?>
