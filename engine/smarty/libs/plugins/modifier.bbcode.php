<?php

function smarty_modifier_bbcode($text) {
	
  			$text=preg_replace("/\[b\](.+?)\[\/b\]/s", '<b>$1</b>',$text);
			
			$text=preg_replace("/\[i\](.+?)\[\/i\]/s", '<i>$1</i>',$text);
			
			$text=preg_replace("/\[u\](.+?)\[\/u\]/s", '<u>$1</u>',$text);
			
			$text=preg_replace("/\[quote\](.+?)\[\/quote\]/s", '<table cellspacing="1" width="100%" border="0"><tr><td>Quote:</td></tr><tr><td width="100%" style="border:solid 1px black">$1</td></tr></table>',$text);
			
			$text=preg_replace("/\[quote\=(.+?)](.+?)\[\/quote\]/s", '<table cellspacing="1" width="100%" border="0"><tr><td>$1 каза:</td></tr><tr><td width="100%" style="border:solid 1px black">$2</td></tr></table>',$text);
			
			$text=preg_replace("/\[url\](.+?)\[\/url\]/s", '<a href="$1">$1</a>',$text);
			
			$text=preg_replace("/\[url\=(.+?)\](.+?)\[\/url\]/s", '<a href="$1">$2</a>',$text);
			
			$text=preg_replace("/\[img\](.+?)\[\/img\]/s", '<img src="$1" alt="User submitted image" title="User submitted image"/>',$text);
			
			$text=preg_replace("/\[color\=(.+?)\](.+?)\[\/color\]/s", '<span style="color:$1">$2</span>',$text);
			
			$text=preg_replace("/\[size\=(.+?)\](.+?)\[\/size\]/s", '<span style="font-size:$1">$2</span>',$text);
			
			$text=preg_replace("/\[code\](.+?)\[\/code\]/s",'<table cellspacing="1" width="100%" border="0"><tr><td>Code:</td></tr><tr><td width="100%" style="border:solid 1px black">$1</td></tr></table>',$text);
			
			
			
			$text = nl2br($text);
			return $text;
}

?>