<?
function smarty_block_dialog($params, $content, &$smarty)
{
    if (is_null($content)) {
        return;
    }
    
    $id = $params['id'];
	$title = $params['title'];
    
    $_output = '
	<div id="dialog_'.$id.'" title="'.$title.'" class="none">
		'.$content.'
	</div>';
    echo $_output;
}
?>