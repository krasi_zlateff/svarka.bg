<?php
function smarty_modifier_construct_url($url_id, $pic_id, $directory = "users_pics")
{
	global $imgPathArr;
	
	return $imgPathArr[$url_id]['WEBPATH']."/".$directory."/".(int)($pic_id/MAX_IN_MAIN_FOLDER)."/".(int)($pic_id/MAX_IN_FOLDER);		
}

?>
