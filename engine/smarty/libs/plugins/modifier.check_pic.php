<?php
/*
$test_photo - imeto na testvanata sinmka
$photo - serializenati vsi4ki snimki
$user_id - potrebitelskoto id na pritejatelq na snimkata
*/
function smarty_modifier_check_pic($test_photo, $photo, $user_id)
{
    if($test_photo == "") return false;
    
    if($user_id == "") return false;
    
    if($user_id == $_SESSION['UserID'] && $_SESSION['UserID'] > 0) return true;//moite snimki sa vinagi vidimi za men
    
    $photoArr = unserialize(base64_decode($photo));
   
    if(!is_array($photoArr) || !(count($photoArr) > 0)) return false;
    
    foreach ($photoArr as $value) {
    	if($test_photo == $value['photo']){
    		if($value['status'] != 1) return false;//ne e odobrena ot admina
    		
    		if($value['vis_mode'] == 1) return true;//vidima za vsi4ki
    		
    		if($value['vis_mode'] == 2){//samo ot moite priqteli
    			if((int)$_SESSION['UserID'] > 0){
    				$friendsArr = $GLOBALS['db']->getAll("SELECT source FROM relationships WHERE (source = '{$user_id}' AND dest = '{$_SESSION['UserID']}' AND status = 1) OR (source = '{$_SESSION['UserID']}' AND dest = '{$user_id}' AND status = 1)");
    				
	    			if(count($friendsArr) > 1) return true;
	    			
    			}
    			
    		}
    		 
    		if($value['vis_mode'] == 3){//samo ot men
    			 
    			if($_SESSION['UserID'] == $user_id){
    				
    				return true;
    				
    			}
    			
    		}
    		 
    		if($value['vis_mode'] == 4){//nqkoi priqteli
    			
    			$allowed_user_ids = explode(",", $value['allowed_user_ids']);
    			if(is_array($allowed_user_ids)){
	    			
    					if(in_array($_SESSION['UserID'], $allowed_user_ids) && $_SESSION['UserID'] > 0){
		    				
		    				return true;
		    				
		    			}elseif($_SESSION['UserID'] == 0){//izklu4va se ot nelognati
		    				
		    				return false;
		    				
		    			}
	    			
    			}
    			
    		}
    		 
    		return false;
    	}
    }
}

/* vim: set expandtab: */

?>
