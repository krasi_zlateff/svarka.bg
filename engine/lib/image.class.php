<?
class Image {
	
	function _setType($type) {
		switch($type) {
			case 1 : {
				$this->pic_type = "gif";
				return true;
			}
			case 2 : {
				$this->pic_type = "jpeg";
				return true;
			}
			case 3 : {
				$this->pic_type = "png";
				return true;
			}
			default : {
				$this->warning("Not valid image type!");
				return false;
			}
		}
	}
	
	function _imagecreatefrom($filename) {
		$_imagecreatefrom = "imagecreatefrom".$this->pic_type;
		$image = $_imagecreatefrom($filename);
		
		return $image;
	}
	
	function _image(&$image_o, $filename, $quality = 100) {
		$_image = "image".$this->pic_type;
		$_image($image_o, $filename, $quality);
	}
	
	function Resize_IMG($filename,$resize_width = 0,$resize_height = 0, $output, $pr = false) {
	
		if($resize_width < 1 && $resize_height < 1) return true;
		list($original_width, $original_height, $type) = getimagesize($filename);
		
		if($resize_width>$original_width && $resize_height>$original_height) return true;
		//	print 'bahta';
		$this->_setType($type);
	
		if($pr) {
			if($resize_width < 1) $resize_width = $original_width;
			$height = $original_height*($resize_width/$original_width);
			$width = $resize_width;
			if($resize_height > 0) {
				if($height>$resize_height) {
					$width = $original_width*($resize_height/$original_height);
					$height = $resize_height;
				}
			}
			if($width>$original_width) return true;
		} else {
			if($resize_width < 1) $resize_width = $original_width;
			if($resize_height < 1) $resize_height = $original_height;
			$width = $resize_width; 
			$height = $resize_height;
		}
		if($width < 1 || $height < 1) {
			$width = $original_width; 
			$height = $original_height;
		}
		
		$x = 0;
		$y = 0;	
		if(!$pr) {
			if ($original_width >= $width && $original_height >= $height) {
				if($original_height < $original_width) {
					$ratio = (double)($original_height / $height);
					$cpyWidth = $width * $ratio;
					if ($cpyWidth > $original_width) {
						$ratio = (double)($original_width / $width);
						$cpyWidth = $original_width;
						$cpyHeight = $height * $ratio;
						//$y = ($original_height - $cpyHeight) / 2;
					} else {
						$cpyHeight = $original_height;
						$x = ($original_width - $cpyWidth) / 2;
					}
				} else {
					$ratio = (double)($original_width / $width);
					$cpyHeight = $height * $ratio;
					if ($cpyHeight > $original_height) {
						$ratio = (double)($original_height / $height);
						$cpyHeight = $original_height;
						$cpyWidth = $width * $ratio;
						$x = ($original_width - $cpyWidth) / 2;
					} else {
						$cpyWidth = $original_width;
						//$y = ($original_height - $cpyHeight) / 2;
					}
				}
				$original_width = $cpyWidth;
				$original_height = $cpyHeight;
			} 
			else if($original_width<$width) {
				$width = $original_width*($height/$original_height);
				$height = $resize_height;
			} else {
				$height = $original_height*($width/$original_width);
				$width = $resize_width;
			}
		}
		
		$width = round($width);
		$height = round($height);
		$width = round($width);
		$height = round($height);
		$width = round($width);
		$height = round($height);
	
		$this->image = imagecreatetruecolor($width, $height);
		
		$handle = $this->_imagecreatefrom($filename);
		imagecopyresampled($this->image, $handle, 0, 0, $x, $y, $width, $height, $original_width, $original_height);
		imagedestroy($handle);
		if($width<=120) $quality = 100;
		else $quality = 100;
		$this->_image($this->image, $output, $quality);
		imagedestroy($this->image);

		return true;
	}
	
	function set_logo($filename) {
		list($width, $height, $type) = getimagesize($filename);
		$this->_setType($type);
		$_image = "image".$this->pic_type;
	  if ($this->pic_type <> 'gif' && $this->pic_type <> 'png'){
		$_imagecreatefrom = "imagecreatefrom".$this->pic_type;
		$image = $_imagecreatefrom($filename);

		$logo = imagecreatefrompng(LOCALPATH.'/engine/lib/images/logo.png');
		$width = $width - 125;
		if ($width <= 0) {
			$width = '0';
		} else {
			$width = $width/2;
		}
		$height = $height-($height-($height/2))-12;
		imagecopy($image,$logo,$width,$height,0,0,125,24);

		
		$_image($image, $filename);
	  } else {
//	        $img = ImageCreateFromGif($filename);
//            //Convert to PNG and send it to browser
//			if($img) {
//
//			 $image = ImagePNG($img, $filename);
//			 //Clean-up memory
//			 ImageDestroy($img);
//			}
//		    $image = ImageCreateFromPNG($filename); 
//			$logo = imagecreatefrompng(LOCALPATH.'/engine/lib/images/logo.png');
//			$width = $width - 125;
//			if ($width <= 0) {
//				$width = '0';
//			} else {
//				$width = $width/2;
//			}
//			$height = $height-($height-($height/2))-12;
//			imagecopy($image,$logo,$width,$height,0,0,125,24);
//	
//			
//			ImagePNG($image, $filename);
	  	
	  }
		unset($image);
		unset($logo);
	
	}
	
	function set_logo_small($filename) {
		list($width, $height, $type) = getimagesize($filename);
		$this->_setType($type);
		$_image = "image".$this->pic_type;
	  if ($this->pic_type <> 'gif'){
		$_imagecreatefrom = "imagecreatefrom".$this->pic_type;
		$image = $_imagecreatefrom($filename);

		$logo = imagecreatefrompng(LOCALPATH.'/engine/lib/images/mark_small.png');
		$width = $width - 100;
		if ($width <= 0) {
			$width = '0';
		} else {
			$width = $width/2;
		}
		$height = $height-($height-($height/2))-9;
		imagecopy($image,$logo,$width,$height,0,0,100,18);

		
		$_image($image, $filename);
	  }
		unset($image);
		unset($logo);
	
	}
	
  function smart_resize_image( $file, $width = 0, $height = 0, $output = 'file', $proportional = false, $delete_original = false, $use_linux_commands = false )
  {
    if ( $height <= 0 && $width <= 0 ) {
      return false;
    }
 
    $info = getimagesize($file);
    $image = '';
 
    $final_width = 0;
    $final_height = 0;
    list($width_old, $height_old) = $info;
    
 	if($width_old <= $width && $height_old <= $height) return false;
 	
    if ($proportional) {
      if ($width == 0) $factor = $height/$height_old;
      elseif ($height == 0) $factor = $width/$width_old;
      else $factor = min ( $width / $width_old, $height / $height_old);   
 
      $final_width = round ($width_old * $factor);
      $final_height = round ($height_old * $factor);
 
    }
    else {
      $final_width = ( $width <= 0 ) ? $width_old : $width;
      $final_height = ( $height <= 0 ) ? $height_old : $height;
    }
 
    switch ( $info[2] ) {
      case IMAGETYPE_GIF:
        $image = imagecreatefromgif($file);
      break;
      case IMAGETYPE_JPEG:
        $image = imagecreatefromjpeg($file);
      break;
      case IMAGETYPE_PNG:
        $image = imagecreatefrompng($file);
      break;
      default:
        return false;
    }
 
    $image_resized = imagecreatetruecolor( $final_width, $final_height );
 
    if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
      $trnprt_indx = imagecolortransparent($image);
 
      // If we have a specific transparent color
      if ($trnprt_indx >= 0) {
 
        // Get the original image's transparent color's RGB values
        $trnprt_color    = imagecolorsforindex($image, $trnprt_indx);
 
        // Allocate the same color in the new image resource
        $trnprt_indx    = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
 
        // Completely fill the background of the new image with allocated color.
        imagefill($image_resized, 0, 0, $trnprt_indx);
 
        // Set the background color for new image to transparent
        imagecolortransparent($image_resized, $trnprt_indx);
 
 
      } 
      // Always make a transparent background color for PNGs that don't have one allocated already
      elseif ($info[2] == IMAGETYPE_PNG) {
 
        // Turn off transparency blending (temporarily)
        imagealphablending($image_resized, false);
 
        // Create a new transparent color for image
        $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
 
        // Completely fill the background of the new image with allocated color.
        imagefill($image_resized, 0, 0, $color);
 
        // Restore transparency blending
        imagesavealpha($image_resized, true);
      }
    }
 
    imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
 
    if ( $delete_original ) {
      if ( $use_linux_commands )
        exec('rm '.$file);
      else
        @unlink($file);
    }
 
    switch ( strtolower($output) ) {
      case 'browser':
        $mime = image_type_to_mime_type($info[2]);
        header("Content-type: $mime");
        $output = NULL;
      break;
      case 'file':
        $output = $file;
      break;
      case 'return':
        return $image_resized;
      break;
      default:
      break;
    }
 
    switch ( $info[2] ) {
      case IMAGETYPE_GIF:
        imagegif($image_resized, $output);
      break;
      case IMAGETYPE_JPEG:
        imagejpeg($image_resized, $output, 100);
      break;
      case IMAGETYPE_PNG:
        imagepng($image_resized, $output);
      break;
      default:
        return false;
    }
 
    return true;
  }
	
}
?>