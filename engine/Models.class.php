<?php
class Models  {
	
	public $no_paging_arrows = false;
	public $show_pages = 10;
	
	function Models() {
 		$this->db =& $GLOBALS['db'];
	}
	
	function getUsername($id)
	{
		$username = $this->db->getOne('SELECT username FROM users WHERE id='.$id);
		
		return $username;
	}
	
 
	function pagingQuery($sql,$limit, $no_arrows = false, $show_pages = 10) {
		$pos = strpos($sql,"select distinct");
		if($pos === false) {
			$pos = strpos(strtolower($sql),"select");
			if($pos === false) return array();
			$before = substr($sql,0,$pos+6);
			$sql = substr($sql,$pos+6);
		} else {
			$before = substr($sql,0,$pos+15);
			$sql = substr($sql,$pos+15);
		}
		$sql = $before." SQL_CALC_FOUND_ROWS ".$sql;
		$this->no_paging_arrows = $no_arrows;
		$this->show_pages = $show_pages;
		return $this->queryPagesSimple($sql,$limit);
	}

	function queryPagesSimple($sql,$limit) {
		$this->page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
		if($this->page < 1) $this->page = 1;
		$offset = ($this->page-1)*$limit;

		$sql .= " LIMIT $offset, ".$limit;
		$arr = $this->db->getAll($sql);
		
		$this->countAll = $this->db->GetOne("SELECT FOUND_ROWS()");
		if($this->page>1 && $this->countAll<=$offset) Core::Redirect('?page=1');

		$this->pages = false;
		if(count($arr)>0) $this->pages = $this->NPages($this->countAll,$limit);
		return $arr;
	}
	
	function NPages($numimages,$ImagePerPage = 10, $redirect = '') {
		if($ImagePerPage<1) $ImagePerPage = 1;
		
		$PrevImagePath = "<";
		$NextImagePath = ">";
		$FirstImagePath = "<<";
		$LastImagePath = ">>";
		
		
		$page = $this->page;
		if($page<1) $page = 1;
		$pages = ceil($numimages / $ImagePerPage);
		$ret = "";
		if($pages > 1) {
			$page1 = $page % $this->show_pages;
			$d = $page - $page1;
			$pflag = 0;
			$j = 0;
			$ret = '<ul class="pages">';
			if(($pages - $this->show_pages) < ($page - 4) && ($pages - $this->show_pages) > 0) { $t = $page - $pages + 5; }
			if($pages > $this->show_pages) $begin = $page - 4 - $t ;
			else $begin = 1;
			if($page > 5) $end = $page + 5 + $j ;
			else $end = $this->show_pages;
			for($i=$begin; $i<=$end && $i<=$pages; $i++) {
				if ($i <= 0) { $j++; continue; }
				if ($page>1 && $pflag == 0) {
					$pflag = 1;
					$prev = $page-1;
					if ( !$this->no_paging_arrows ) $ret .=  "<li><span><a href=\"".'?page=1'."\">".$FirstImagePath."</a></span></li>";
					if ( !$this->no_paging_arrows ) $ret .=  "<li><span><a href=\"".'?page='.$prev."\">".$PrevImagePath."</a></span></li>";
				}
				if ($page != $i) {
					$ret .=  "<li class=\"num\"><span><a href=\"".'?page='.$i."\"><strong>$i</strong></a></span></li>";
				}
				if ($page == $i) { $ret .=  "<li class=\"num-selected\"><span>$i</span></li>";}
			}
			if ($page != $pages) {
				 $next = $page+1;
				 if ( !$this->no_paging_arrows ) $ret .=  "<li><span><a href=\"".'?page='.$next."\">".$NextImagePath."</a></span></li>";
				 if ( !$this->no_paging_arrows ) $ret .=  "<li><span><a href=\"".'?page='.$pages."\">".$LastImagePath."</a></span></li>";
			}
			$ret .=  "";
		} 
		return $ret;
	}

	function insert($table, $arrFields, $where=false, $forceUpdate=true,$magicq=false) {
		return $this->db->AutoExecute($table, $arrFields, "INSERT", $where, $forceUpdate,$magicq);
	}

	function update($table, $arrFields, $where=false, $forceUpdate=true,$magicq=false) {
		return $this->db->AutoExecute($table, $arrFields, "UPDATE", $where, $forceUpdate,$magicq);
	}

	
	function insertID() {
		return $this->db->Insert_ID();
	}
	
	function deleteFromTable($table, $where = ""){
		if($where != "") $where = "WHERE ".$where;
		$GLOBALS['db']->Execute("DELETE FROM $table $where");
	}
	
}

?>