<?php
class Core {
	function Core() {	}
	
	function ParseURL() {
		
		$url = (isset($_GET['address']) && $_GET['address'] != "") ? $_GET['address'] : "main";
		$url = str_replace('../','',$url);
		$url = str_replace('\0','',$url);
		$urlArr = explode('/', $url);
		$count = count($urlArr);
		$controller = $urlArr[0];
		$function = $urlArr[1];		
		$includepath = '';
		$this->admin = false;
		$tplcontroller = $controller;
		$this->tplfunction = ( $function != '' ) ? $function : 'index';
		if ( count($_POST) > 0 ) $isPostBack = true; else $isPostBack = false;
		if ( $controller == 'admin' && $_SESSION['admin'] === 1) {
			$tplcontroller = $function;
			$controller = 'admin_'.$function;
			$function = $urlArr[2];
			if ( $function == '' ) $this->tplfunction = 'admin_index';
			$this->tplfunction = ( $function != '' ) ? 'admin_'.$function : 'admin_index';
			$includepath = '/admin/';
			array_shift($urlArr);
			$this->admin = true;
		}
		if ( $_SESSION['redirect'] ) {
			$_GET = $_SESSION['redirect']['get'];
			$_POST = $_SESSION['redirec']['post'];
		}
		unset($urlArr[0]);
		unset($urlArr[1]);
		$parameters = $urlArr;
 
		if ( is_file(LOCALPATH.LOCALSLASH.'controllers'.LOCALSLASH.$includepath.$controller.'.controller.php')) { // controller exists
			require(LOCALPATH.LOCALSLASH.'controllers'.LOCALSLASH.$includepath.$controller.'.controller.php');
			$controllerClassName = $controller.'Controller';
			$controllerClass = new $controllerClassName;
			$controllerClass->isPostBack =  $isPostBack;
			
			if (method_exists($controllerClass, $function)) { // method exists
				$this->method = $function;
				call_user_func_array(array($controllerClass, $this->method), $parameters); 
			}
			else { 
				// try to send parameter to index function
				if (method_exists($controllerClass, 'index')) {
					call_user_func_array(array($controllerClass, 'index'), $parameters);
				}else $this->Redirect("/");
				
				$function = 'index';
				if ( $includepath == '/admin/') $this->tplfunction = 'admin_index';
				else $this->tplfunction = 'index';
			}
		   
			$this->tplfunction = ( $controllerClass->force_template != '' ) ? $controllerClass->force_template : $this->tplfunction;
			$controllerClass->templatePath = $tplcontroller.$includepath.LOCALSLASH.$this->tplfunction.'.tpl';
			$this->controller = $controller;
			$this->method = $function;
		}
		else 
		{
			// display index page
			
			if ( $this->admin ){
				require(LOCALPATH.LOCALSLASH.'controllers'.LOCALSLASH.'admin'.LOCALSLASH.'admin_main.controller.php');
				$controllerClass = new admin_mainController;
				$this->method = 'admin_index';
				$controllerClass->templatePath = 'main/admin/admin_index.tpl';
				$this->controller = 'admin_main';
			}else{
				require(LOCALPATH.LOCALSLASH.'controllers'.LOCALSLASH.'main.controller.php');
				$controllerClass = new mainController;
				$this->method = 'index';
				$controllerClass->templatePath = 'main/index.tpl';
				$this->controller = 'main';
			}
			
			$controllerClass->isPostBack =  $isPostBack;
			
			call_user_func_array(array($controllerClass, $this->method), array($parameters));
		}
		
		
		if($controllerClass->no_smarty === 1){
			$GLOBALS['db']->Close();
			unset($GLOBALS['db']); 
			die;
		}
		
		//IF AJAX DISPLAY AND DIE
		if ($controllerClass->isAjax) {
			$controllerClass->display($controllerClass->templatePath);
			$GLOBALS['db']->Close();
			unset($GLOBALS['db']); 
			die;
		}
		
		return $controllerClass;
		
		
	}
	
	function Message( $text = '', $type = 0 ) { // 0 - success, 1 - error
		$message['Text'] = $_SESSION['Message'];
		$message['Type'] = $_SESSION['MessageType'];
		if ( $message['Text'] != "" ) {
			$_SESSION['Message'] = "";
			$_SESSION['MessageType'] = "";
			return $message;
		}
		else if ( $text != "" ) {
			$_SESSION['Message'] = $text;
			$_SESSION['MessageType'] = $type;
		}
	}
	
	function Redirect($to = '', $remember = false ) {
		if ( $remember ) {
			$_SESSION['redirect']['get'] = $_GET;
			$_SESSION['redirect']['post'] = $_POST;
		}
		else $_SESSION['redirect'] = null;
		
		if ( strlen($to) > 0)
			if ( headers_sent() ) print '<script>document.location="'.htmlspecialchars($to).'"'; else header("Location: " . $to);
		else
			header("Location: " . $_SERVER["REQUEST_URI"]);
		exit;
	}

	function mail($to = '', $subject = '', $message = '') {
        require_once(LOCALPATH.'/engine/lib/phpmailer.class.php');
		$charset = "UTF-8";
		$from_mail = 'support@'.SITENAME;
		$from_name = SITENAME;
		$mail = new PHPMailer();
		$mail->From     = $from_mail;
		$mail->Sender     = $from_mail;
		$mail->FromName = $from_name;
		$mail->AddAddress($to);
		$mail->ContentType = 'text/html';
		$mail->Body = $message;
		$mail->MsgHTML($message);
		$mail->CharSet = $charset;
		$mail->Subject = $subject;
		$mail->Send();
		$mail->ClearAddresses();
		$mail->ClearAttachments();  
     }

}
?>
