<?
class admin_usersController extends Controllers {
	var $models = array('users');
	
	function index() {
		
		if ( $this->isPostBack ) {
 			if ( isset($_POST['delete']) && count($_POST['ids']) > 0 ) {
 				foreach ($_POST['ids'] as $user_id) {
 					$photo_name = $this->users->getAvatar($user_id);
 					$this->delPhotoPhysicaly($photo_name);
 					$this->users->deleteUser($user_id);
 					Core::Message("Успешно изтрихте потребителите!");
 				}

 			}
		}
		
		$this->assign('users', $this->users->getUsers("pagingQuery", 20));
		$this->assign('pages', $this->users->pages);
		$this->assign('selected', 'index');
		
	}
	
	function edit($id) {
		if ( $this->isPostBack ) {
			if($_POST['username'] == '') $error = "Не сте въвели потребителско име.<br />";
			if($_POST['password'] != $_POST['password_2'] && $_POST['password'] != '') $error .= "Двете пароли не съвпадат.<br />";
			if($_POST['email'] == '') $error .= "Не сте въвели е-майл.<br />";
			if(!Controllers::is_email($_POST['email'])) $error .= "E-mail адресът е невалиден.<br />";
			if($this->users->username($_POST['username'], $id)) $error .= "Потребителското име e заето.<br />";
			if($this->users->email($_POST['email'], $id)) $error .= "Този емайл e зает.<br />";
			if($error == ''){
				if($_POST['password'] == ''){
					unset($_POST['password']); 
				}else{
					$_POST['password'] = md5($_POST['password']); 
				}
				
				if($_POST['del_photo'] == 1 && $_POST['photo_name'] != ""){
					$this->delPhotoPhysicaly($_POST['photo_name']);
					$_POST['avatar'] = "";
				}
				
				$this->users->edit($_POST, (int)$id);
				
				$this->tournaments->setStatusTournamentForUserId($id, 0);
				if(is_array($_POST['tournaments']) && count($_POST['tournaments']) > 0){
					foreach ($_POST['tournaments'] as $tourn_id){
						$this->tournaments->ForceInTournament($tourn_id, $id);
					}
				}
				
				Core::Message("Успешно редактирахте потребителя!", MSG_SUCCESS);
				
			}else{
				Core::Message($error, MSG_ERROR);
			}
		}
	 
		
		$this->assign('selected', 'edit');
		$data = $this->users->getUsersById((int)$id);
		$this->assign("i", $data); 
		$tournaments = $this->tournaments->getAllTournaments();
		if(is_array($tournaments) && count($tournaments) > 0){
			$ImMemeber = $this->tournaments->getMyTournaments(1, $id);
			if(is_array($ImMemeber) && count($ImMemeber) > 0){
				foreach ($ImMemeber as $v){
					$tournaments[$v['id']]['selected'] = 1;
				}
			}
		}
		
		$this->assign("tournaments", $tournaments);
		 
	}
	
	
	
}

?>