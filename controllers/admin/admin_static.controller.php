<?php 
	class admin_staticController extends Controllers{
		
		function index(){
			
			$this->assign("selected", "index");
			if($this->isPostBack){
				if(isset($_POST['delete']) && count($_POST['ids']) > 0){
					$delIds = implode(",", $_POST['ids']);
					if($delIds != ""){
						Controllers::deleteIds("static", "id", $delIds);
					}
				}
			}
			$this->assign("static_pages", $this->static->getAllStatic("pagingQuery", 20));
			$this->assign("pages", $this->static->pages);
		}
		
		
		function add(){
			$this->assign("selected", "add");
			if($this->isPostBack){
				if($_POST['title'] == "") $error = "Не сте въвели заглавие!<br />";
				if($_POST['content'] == "") $error .= "Не сте въвели съдържание!<br />";
					
				if($error == ""){
					$this->static->insertStatic($_POST);
					Core::Message("Успешно добавихте статична страница!");
					Core::Redirect("/admin/static");
				}else{
					Core::Message($error, MSG_ERROR);
				}
			}
			
			$this->force_template = 'admin_edit';
		}
		
		function edit($id = ""){
			$this->assign("selected", "edit");
			
			if($this->isPostBack){
				if($_POST['title'] == "") $error = "Не сте въвели заглавие!<br />";
				if($_POST['content'] == "") $error .= "Не сте въвели съдържание!<br />";
					
				if($error == ""){
					$this->static->editStatic($_POST, $id);
					Core::Message("Успешно редактирахте статична страница с ID ={$id}!");
					Core::Redirect("/admin/static");
				}else{
					Core::Message($error, MSG_ERROR);
				}
			}
			$static = $this->static->getStaticById($id);
			if($static['id'] > 0){
				$this->assign("i", $static);
			}else{
				Core::Message("Статична страница с ID = $id вече не съществува!", MSG_ERROR);
				Core::Redirect("/admin/static");
			}
		}
		
	}
?>