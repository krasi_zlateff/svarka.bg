<?php 
	class admin_tournamentsController extends Controllers{
		
		function index(){
			$this->assign("selected", "index");
			if($this->isPostBack){
				if(isset($_POST['delete']) && count($_POST['ids']) > 0){
					$delIds = implode(",", $_POST['ids']);
					if($delIds != ""){
						Controllers::deleteIds("tournaments", "id", $delIds);
						Controllers::deleteIds("tourn_members", "tourn_id", $delIds);
					}
				}
			}
			$this->assign("tournaments", $this->tournaments->getAllTournaments("pagingQuery", 20));
			$this->assign("pages", $this->tournaments->pages);
		}
		
		function add(){
			$this->assign("selected", "add_tournament");
			if($this->isPostBack){
				if($_POST['name'] == "") $error = "Не сте въвели име на турнир!<br />";
				if($_POST['start_date'] == "") $error .= "Не сте въвели начало на турнира!<br />";
				if($_POST['end_date'] == "") $error .= "Не сте въвели край на турнир!<br />";
				$startTime = 0;
				$endTime = 0;
				if(!$this->checkDate($_POST['start_date'], $startTime)) $error .= "Въведената начална дата не е коректна!";
				if(!$this->checkDate($_POST['end_date'], $endTime)) $error .= "Въведената крайна дата не е коректна!";
				if(!($startTime < $endTime)) $error .= "Крайното време трябва да е по-голямо от началното!";
				
				if($error == ""){
					$this->tournaments->insertTournament($_POST);
					Core::Message("Успешно добавихте турнир!");
					Core::Redirect("/admin/tournaments");
				}else{
					Core::Message($error, MSG_ERROR);
				}
			}
		}
		
		function edit($tourn_id = ""){
			$this->assign("selected", "edit_tournament");
			if($this->isPostBack){
				
				if($_POST['name'] == "") $error = "Не сте въвели име на турнир!<br />";
				if($_POST['start_date'] == "") $error .= "Не сте въвели начало на турнира!<br />";
				if($_POST['end_date'] == "") $error .= "Не сте въвели край на турнир!<br />";
				$startTime = 0;
				$endTime = 0;
				if(!$this->checkDate($_POST['start_date'], $startTime)) $error .= "Въведената начална дата не е коректна!";
				if(!$this->checkDate($_POST['end_date'], $endTime)) $error .= "Въведената крайна дата не е коректна!";
				if(!($startTime < $endTime)) $error .= "Крайното време трябва да е по-голямо от началното!";
				
				if($error == ""){
					$this->tournaments->editTournament($_POST, $tourn_id);
					Core::Message("Успешно редактирахте турнир!");
				}else{
					Core::Message($error, MSG_ERROR);
				}
			}
			
			$tournament = $this->tournaments->getTournamentById($tourn_id);
			if($tournament['id'] > 0){
				$this->assign("i", $tournament);
			}else{
				Core::Message("Турнир с ID = $tourn_id вече не съществува!", MSG_ERROR);
				Core::Redirect("/admin/tournaments");
			}
		}
		
		function approve(){
			$this->assign("selected", "approve");
			
			if($this->isPostBack){
				
				if(isset($_POST['approve']) && count($_POST['ids']) > 0){
					$ids = implode(",", $_POST['ids']);
					if($ids != "") $this->tournaments->setApprove($ids);
					Core::Message("Успешно одобрихте ID= $ids");
				}
				
				if(isset($_POST['delete']) && count($_POST['ids']) > 0){
					$delIds = implode(",", $_POST['ids']);
					if($delIds != ""){
						Controllers::deleteIds("tourn_members", "id", $delIds);
					}
				}
			}
			
			$this->assign("users", $this->users->getUsersForApproveTourn("pagingQuery", 20));
			$this->assign("pages", $this->users->pages);
		}
		
		function approve_user($tourn_member_id = 0){
			if((int)$tourn_member_id > 0){
				$this->tournaments->setApprove($tourn_member_id);
			}
			Core::Redirect("/admin/tournaments/approve");
		}
		
		function view_members($tourn_id = 0){
			$this->assign("selected", "view_members");
			if((int)$tourn_id > 0){
				
				if($this->isPostBack){
					if(isset($_POST['delete']) && count($_POST['ids']) > 0){
						$delIds = implode(",", $_POST['ids']);
						if($delIds != ""){
							//$this->tournaments->removeMembers($tourn_id, $delIds);
							Controllers::deleteIds("tourn_members", "id", $delIds);
						}
					}
				}
				
				$this->assign("users", $this->tournaments->getMemebersOfTournId($tourn_id, "pagingQuery", 20));
				$this->assign("pages", $this->tournaments->pages);
				$this->assign("tournament", $this->tournaments->getTournamentNameById($tourn_id));
			}else{
				Core::Redirect("/admin/tournaments");
			}
		}
		
	}
?>