<?php 
	class staticController extends Controllers{
		
		function index($id = 0){
			if((int)$id == 0) Core::Redirect(HOME_PAGE);
			$static = $this->static->getStaticById($id);
			if(!($static['id'] > 0)){
				Core::Message("Страницата вече не съществува или е преместена!", MSG_ERROR);
				Core::Redirect(HOME_PAGE);
			}
			$this->assign("static", $static);
		}
		
	}
?>