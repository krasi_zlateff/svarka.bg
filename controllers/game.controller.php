<?php
	class gameController extends Controllers{

		function index(){;}

		function play($id = 0)
		{
			$this->checkLoggedAndRedirect(true);

			$gameState = $this->pulling->checkGameState($id);

			if ($gameState['error_code'] == 0)
			{
				if ($gameState['host'] == 0)
				{
					if (!$gameState['invited'])
					{
						Core::Message('Тази маса е достъпна само с покана.', MSG_ERROR);
						Core::Redirect(GAMES_HOME_PAGE);
					}
					else
					{
						if (!$this->pulling->findFreePosition($id))
						{
							Core::Message('Масата няма свободни места.', MSG_ERROR);
							Core::Redirect(GAMES_HOME_PAGE);
						}
					}
				}
				else
					$this->assign('host', true);
			}
			else if ($gameState['error_code'] == 2)
			{
				Core::Message('Играта вече е започнала.', MSG_ERROR);
				Core::Redirect(GAMES_HOME_PAGE);
			}
			else
			{
				Core::Message('Масата не е отворена за игра.', MSG_ERROR);
				Core::Redirect(GAMES_HOME_PAGE);
			}

			$gameData = $this->pulling->getGameData($id);
			$roundAndStep = $this->pulling->getRoundAndStep($id);

			$user_info = $this->users->getFullUserById($_SESSION['UserID']);

			$this->assign('user', $user_info);
			$this->assign('gameID', $id);
			$this->assign('round', $roundAndStep['round']);
			$this->assign('gameData', $gameData);

			require('../root/cards.php');
			$this->assign('cardArr', json_encode($cards));
		}

		function idle()
		{
			echo json_encode($this->pulling->getPendingMessages());

			exit();
		}

		function fold()
		{
			$this->pulling->playerFold($_POST['game_id']);

			exit();
		}

		function game_messages()
		{
			$last = 0;

			if (isset($_POST['last']) && $_POST['last'] != null)
				$last = $_POST['last'];

			if (isset($_POST['lastSite']) && $_POST['lastSite'] != null)
				$lastSite = $_POST['lastSite'];

			echo json_encode($this->pulling->getAllMessages($_POST['game'], $last, $lastSite));

			exit();
		}

		function player_leave()
		{
			$leave = $this->pulling->playerLeave($_POST['game'], $_POST['player']);

			exit();
		}

		function send_game_invitation()
		{
			$this->pulling->sendGameInvitation($_SESSION['UserID'], $_POST['reciever'], $_POST['game_id'], $_POST['position']);

			exit();
		}

		function set_read()
		{
			$this->pulling->setRead($_POST['time']);

			exit();
		}

		function tables()
		{
			$games = $this->pulling->getGamesList();
			$this->assign('games', $games);
		}

		function begin_round()
		{
			require('../root/cards.php');
			$out = array();
			$out['dealt_cards'] = $this->pulling->dealCards($deck);
			$out['dealt_cards'] = $out['dealt_cards'][$_SESSION['UserID']];
			$out['cards'] = $cards;

			echo json_encode($out);

			exit();
		}

		function send_chat()
		{
			$this->pulling->chat($_POST['game'], $_POST['msg']);

			exit();
		}

		function play_dark()
		{
			$this->pulling->playOnTheDark($_POST['game']);

			exit();
		}

		function open_table()
		{
			$table = $this->pulling->openTable($_POST);

			echo $table;

			exit();
		}

		function game_enter_state()
		{
			$state = $this->pulling->getStateOnEnter($_POST['game']);
            
			echo json_encode($state);

			exit();
		}

		function check_timeout()
		{
			$timeout = $this->pulling->checkTimeout($_POST['game'], $_POST['player']);

			exit();
		}

		function start_game()
		{
			require('../root/cards.php');
			$tmp = $this->pulling->startGame($_POST['game'], $deck);

			echo json_encode($tmp);

			exit();
		}

		function bet()
		{
			$answer = false;
			if (isset($_POST['answer']))
				$answer = true;

			$tmp = $this->pulling->bet($_POST['game'], $_POST['bet'], $_POST['lastPlayer'], $_POST['lastBet'], $answer);

			echo json_encode($tmp);

			exit();
		}

		function get_game_navigation()
		{
			$this->isAjax = 1;
			$this->pulling->getGameCondition($_POST['game']);
		}

		function view_cards()
		{
			require('../root/cards.php');

			$pCards = $this->pulling->getPlayerCards($_POST['game'], $_SESSION['UserID']);
			$this->pulling->openCards($_POST['game'], $_SESSION['UserID']);
//			$this->pulling->setViewdCards($_SESSION['UserID'], $pCards, $_POST['game']);
			echo json_encode(array("cards"=>$pCards, "pile"=>$cards));

			exit();
		}

		function break_dark()
		{
			echo json_encode(array("bet"=>$this->pulling->getLastBet($_POST['game'])));

			exit();
		}

		function open_cards()
		{
			$this->pulling->playOnLight($_POST['game']);

			exit();
		}

		function enter()
		{
			$this->pulling->enterRound($_POST['game'], $_POST['val']);

			exit();
		}

		function get_opened_cards()
		{
			require('../root/cards.php');
//
//			$pCards = $this->pulling->getPlayerCards($_POST['game'], $_POST['player']);
//			$this->pulling->openCards($_POST['game'], $_SESSION['player']);
////			$this->pulling->setViewdCards($_SESSION['player'], $pCards, $_POST['game']);
//			echo json_encode(array("cards"=>$pCards, "pile"=>$cards));

			exit();
		}
	}

?>