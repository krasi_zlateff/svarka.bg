<?
class accountController extends Controllers {

	function edit()
	{
		$this->checkLoggedAndRedirect(true);

		if ( $this->isPostBack && isset($_POST['crop']))
		{
			$realPath = $_POST['path'];
			$name = $_POST['img_name'];
			$x = $_POST['x'];
			$y = $_POST['y'];
			$w = $_POST['w'];
			$h = $_POST['h'];
			$path = USER_IMG_PATH_PHYSICAL.LOCALSLASH.(int)($_SESSION['UserID']/1000).LOCALSLASH."av_".$name.'.jpg';

			$jpeg_quality = 90;

			$img_r = imagecreatefromjpeg($realPath);
			$dst_r = ImageCreateTrueColor( $w, $h );

			imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $w, $h, $w, $h);
			imagejpeg($dst_r, $path, $jpeg_quality);
		}

		if ( $this->isPostBack && isset($_POST['account']))
		{
			if($_POST['del_photo'] == 1){
				$this->delPhoto();
				$_POST['avatar'] = "";
			}
			if($_FILES['avatar']['tmp_name'] != "")
			{
				$name = "";
				if($this->writeAvatar($name)) $_POST['avatar'] = $name;
			}
			$this->users->edit($_POST, $_SESSION['UserID']);
			Core::Message("Успешно записахте промените", MSG_SUCCESS);
		}


		$user_info = $this->users->getFullUserById($_SESSION['UserID']);

		$this->assign('user', $user_info);



		$this->assign('allowed_year', date("Y"));
		$this->assign('cities', Controllers::getCities());
	}

	function lost_password()
	{
		if (isset($_POST['change_pass']))
		{
			$email = $_POST['email'];

			if ( !preg_match(VALID_EMAIL, $email))
			{
				$msg_error = 'Невалиден e-mail!<br />';
				Core::Message($msg_error, MSG_ERROR);
			}
			else
			{
				$this->users->sendNewPassword($email);
			}
		}
	}

	private function writeAvatar(&$name = "")
	{
		require_once(LOCALPATH.'/engine/lib/image.class.php');
		global $picSizeArr;
		$image = new Image();
		if($name == "") $name = md5(time().rand(0, 42342342));
		$directory = USER_IMG_PATH_PHYSICAL.LOCALSLASH.(int)($_SESSION['UserID']/1000);
		if(!is_dir($directory)) mkdir($directory);
		if(is_array($picSizeArr) && count($picSizeArr) > 0)
		{
			foreach ($picSizeArr as $prefix=>$whArr){
				$output = $directory.LOCALSLASH.$prefix.$name.'.jpg';
				if($this->testArray($whArr))
				{
	     			$return = $image->smart_resize_image($_FILES['avatar']['tmp_name'], $whArr['width'], $whArr['height'], $output, true);
				}else $return = false;
	     		if(!$return) @copy($_FILES['avatar']['tmp_name'], $output);
			}
		}
		$this->delPhoto();
     	return $return;
	}

	private function delPhoto()
	{
		global $picSizeArr;
		$photo_name = $this->users->getAvatar($_SESSION['UserID']);
		return $this->delPhotoPhysicaly($photo_name);
	}

	function password()
	{

		$this->checkLoggedAndRedirect(true);


		if ( isset($_POST['update_password']) )
		{

			if (!preg_match(VALID_PASSWORD, $_POST['password_current']))
			{
				$msg_error .= 'Въвели сте непозволени символи или прекалено дълга сегашна парола!<br />';
			}

			if (!preg_match(VALID_PASSWORD, $_POST['password_new']))
			{
				$msg_error .= 'Въвели сте непозволени символи или прекалено дълга нова парола!<br />';
			}

			if ( !$this->users->checkPassword ( $_POST['password_current'], $_SESSION['UserID']) )
			{
				$msg_error .= "Въвели сте грешна сегашна парола!<br />";
			}

			if ( trim($_POST['password_new']) == "" or trim($_POST['password_new1']) == "" )
			{
				$msg_error .= "Попълнете новата парола!<br />";
			}

			if ( $_POST['password_new'] != $_POST['password_new1'] )
			{
				$msg_error .= "Полетата за нова парола не съвпадат!<br />";
			}

			if($msg_error != ""){
				Core::Message($msg_error ,MSG_ERROR);
			}else {
				$this->users->setPassword($_POST['password_new'], $_SESSION['UserID']);
				Core::Message("Паролата е сменена успешно!");
				Core::Redirect(USER_HOME_PAGE);
			}
		}

	}
}

?>