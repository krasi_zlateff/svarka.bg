<?php 
class messagesController extends Controllers
{		
	function index()
	{
		
	}
	
	function inbox()
	{
		$this->checkLoggedAndRedirect(true);
		
		$msgs = $this->messages->getMessages(0);
		
		$this->assign('msgs', $msgs);
	}
	
	function outbox()
	{
		$this->checkLoggedAndRedirect(true);
		
		$msgs = $this->messages->getMessages(1);
		
		$this->assign('msgs', $msgs);
	}
	
	function send($msg_id = 0, $user_id = 0)
	{
		$this->checkLoggedAndRedirect(true);
		global $emotes;
		
		if ( $msg_id != 0 && $user_id != 0)
		{
			$msg = $this->messages->getReplyMessage($msg_id, $user_id);
			$this->assign('reply_msg', $msg);
			
			$user = $this->users->getUsersById($msg['sender_id']);
		}
		else
			$user = $this->users->getUsersById($user_id);
		
		if (!isset($user[0]))
		{
			Core::Message('Потребителят, на който искате да изпратите съобщение не съществува.', MSG_ERROR);
			Core::Redirect(INBOX_HOME_PAGE);
		}
		else
			$this->assign('user', $user);		
		
		if ($this->isPostBack)
		{
			$this->messages->sendMessage($_POST);
			
			Core::Message('Съобщението е изпратено успешно.', MSG_SUCCESS);
			Core::Redirect(INBOX_HOME_PAGE);			
		}
		
		$this->assign('emotes', $emotes);
	}
	
}
?>