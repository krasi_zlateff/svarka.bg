<?php
class ajaxController extends Controllers
{
	public $isAjax = 1;
	public $force_template = 0;

	function index()
	{
		exit();
	}
	
	function read_msg()
	{
		$msg = $this->messages->read($_POST['id']);
		
		echo json_encode($msg);
		
		exit();
	}
	
	function add_friend()
	{
		$this->users->addFriend($_POST['id']);
		
		exit();
	}
	
	function del_friend()
	{
		$this->users->delFriend($_POST['id']);
		
		exit();
	}
	
	function tournaments_member($act = '')
	{
		if ($act == 'join')
		{
			$this->tournaments->join($_POST['id']);
		}
		else if ( $act == 'part')
		{
			$this->tournaments->part($_POST['id']);
		}
		
		exit();
	}
	
	function search_users_invite()
	{
		if ($_POST['fr_only'] == 1)
		{
			$result = $this->users->searchFriends($_POST['search']);
		}
		else
		{
			$result = $this->users->searchUsers($_POST['search']);
		}
		
		echo json_encode($result);
		
		exit();
	}
}
?>