<?php
	class usersController extends Controllers{

		function index($id = 0)
		{
//			var_dump($this->users->isFriend($id));
			$this->assign('is_friend', $this->users->isFriend($id));
			$this->assign('user_data', $this->users->getFullUserById($id));
		}

		function all()
		{
			$this->assign('users', $this->users->getUsers());
		}

		function friends()
		{
			$this->checkLoggedAndRedirect(true);

			$this->assign('friends', $this->users->getFriends());
		}

		function confirm($id)
		{
			$this->checkLoggedAndRedirect(true);

			$this->users->confirmFriendRequest($id);

			Core::Message('Поканата за приятелство беше одобрена.', MSG_SUCCESS);
			Core::Redirect(INBOX_HOME_PAGE);
		}

		function register(){
			require('../root/captcha/php-captcha.inc.php');
			$this->checkLoggedAndRedirect(false);

			if ( $this->isPostBack ) {

				if (!preg_match(VALID_PASSWORD, $_POST['password'])) {
					$msg_error .= 'Въвели сте непозволени символи или прекалено дълга парола!<br />';
				}

				if ( ( $_POST['password'] != $_POST['password1'] )) {
					$msg_error .= 'Двете пароли не съвпадат!<br />';
				}

				if ( !preg_match(VALID_USERNAME, $_POST['username'])) {
					$msg_error .= 'Невалидно потребителско име!<br />';
				}

				if ( !preg_match(VALID_EMAIL, $_POST['email'])) {
					$msg_error .= 'Невалиден e-mail!<br />';
				}

				if ( ($this->users->username($_POST['username']))<>0 ){
					$msg_error .= 'Потребителското име е заето!<br />';
				}

				if ( ($this->users->email($_POST['email']))<>0 ){
					$msg_error .= 'Този е-майл вече е зает!<br />';
				}

				if (!PhpCaptcha::Validate($_POST['code'])){
					$msg_error .= 'Кода от картинката не съвпада!<br />';
				}

				if($msg_error != ""){
					Core::Message($msg_error, MSG_ERROR);
				}else {

					$user_id = $this->users->add_user($_POST);

					$udata = $this->users->getUserInfoSimple($user_id);


					if (  $udata['id'] > 0 ) {

							$mail = "
							Благодарим ви, че се регистрирахте в ".SITENAME."!
			                <br /><br />
						     Потребителското ви име е: ".$udata['username']."<br />
						     Паролата ви е: ".$_POST['password']."
							<br /><br />
							"
							/*.SITENAME." ви дава възможност да:
							<br /><br />
							* Играете сварка онлайн!<br />
							* и много други неща.
							"*/
							;
							Core::mail($udata['email'], SITENAME.' регистрация', $mail);

							$this->setLogedSession($udata);

							$message['content'] = 'Успешна регистрация, заповядайте в '.SITENAME.'. Можете да се възползвате от пълните услуги на сайта!';
							Core::Message($message['content'], MSG_SUCCESS);
							Core::Redirect(USER_HOME_PAGE);
					}else {
							$message['content'] = 'Възникна проблем по време на регистрация. Моля, опитайте по-късно.';
							Core::Message($message['content'], MSG_ERROR);
					}

				}
			}
		}

		function isalreadyin(){
			switch ($_POST['field']) {
				case 'username':
				if($this->users->username($_POST['username'])) print "Това потребителско име е заето!";
				break;
				case 'email':
				if($this->users->email($_POST['email'])) print "Този имейл е зает!";
				break;
			}
		}

		function login(){

			$this->checkLoggedAndRedirect(false);

			if ( $this->isPostBack ) {

				 $login = $this->users->login($_POST);

				if ( !((int)$login['id'] > 0) ) {
					Core::Message("Грешен потребител или парола", MSG_ERROR);
					($_SERVER['HTTP_REFERER'] != "") ? Core::Redirect($_SERVER['HTTP_REFERER']) : Core::Redirect(LOGIN_PAGE);
				}

				$this->setLogedSession($login);

				$this->users->setOnline($login['id']);

				if($_POST['save_me']){
					$md5 = md5(microtime());
					$this->users->SetSaveMe($md5);
			 		setcookie("user_md5", $md5, time()+31104000, '/');
				}

				if ( $login['usertype'] == ADMIN ) {
					$_SESSION['admin'] = 1;
					$redirect_to = "/admin";
				}
				else{

					$redirect_to = (isset($_GET['return_to'])) ? $_GET['return_to'] : USER_HOME_PAGE;
				}

				Core::Redirect($redirect_to);

			}

		}

		function logout() {

			$this->users->setLogout($_SESSION['UserID']);
			session_destroy();
			session_start();
			Core::Message("Успешно излязохте от системата! Заповядайте отново!", MSG_SUCCESS);
			Core::Redirect(HOME_PAGE);

		}

		function lost_password () {

			$this->checkLoggedAndRedirect(false);
			require('../root/captcha/php-captcha.inc.php');

			if ($this->isPostBack)
			{
				if (!PhpCaptcha::Validate($_POST['code'])){
					$msg_error = 'Кода от картинката не съвпада!<br />';
				}

				if (!($this->users->checkMail(mysql_escape_string($_POST['mail'])) > 0))
				{
					$msg_error .= 'Грешен E-mail!';
				}

				if($msg_error != ""){
					Core::Message($msg_error, MSG_ERROR);
				}else{
					$code = md5(time());
					$message = 'Здравейте, поискали сте смяна на паролата ви в '.SITENAME.', това е линка чрез който ще получите новата си парола<br /> <a href="'.WEBPATH.'users/change_pass/'.$code.'">'.WEBPATH.'users/change_pass/'.$code.'</a>';
					Core::mail($_POST['mail'], 'Смяна на парола в '.SITENAME, $message);
					$this->users->setLostPassCode(mysql_escape_string($_POST['mail']), $code);
					$_SESSION['set_lost_pass'] = true;
					Core::Redirect('/users/sent');
				}
			}
		}

		function sent(){
			if(!isset($_SESSION['set_lost_pass'])) Core::Redirect('/');

			unset($_SESSION['set_lost_pass']);
		}

		function change_pass ($code = ""){
			$this->checkLoggedAndRedirect(false);

			$check = $this->users->chekLostPasscode($code);

			if ((int)$check > 0) {
				$newpass = strtoupper(substr(md5(time()), 0, 6));
				$newfordb = md5($newpass);
				$this->users->setNewPass($newfordb, $check);
				$mail = $this->users->getEmailById($check);
				Core::mail($mail, 'Нова парола за '.SITENAME, 'Вашата нова парола е <strong>'.$newpass.'</strong>');
				$this->assign('OK', true);
			}
			else $this->assign('OK', false);
		}
	}

?>