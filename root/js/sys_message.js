function add_dim_for_msg(container){
	$(document).ready(function(){
	
		if(typeof container == "undefined") container = "popup_container";
		if(!($("#overlay").length > 0)){
			//$("body").prepend("<div id=\"overlay\">&nbsp;</div>");
			$("#overlay").css("height", $(document).height()+55);
			$("#overlay").show();
		}else{
			$("#overlay").css("height", $(document).height()+55);
			$("#overlay").css("display", "block");
		}
		$(".msg_container").show();
		$("."+container).show();
		
	});
}

function remove_dim_for_msg(container){
	if(typeof container == "undefined") container = "popup_container";
	$("#overlay").hide();
	$(".msg_container").hide();
	$("."+container).hide();
}