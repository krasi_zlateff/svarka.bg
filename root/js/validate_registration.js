$(document).ready(function(){
	$("#registration").validate ({
	rules: {
		
			username: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			password: {
				required: true
			},
			password1: {
				required: true,
				equalTo: "#password"
			},
			code: {
				required: true
			}	
	},
	messages: {
		username: "Не сте въвели потребителско име!",
		email: "Не сте въвели имейл или е въведен некоректно!",
		password: "Не сте въвели парола!",
		password1: "Двете пароли не съвпадат!",
		code: "Не сте въвели код!"
	},
	errorClass : "errormsg"
  });	
	
});