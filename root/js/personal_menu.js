$(document).ready(function(){
	$("#settings").click(
			function(){
				$(".user_avatar").unbind();
				$(".toggle").hide();
				$(".settings_wrapper").fadeIn();
			}
	);
	
	$(".close_menu").click(function(){
		$(".settings_wrapper").fadeOut(400, function(){
			user_avatar();
			$(".toggle").show();
		});
	});
});