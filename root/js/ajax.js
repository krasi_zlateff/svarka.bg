function ValidateArray () {
	this.username = 0;
	this.email = 1;
}

 Val = new ValidateArray();
 var elementNum;
  
function Ajax(file, data, div, method)
{
     $.ajax({
        type: method,
        url: file,
        data: data,
        cache: false,
        success: function(html){
         
        	document.getElementById(div).innerHTML = html;
  
         valid = html;
        }
     });
}

function PrependAjax(file, div, jqueryFunc, data, method, no_loader, eval_html, loader)
{
	if(typeof method == "undefined") method = 'POST';
	if(typeof no_loader == "undefined") no_loader = false;
	if(typeof eval_html == "undefined") eval_html = false;
	if(typeof loader == "undefined") loader = 'ajax-loader.gif';
	
	uniqueId = generateUniqueID();
	
	if(!no_loader){
		var $image_loader = $('<div align="center" id="loader_'+uniqueId+'"><img src="/i/design/'+loader+'" alt="Моля изчакайте" title="зарежда се" /></div>');
		
		switch(jqueryFunc){
		 	case 'after' : $image_loader.insertAfter($("#"+div));
		 					break;
		 	case 'before' : $image_loader.insertBefore($("#"+div));
				break;
		 	case 'html' : $("#"+div).html($image_loader);
		 					break;
		 	case 'append' : $image_loader.appendTo($("#"+div));
		 					break;
		 	default : $("#"+div).prepend($image_loader);
		}
	}
	
	
	
     $.ajax({
        type: method,
        url: file,
        data: data,
        cache: false,
        success: function(html){
    
    	 if(typeof $image_loader != "undefined") $image_loader.remove();
    	 
    	 if(eval_html){
    		 eval(html);
    	 }else{
         	switch(jqueryFunc){
	         	case 'after' : $("#"+div).after(html);
	         					break;
	         	case 'before' : $("#"+div).before(html);
								break;
	         	case 'html' : $("#"+div).html(html);
	         					break;
	         	case 'append' : $("#"+div).append(html);
	         					break;
	         	default : $("#"+div).prepend(html);
         	}
    	 }
        }
     });
}

function generateUniqueID(){
	var now = new Date();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var seconds = now.getSeconds()
	return hours+""+minutes+""+seconds;
}
 
function touch_ajax(file)
{
     $.ajax({
        type: "GET",
        url: file,
        data: null,
        cache: false 
     });
}
 
function validate(field, content, div, success, error) {
	   $.ajax({
        type: "GET",
        url: '/validations/field/'+field+'/'+content+'/'+success+'/'+error,
        cache: false,
        success: function(html){
        string=html.split("[DELIMITER]");
        $("#"+div).html(string[0]);
   
        eval("Val."+field +"= string[1]");
   
        }
        
     });

}
 
function checkValidate( arr ) {
	retval = true;
	for (var i = 0;i<arr.length;i++) {

			 if ( eval("Val."+arr[i]) == 0 ) {
			 	alert('Fill all fields!');
			 	retval = false;
	
			 }
			 
	}
	return retval;
		
}

function AjaxMsgs(file, data, div) {
	$.ajax({
		type: "GET",
		url: file,
		data: data,
		cache: false,
		success: function(html) {
			var old_num = $("#count_messages").val();
			if (html != old_num ) {
				if ( html > 0 ) $("#"+div).addClass("green"); else  $("#"+div).removeClass("green");
				$("#"+div).html("("+html+")"); 
			}
			setTimeout("AjaxMsgs('"+file+"', '', '"+div+"');", 15000);
		}
	});
}


function AjaxGetMsg(file, data, div, method, msg_id, target_id , to_id)
{
	if(target_id == 0 || target_id == 1){
		$('#reply').hide();
	}else{
		$('#reply').show();
	}
     $.ajax({
        type: method,
        url: file,
        data: data,
        cache: false,
        success: function(html){
    	 
        	document.getElementById(div).innerHTML = html;
         	$('.row').removeClass('row_selected');
         	$('#row_'+msg_id).addClass('row_selected');
         	$('#row_'+msg_id).removeClass('unread');
			$('#reply').attr("onclick", "");
			$('#reply').unbind("click").click(function(){
				if(to_id == 0){
					alert("Не можете да отговорите на това съобщение!");
				}else{
					if(target_id == to_id){
						document.location = '/messages/send/'+target_id;
					}else{
						document.location = '/messages/send/'+target_id+'/'+msg_id;
					}
				}
				
			});
			
//			$(".chk_selector").each(function(){
//				$(this).attr("checked", "");
//			});
			
			$("#chk_"+msg_id).attr("checked", "true");
			$("#"+div).fadeIn("slow");
        }
     });
}

function checkMsg() {
	AjaxMsgs("/main/countMessages", "", "count_messages");
}

function validateUser(field, content, div) {
	   $.ajax({
        type: "POST",
        url: '/validations/validUser/'+content,
        cache: false,
        success: function(html){
        string=html.split("[DELIMITER]");
        $("#"+div).html(string[0]);
   		if(string[0] != "") $("#"+div).show();
        eval("Val."+field +"= string[1]");
   
        }
        
     });

}

function validateEmail(field, content, div) {
     
     $.ajax({
        type: "POST",
        url: '/validations/validEmail/'+content,
        cache: false,
        success: function(html){
        string=html.split("[DELIMITER]");
        $("#"+div).html(string[0]);
   		if(string[0] != "") $("#"+div).show();
        eval("Val."+field +"= string[1]");
   
        }
        
     });
}

function execute_js(file, data, onsuccess)
{    
	if(typeof data == "undefined" || data == "") data = {isAjax : 1, no_smarty: 1};
	
     $.ajax({
        type: 'post',
        url: file,
        data: data,
        cache: false,
        success: function(html){
        	eval(html);
        	eval(onsuccess);
        }
     });
}

function post_ajax(file, data, selector)
{
     $.ajax({
        type: "POST",
        url: file,
        data: data,
        cache: false,
          success: function(html){
	    	 if(html != ""){
	    	 	$(selector).html(html).show();
	    	 }else{
	    		 $(selector).html('').hide();
	    	 }
        }
     });
}

