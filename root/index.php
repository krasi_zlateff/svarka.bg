<?php
header('Content-type: text/html; charset=utf-8');
@session_start();

require('../setup/configure.inc.php');
 
$core = new Core();
if($_POST['isAjax'] != 1){
	$controller_obj = new Controllers();
	
	$controller_obj->tryLogin();
	
	//update last activity
	if($controller_obj->isLogged()) $controller_obj->updateLastActivity();
}	
$body = $core->ParseURL();

if(is_object($body)) $body->assignVariables($core);

//if ($core->controller == 'game')
//{
//	require('cards.php');
//	
//	$controller_obj->assign('cards', $cards);
//	$controller_obj->assign('deck', $deck);
//}
 
//DISPLAY LAYOUT SETTINGS
if (!isset($body->layout)){
	($core->admin && isset($core->admin)) ? $body->layout = 'admin/admin_index': $body->layout = 'index';
}
$body->display('layout/'.$body->layout.'.tpl');

//Close opened mysql connection
$GLOBALS['db']->Close();
unset($GLOBALS['db']);
?>
