		{literal}
			<script type="text/html" id="chat-msg">
				<div class="chat_text_line">
					<span class="chat_writer">${date}</span> ${msg}
				</div>
			</script>
			<script type="text/javascript" src="/js/jquery-ui-1.8.8.custom.min.js"></script>
			<script type="text/javascript">
				$(function(){

					/*
					* !!!!!!!!
					* TODO: kato se vzimat message-ite da se dobavi edno property sydyrjasho
					* vsichki environment promenlivi za syotvetnata igra i rund za refresh na js promenlivite
					* i da ne se izostava ot aktualnite danni ot bazata i tn
					* moje da se dobavqt v kraq na vseki rund po oshte edna zaqvka za oshte edin refresh
					* i aktualizaciq na systoqniqta za da se podsiguri che vseki rund pochva na chisto
					* i ne se natrupvat greshki
					* !!!!!!!!
					*/

					// stuff
						$(".alpha07").mouseenter(function(){
							$(this).animate({
								opacity: 1
							});
						});

						$(".alpha07").mouseleave(function(){
							$(this).animate({
								opacity: 0.7
							});
						});
					// end

					// FALLBACKS

					var visiblePlayers = [];

					// END FALLBACKS

					var globalTimer = null;
					var gotInitialState = false;
					var currentInvitation = 0;
					var playerCount = 1;
					var lastGameMessage = null; // last message in state machine queue
					var quickMessage = null;
					var currentPlayer = null; // current player ID
					var currentPosition = null; // current player position
					var lastPlayer = false;
					var lastBet = 0;
					var slStart = 0;
					var fold = false;
					var money = 0;
					var playerButtons;
					var deck;
					var round;
					var tempInterval;
					var viewedEnd = true;
					var viewed = true;
					var secTimer;
					var tempSwitches;
					var semCardsShowed;
					var myPosition;
					var myID;
					var host;
					var remainingSeconds = 0;
					var remainingGlobalSeconds = 0;
					var remainingEndRoundSeconds = 0;
					var roundEndTimer = 6;
                    var lastGetGame = '';
                        
					// BEGIN Turn Counters

					var globalTurnCounter = null;
					var visualTurnCounter = null;
					var visualTicks = null;
					var roundEndInfoTimer = null;

					var fallbackTimer = null;

					// END Turn Counters

					$("body").click(function(){
						remainingGlobalSeconds = 0;
					});

					globalTimer = setInterval(tickGlobalTimer, 3000);

					// FALLBACK FOR THE ISSUE WITH THE BROWSER HIDING THE PLAYER ELEMENTS

					fallbackTimer = setInterval(function(){
						for (var fb in visiblePlayers)
						{
							$("#player" + visiblePlayers[fb]).css("visibility", "visible");
						}
					}, 2000);

					// END

					// FALLBACK FOR THE ISSUE WITH THE END OF ROUND POP-UP NOT HIDING AFTER ITS TIMEOUT
					// TODO: FIX THIS!!!
//					fallbackEndOfRoundTimer = setInterval(function(){
//						if (((remainingEndRoundSeconds >= roundEndTimer) || ($("#end-round-timer").html() == '') || ($("#end-round-timer").html() == 0)) && ($("#end-round-lightbox").css("display") == "block"))
//						{
//							console.log("fallback round pop-up");
//							remainingEndRoundSeconds = 0;
//							clearInterval(roundEndInfoTimer);
//							$("#viewed-end").click();
//							roundEndInfoTimer = null;
//						}
//					}, 3000);

					// END

					function tickEndRoundTimer()
					{
						++remainingEndRoundSeconds;
						$("#end-round-timer").html(roundEndTimer - remainingEndRoundSeconds);

						if (remainingEndRoundSeconds >= roundEndTimer)
						{
							remainingEndRoundSeconds = 0;
							clearInterval(roundEndInfoTimer);
							$("#viewed-end").click();
							roundEndInfoTimer = null;
						}
					}

					function tickGlobalTimer()
					{
						remainingGlobalSeconds += 3;
					}

					if ( jQuery.browser.msie && jQuery.browser.version >= 9 )
			        {
			            jQuery.support.noCloneEvent = true;
			        }

					round = $("#round").val();
					myPosition = $("#myPosition").val();
					deck = $.parseJSON($("#deck").val());

					// OPERA RELATIVE POSITION ISSUE FIX
					if( $.browser.opera )
					{
						$("#player2 .avatar").css({"top": "15px" });
						$("#player3 .avatar").css({"top": "-16px"});
						$("#player4 .avatar").css({"top": "-16px"});
						$("#player5 .avatar").css({"top": "14px" });
						$("#player9 .avatar").css({"top": "41px" });
						$("#player8 .avatar").css({"top": "76px" });
						$("#player7 .avatar").css({"top": "41px" });
					}

					function addTopMessagesCont()
					{
						div = "<div id='top-messages-cont'></div>";
						$("body").append(div);
					}

					$(".close-msg").live("click", function(){
						$(this).parent().fadeOut(2000, function(){$(this).remove();})
					});

					function getMyPosition(msgs, id)
					{
						for (var t in msgs)
						{
							if (msgs[t].player == id)
								return msgs[t].data.pos;
						}
					}
function dump(arr,level) {
    var dumped_text = "";
    if(!level) level = 0;
    
    //The padding given at the beginning of the line.
    var level_padding = "";
    for(var j=0;j<level+1;j++) level_padding += "    ";
    
    if(typeof(arr) == 'object') { //Array/Hashes/Objects 
        for(var item in arr) {
            var value = arr[item];
            
            if(typeof(value) == 'object') { //If it is an array,
                dumped_text += level_padding + "'" + item + "' ...\n";
                dumped_text += dump(value,level+1);
            } else {
                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
            }
        }
    } else { //Stings/Chars/Numbers etc.
        dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
    }
    return dumped_text;
}
					function getGameMessages()
					{
                        turnCards();
						if (lastGameMessage == null)
							lastGameMessage = 0;

						if (quickMessage == null)
							quickMessage = 0;

						$.post("/game/game_messages",{"game": $("#gameID").val(), "last": lastGameMessage, "lastSite": quickMessage}, function(data){
                            if(data != lastGetGame) {
                            lastGetGame = data;
                                
							if (data.site.count > 0)
							{
								if (!$("#top-messages-cont").is(":visible"))
									addTopMessagesCont();
								for(var i in data.site.messages)
								{
									if (data.site.messages[i].type == 1)
									{
										$("#top-messages-cont").prepend("<div>" + data.site.messages[i].data + "<div class='close-msg pointer'></div></div>");

										if ($("#top-messages-cont").children().length > 5)
										{
											$("#top-messages-cont").children().each(function(j){
												if (j >= 7)
													$(this).fadeOut(4000, function(){$(this).remove();});
											});
										}
									}
								}

								$.post('/game/set_read', {"time": data.site.last.time}, function(){;});
							}

							if (data.game.count > 0)
							{
								myID = $("#myID").val();
								host = $("#host").val();
								cap = $("#playersCap").val();
								viewEnd = false;

//								console.log(JSON.stringify(data));
								for (var j in data.game.messages)
								{
									try
									{
										msg = data.game.messages[j];

										if (msg.data.msg != '')
										{
											$("#chat_text").append($("#chat-msg").tmpl({"msg":msg.data.msg, "date":msg.time}));
										}
									}
									catch(e)
									{
										;
									}

									switch(msg.code)
									{
										case "0":
//											viewedEnd = false;
											break;
										case "1":
											++playerCount;
											p = msg.data.pos;

											if (msg.data.id == myID)
												myPosition = msg.data.pos;
											else
											{
												;
//												$("#player" + msg.data.pos + " .open-cards").fadeIn();
											}

											visiblePlayers[visiblePlayers.length] = p;

											$("#player" + p).css("visibility", "visible");
											$("#player" + p + " .avatar img").attr("src", msg.avatar);
											$("#player" + p).children(".player-name").children(".cont").html(msg.username + ' <span class="player-credits"></span>');
											$(".chat_users_list").append('<div class="chat_username chat-entry-' + msg.data.pos + '">' + msg.username + '</div><div class="chat_username_border">&nbsp;</div>');
											break;
										case "2":
											p = msg.data.pos;

											//$("#player" + p).css("visibility", "hidden");
											$(".chat-entry-" + msg.data.pos).next().remove();
											$(".chat-entry-" + msg.data.pos).remove();

											tmpArr = [];

											for (var fb in visiblePlayers)
											{
												if (visiblePlayers[fb] != p)
													tmpArr[tmpArr.length] = p;
											}

											visiblePlayers = tmpArr;
											break;
										case "6":
											$(".c1").css("background", "url(/i/cards/face-downs.png) no-repeat");
											$(".c2").css("background", "url(/i/cards/face-downs.png) no-repeat");
											$(".c3").css("background", "url(/i/cards/face-downs.png) no-repeat");
                                            
											beginRound(msg, myPosition);

											$(".player-cards").html("");

											$("#slider-text").html('');
											$("#start-game").fadeOut();
											break;
										case "8":
											$(".c1").css("visibility", "visible");
											$(".c2").css("visibility", "visible");
											$(".c3").css("visibility", "visible");

											$(".players").each(function(){
												if ($(this).attr("id").replace("player", "") != myPosition)
													$(this).children(".open-cards").fadeIn();
											});

											if (msg.round > 1)
											{
												viewedEnd = false;
											}
											break;
										case "13":
											// End of round
											handleEndOfRound(msg);
//											$("#table-lightbox").fadeOut();

											if (!viewedEnd)
											{
												for (var j in msg.cards)
												{
													if (msg.cards[j].pos == myPosition)
													{
														$("#player" + myPosition + " .open-cards").fadeIn();
														$("#c1b").fadeOut();
														$("#c2b").fadeOut();
														$("#c3b").fadeOut();
													}

													$("#open-p" + msg.cards[j].pos + " .c1").css("background", "url(/i/cards/" + msg.cards[j].c1.file + "s.png) no-repeat");
													$("#open-p" + msg.cards[j].pos + " .c2").css("background", "url(/i/cards/" + msg.cards[j].c2.file + "s.png) no-repeat");
													$("#open-p" + msg.cards[j].pos + " .c3").css("background", "url(/i/cards/" + msg.cards[j].c3.file + "s.png) no-repeat");
												}

												lastGameMessage = msg.id;
												clearTimeout(gameListener);
												data.game.messages = null;

												$("#end-round-points").html(msg.winner.sum);
												$("#end-round-winner").html(msg.winner.user);
												$("#end-round-lightbox").fadeIn();
//												$("#table-lightbox").fadeOut();

												if (remainingEndRoundSeconds <= 0 && roundEndInfoTimer == null)
												{
													roundEndInfoTimer = setInterval(tickEndRoundTimer, 1000);
												}
											}

											break;
										case "9":
										case "10":
//										case "11":
										case "12":
//										case "13":
//										case "14":
										case "15":
										case "16":
										case "17":
										case "18":
										case "19":
										case "20":
										case "21":
										case "22":
										case "23":
										case "24":
										case "25":
											secondarySwitch(msg);
											break;
										default:
											;
											break;
									}
                                    
								}
                                
								$("#chat_text").animate({ scrollTop: $("#chat_text").attr("scrollHeight") }, 1000);

								if (data.game.messages != null)
								{
									lastGameMessage = data.game.last.id;
//									console.log("last: " + lastGameMessage);
								}
							}
                            
                            } // end 
						}, "json");
					}
                    
					$("#viewed-end").click(function(){
						viewedEnd = true;
						$("#player" + $("#myPosition").val() + " .open-cards").fadeOut();
						$(".c1").css("background", "url(/i/cards/face-downs.png) no-repeat");
						$(".c2").css("background", "url(/i/cards/face-downs.png) no-repeat");
						$(".c3").css("background", "url(/i/cards/face-downs.png) no-repeat");
						$(this).parent().fadeOut();
						gameListener = setInterval(gameLoop, 4000);
//						viewedEnd = false;
					});

					function secondarySwitch(msg)
					{
						switch(msg.code)
						{
							case "9":
								// Player turn
//								console.log("9");
								procUserTurn(msg);
								break;
							case "10":
								handleBetting(msg);
								break;
							case "15":
								// player game type (dark/normal)
								break;
							case "16":
								// chat
								break;
							case "18":
								// player views his cards
								break;
							case "19":
								// player opened his cards
								if (msg.player != $("#myID").val())
								{
									$("#open-p" + msg.pos + " .c1").css("background-image", "url(/i/cards/" + msg.data.pile.c1.file + "s.png)");
									$("#open-p" + msg.pos + " .c2").css("background-image", "url(/i/cards/" + msg.data.pile.c2.file + "s.png)");
									$("#open-p" + msg.pos + " .c3").css("background-image", "url(/i/cards/" + msg.data.pile.c3.file + "s.png)");
								}
								break;
							case "20":
								// SVARKA
								break;
							case "24":
								// DEALER GIVING
								$("#player" + msg.data.position + " .player-name .player-credits").html(msg.data.credits);
								break;
							default:
								;
								break;
						}
					}

					function endFirstCounter()
					{
						if (remainingGlobalSeconds >= (60000*5))
						{	// leave after 5-6 minutes of nonactivity
							alert("Времето ви за отговор изтече");
							leave();
						}
					}

					function tickTurnCounter()
					{
						--remainingSeconds;
						var perc = parseInt(10 - (parseInt((remainingSeconds/visualTicks)*100)/10));
//						$("#timeout-counter").html("време: " + parseInt(((remainingSeconds/visualTicks)*100)/10));

						++perc;

						$("#player" + currentPosition + " .time-counter" + " .ct" + perc)
							.css("background", "url(/i/table/counters/c" + perc + "_1.png) no-repeat");

						if (remainingSeconds <= 0)
						{
							$("#player" + currentPosition + " .time-counter").fadeOut();
							$("#player" + currentPosition + " .time-counter .cs").each(function(i, el){
								$(this).css("background", "url(/i/table/counters/c" + parseInt(i + 1) + ".png) no-repeat");
							});

							clearInterval(visualTurnCounter);

							$.post("/game/check_timeout", {"game": $("#gameID").val(), "player": currentPlayer }, function(){
								clearInterval(visualTurnCounter);
//								clearInterval(globalTurnCounter);
							});
						}
					}

					function procUserTurn(msg)
					{
						currentPlayer = msg.player;
						currentPosition = msg.data.pos;

						clearInterval(visualTurnCounter);
//						clearInterval(globalTurnCounter);

						$(".time-counter").each(function(){
							if ($(this).css("display") == "block" && $(this).parent().attr("id").replace("player", "") != currentPosition)
								$(this).fadeOut();
						});

						$("#player" + msg.data.pos + " .time-counter").fadeIn();


						$(".players").each(function(p, pe){
							$("#" + $(this).attr("id") + " .time-counter .cs").each(function(i, el){
								$(this).css("background", "url(/i/table/counters/c" + parseInt(i + 1) + ".png) no-repeat");
							});
						});

						for (var p in msg.players)
						{
							$("#player" + msg.players[p].position + " .player-name .player-credits").html(msg.players[p].credits);
						}

						if (msg.timer && msg.sec > 0)
						{
							if (myID != msg.player)
								remainingSeconds = msg.sec + 10.0;
							else
								remainingSeconds = msg.sec;

							visualTicks = remainingSeconds;
							visualTurnCounter = setInterval(tickTurnCounter, 1000);
						}

						$(".bets").css("display", "none").attr('disabled', '');
						$(".glow").remove();

						if (myID == msg.player)
						{
							if (msg.sec > 0)
							{
								globalTurnCounter = setInterval(endFirstCounter, msg.sec * 1000);
							}
							else if (msg.timer)
							{
								// leave if time passed and refreshed

								leave();
							}

							$.post("/game/get_game_navigation", {"game": $("#gameID").val()}, function(btns){
//								$("#table-lightbox").fadeIn();
								playerButtons = btns;
								viewed = false;

								if (btns.viewed && btns.viewed.val == true)
								{
									viewed = true;
									for (var j in btns)
									{
										if (btns[j] != "btn_dark" && btns[j] != "btn_view")
										{
											if (btns[j].name)
											{
												if (btns[j].name == "rslider")
												{
													$("#slider").fadeIn();
													if (btns[j].start > 0)
														start_ = btns[j].start;
													else
														start_ = $("#minBet").val();

													slStart = start_;
													$("#slider-item").css("left", start_ + "px");
													$("#slider-text").html(start_);
													money = btns[j].end;
												}
												else if (btns[j].name == "enter")
												{
													$("#btn_enter").val("Влез с " + btns[j].val).attr("disabled", "").attr("bet", btns[j].val).fadeIn();
												}
											}
											else if(!btns[j].name)
											{
												$("#" + btns[j]).attr('disabled', '').fadeIn();

											}
										}
									}
								}
								else
								{
									for (var i in btns)
									{
										if ((btns[i] == "btn_dark" || btns[i] == "btn_view") && (!btns[i].name))
										{
											$("#" + btns[i]).attr('disabled', '').fadeIn();
										}
									}
								}
							}, "json");

//							if (isNaN(msg.data.lastBet))
							lastBet = parseInt(msg.data.lastBet);
//							alert(lastBet);

//							else
//								lastBet = msg.data.lastBet;

							credits = parseInt($.trim($("#player" + msg.data.pos + " .player-name .player-credits").html()));
						}
						else
						{
//							$("#table-lightbox").fadeOut();
						}

						lastPlayer = msg.data.last;

						$("#game-table .left .player-name").css("border", "2px solid transparent");

						$("<img class='glow' style=' position: absolute;' src='/i/design/glow.png' />").insertAfter("#player" + msg.data.pos + " .avatar");
					}

					function handleEndOfRound(msg)
					{
						$("#current-player-cards").children().remove();
					}

					function handleBetting(msg)
					{
						t = $("#player" + msg.data.pos + " .player-cards");
						ct = t.html();
						ct = $.trim(ct);
						if (ct == '' || ct == null)
							ct = 0;

						if (msg.data.bet == '' || msg.data.bet == null)
							msg.data.bet = 0;

						t.html(parseInt(msg.data.bet) + parseInt(ct));
					}

					function beginRound(msg, pos)
					{
						sum  = msg.data.cards.sum;

						var p = {};

						$("#btn_view").attr("disabled", "disabled");

//						if (pos == "8")
//						{
							p = {
								"c1": {"top": "125px", "left": "200px"},
								"c2": {"top": "125px", "left": "220px"},
								"c3": {"top": "125px", "left": "240px"}
							};
//						}
//						else if (pos == "1")
//						{
//							p = {
//								"c1": {"top": "125px", "left": "85px" },
//								"c2": {"top": "125px", "left": "105px"},
//								"c3": {"top": "125px", "left": "125px"}
//							};
//						}
//						else if (pos == "2")
//						{
//							p = {
//								"c1": {"top": "60px", "left": "145px"},
//								"c2": {"top": "60px", "left": "165px"},
//								"c3": {"top": "60px", "left": "185px"}
//							};
//						}
//						else if (pos == "3")
//						{
//							p = {
//								"c1": {"top": "45px", "left": "265px"},
//								"c2": {"top": "45px", "left": "285px"},
//								"c3": {"top": "45px", "left": "305px"}
//							};
//						}
//						else if (pos == "4")
//						{
//							p = {
//								"c1": {"top": "45px", "left": "460px"},
//								"c2": {"top": "45px", "left": "480px"},
//								"c3": {"top": "45px", "left": "500px"}
//							};
//						}
//						else if (pos == "5")
//						{
//							p = {
//								"c1": {"top": "60px", "left": "555px"},
//								"c2": {"top": "60px", "left": "575px"},
//								"c3": {"top": "60px", "left": "595px"}
//							};
//						}
//						else if (pos == "6")
//						{
//							p = {
//								"c1": {"top": "125px", "left": "615px"},
//								"c2": {"top": "125px", "left": "635px"},
//								"c3": {"top": "125px", "left": "655px"}
//							};
//						}
//						else if (pos == "7")
//						{
//							p = {
//								"c1": {"top": "200px", "left": "555px"},
//								"c2": {"top": "200px", "left": "575px"},
//								"c3": {"top": "200px", "left": "595px"}
//							};
//						}
//						else if (pos == "9")
//						{
//							p = {
//								"c1": {"top": "200px", "left": "145px"},
//								"c2": {"top": "200px", "left": "165px"},
//								"c3": {"top": "200px", "left": "185px"}
//							};
//						}

						if (pos == myPosition)
							size = {};
						else
							size = {};

						$("#current-player-cards").children().remove();
                        $("#c1b").remove();
                        $("#c2b").remove();
						$("#c3b").remove();
						$(".player-card-image").remove();

						if (msg.data.viewed)
						{
							c1 = msg.data.pile[msg.data.cards.c1].file;
							c2 = msg.data.pile[msg.data.cards.c2].file;
							c3 = msg.data.pile[msg.data.cards.c3].file;
							sum = msg.data.cards.sum;
						}
						else
						{
							c1 = 'face-down';
							c2 = 'face-down';
							c3 = 'face-down';
							sum = '';
						}

						try{
							$("#game-table").append("<img id='card1' src='/i/cards/" + c1 + ".png' style='position: absolute; top: 0px; left: 0px; width: 0px; height: 0px;' />");
							$("#card1").animate({"top": p.c1.top,"left": p.c1.left,"width": "66px","height": "99px"}, 500, function(){
								$("#card1").remove();
								$("#game-table").append("<div id='c1b' class='player-card-image' style='width: 66px; height: 98px; background: url(/i/cards/" + c1 + ".png) no-repeat;position: absolute; top: " + p.c1.top + "; left: " + p.c1.left + ";' class='left'></div>");

								$("#game-table").append("<img id='card2' src='/i/cards/" + c2 + ".png' style='position: absolute; top: 0px; left: 0px; width: 0px; height: 0px; z-index:998' />");
								$("#card2").animate({"top": p.c2.top,"left": p.c2.left,"width": "66px","height": "99px"}, 500, function(){
									$("#card2").remove();
									$("#game-table").append("<div id='c2b' class='player-card-image' style='width: 66px; height: 98px; background: url(/i/cards/" + c2 + ".png) no-repeat;position: absolute; top: " + p.c2.top + "; left: " + p.c2.left + ";' class='left'></div>");

									$("#game-table").append("<img id='card3' src='/i/cards/" + c3 + ".png' style='position: absolute; top: 0px; left: 0px; width: 0px; height: 0px; z-index:999' />");
									$("#card3").animate({"top": p.c2.top,"left": p.c3.left,"width": "66px","height": "99px"}, 500, function(){
										$("#card3").remove();
										$("#game-table").append("<div id='c3b' class='player-card-image' style='width: 66px; height: 98px; background: url(/i/cards/" + c3 + ".png) no-repeat;position: absolute; top: " + p.c3.top + "; left: " + p.c3.left + ";' class='left'></div>");
										$("#game-table").append("<div class='clear'></div>");
										$("#sum").remove();
										$("#game-table").append("<div id='sum'>" + sum + "</div>");
										$("#sum").corner("5px");
										$("#btn_view").attr("disabled", "");
									});
								});
							});

						}
						catch (e)
						{
							;
						}
					}

					function handleInitialState()
					{
						;
					}

					function getCurrentState()
					{
						id = $("#gameID").val();
						$.post("/game/game_enter_state", {"game": id}, function(data){
							initGame(data.players);
						}, "json");
					}


					function gameLoop()
					{
						if (!gotInitialState)
						{
							getCurrentState();
							gotInitialState = true;
							getGameMessages();
						}
						else
						{
							getGameMessages();
						}
					}

					try
					{
						clearTimeout(mainListener);
					}
					catch(e)
					{
						;
					}

					gameListener = setInterval(gameLoop, 2000);

					function bet(val, t)
					{
						b = $(".bets");
						b.fadeOut("fast");
						b.attr('disabled', 'disabled');
						game = $("#gameID").val();

						$.post("/game/bet", {"game": game, "bet": val, 'lastPlayer': lastPlayer, 'lastBet':lastBet}, function(data){
							if (data.error == "0")
							{
								lastPlayer = false;
							}
							else
								alert(data.msg);
						}, "json");
					}

					$("#btn_bet").click(function(){
						t = $(this);
						bt = $("#slider-text").html();

						bet(bt, t);

//						$("#table-lightbox").fadeOut();
					});

					$("#btn_enter").click(function(){
						t = $(this);
						val = t.attr("bet");
						game = $("#gameID").val();
						$.post("/game/enter", {"game": game, "val": val}, function(data){
//							$("#table-lightbox").fadeOut();
						}, "json");
					});

					$("#btn_answer").click(function(){
						t = $(this);

						if (lastBet == 0)
							alert("За да отговорите предишният играч трябва да е направил залог!");
						else
						{
//							$("#table-lightbox").fadeOut();
							bet($("#slider-text").html(), t);
						}
					});

					$("#btn_fold").click(function(){
						t = $(this);
						$.post("/game/fold", {"game_id": $("#gameID").val()}, function(){
							t.fadeOut();
//							$("#player" + myPosition).css("border-color", "red");
						});

//						$("#table-lightbox").fadeOut();
					});

					$("#btn_dark").live("click", function(){
						game = $('#gameID').val();
						t = $(this);
							t.fadeOut();
							$("#btn_view").fadeOut();
							$(".bets").attr("disabled", "disabled");
							for(var i in playerButtons)
							{
								if (playerButtons[i].name)
								{
									if (playerButtons[i].name == "rslider");
									{
										if (playerButtons[i].start > 0)
											start_ = playerButtons[i].start;
										else
											start_ = $("#minBet").val();

										slStart = start_;

										$("#slider-item").css("left", start_ + "px");
										$("#slider-text").html(start_);
										money = playerButtons[i].end;
										$("#slider").fadeIn();
									}
								}
								else if (playerButtons[i] != "btn_dark" && playerButtons[i] != "btn_view")
								{
									$("#" + playerButtons[i]).attr('disabled', '');
									$("#" + playerButtons[i]).fadeIn();
								}

							}
					});


					$("#chat-submit").click(function(){
						game = $('#gameID').val();
						msg = $("#chat-text").val();

						if (msg != '')
						{
							$.post("/game/send_chat", {"game": game, "msg": msg}, function(){
								$("#chat-text").val('');
							});
						}
					});

					$("#chat-text").keydown(function(event){
						if (event.keyCode == '13')
							$("#chat-submit").click();
					});

					{/literal}
						{if $host}
						{literal}
							$("#start-game").click(function(){
								t = $(this);
								if (playerCount > 1)
								{
									game = $("#gameID").val();
									$.post("/game/start_game", {"game": game}, function(data){
										if (data.stat == 1)
											t.fadeOut("fast");
										else
											alert(data.msg);
									}, "json");
								}
							});

							$(".add-player").live("click", function(){
								t = $(this);
								currentInvitation = t.parents(".left").first().attr("id").replace("player", "");
								$("#invite-player-cont").find(".cont").fadeIn("fast");

								$(".invite-section").click(function(){
									$(".invite-section").next().toggleClass("hidden");
								});

								$.post("/ajax/search_users_invite", {"fr_only": 0, "search": ''}, function(data){
									cont = '';
									for (var i in data)
									{
										cont += '<p class="pointer" id="user-' + data[i].id + '">' + data[i].username + '</p>\n';
									}

									$("#invite-fr-cont").find(".cont").html(cont).children(".pointer").bind("mouseover mouseout", function(){$(this).toggleClass("hover");}).click(function(){
										user = $(this).attr("id").replace("user-", "");
										game = $('#gameID').val();
										$.post("/game/send_game_invitation", {"reciever": user, "game_id": game, "position": currentInvitation}, function(data){
											$("#invite-player-cont").fadeOut("fast");
										});
									});
								}, "json");

								$("#invite-player-cont").fadeIn("fast").find("#search-btn").click(function(){
									if ($("#invite-fr-cont").hasClass('hidden'))
									{
										fr = 0;
										id = '#invite-all-cont';
									}
									else
									{
										fr = 1;
										id = '#invite-fr-cont';
									}

									$(id).find(".cont").fadeIn("fast").html('<img src="/i/design/invite-preloader.gif" alt="loading..." />');

									search = $("#invite-search").val();

									$.post("/ajax/search_users_invite", {"fr_only": fr, "search": search}, function(data){
										cont = '';
										for (var i in data)
										{
											cont += '<p class="pointer" id="user-' + data[i].id + '">' + data[i].username + '</p>\n';
										}

										$(id).find(".cont").html(cont).children(".pointer").bind("mouseover mouseout", function(){$(this).toggleClass("hover");}).click(function(){
											user = $(this).attr("id").replace("user-", "");
											game = $('#gameID').val();
											$.post("/game/send_game_invitation", {"reciever": user, "game_id": game, "position": currentInvitation}, function(data){
												$("#invite-player-cont").fadeOut("fast");
											});
										});
									}, "json");
								});
							});

							$("#close-invite").click(function(){
								$("#invite-player-cont").fadeOut("fast");
							});

							function initGame(pls)
							{
								//$("#game-table .left").children(".player-name").children(".cont").html('Отвори слот').addClass('open-slot');
								for (i=1; i<=pls.players_cap; ++i)
								{
									$("#player" + i).children(".player-name").children(".cont").html("Покани играч <img src='/i/design/add-small.png' alt='add' class='right' /><div class='clear'></div>").addClass("add-player").removeClass("open-slot");
								}

								for (var j in pls.players_list)
								{
									try
									{
										$("#player" + pls.players_list[j].position).children(".player-name").children(".cont").html(pls.players_list[j].name).removeClass("add-player");
									}
									catch(e)
									{
										;
									}
								}
							}
						{/literal}
						{else}
						{literal}
							function initGame(pls)
							{
								$("#game-table .left").css("visibility", "hidden");
								$("#game-table .left[id='table-middle']").css("visibility", "visible");
							}
						{/literal}
						{/if}
					{literal}

					$("#slider-box").click(function(e){
						t = $(this);

						clLeft = Math.round(e.pageX - t.offset().left);

						if ((Math.round((clLeft/100)*money)) < parseInt(slStart))
						{
							clLeft = slStart;
							$("#slider-text").html(slStart);
						}
						else if (clLeft >= 100)
						{
							clLeft = 100;
							$("#slider-text").html(Math.round((clLeft/100)*money));
						}
						else
						{
							$("#slider-text").html(Math.round((clLeft/100)*money));
						}

						$("#slider-item").css("left", clLeft + "px");
					});

					$("#slider-minus").mousedown(function(){
						clLeft = parseInt($("#slider-item").css("left").replace("px", ""));
						--clLeft;

						if ((Math.round((clLeft/100)*money)) < parseInt(slStart))
						{
							clLeft = slStart;
							$("#slider-text").html(slStart);
						}
						else
						{
							$("#slider-text").html(Math.round((clLeft/100)*money));
						}

						// TODO: CHECK IF IS LESS ACCORDING TO MONEY NOT PIXELS ..SAME FOR PLUS BUTTON

						$("#slider-item").css("left", clLeft + "px");
					});

					$("#slider-plus").mousedown(function(){
						clLeft = parseInt($("#slider-item").css("left").replace("px", ""));
						++clLeft;

						if (clLeft >= 100)
						{
							clLeft = 100;
							$("#slider-text").html(Math.round((clLeft/100)*money));
						}
						else if ((Math.round((clLeft/100)*money)) < parseInt(slStart))
						{
							alert(clLeft);
							clLeft = parseInt(slStart) + 1;
							$("#slider-text").html(clLeft);
						}
						else
						{
							$("#slider-text").html(Math.round((clLeft/100)*money));
						}

						$("#slider-item").css("left", clLeft + "px");

					});

					$("#slider-item").draggable({
						containment: 'parent',
						axis: 'x',
						drag: function(e,ui)
						{
							$("#slider-text").html(Math.round((ui.position.left/100)*money));
						},
						stop: function(e,ui)
						{
							$("#slider-text").html(Math.round((ui.position.left/100)*money));

							if (ui.position.left < slStart)
							{
								$("#slider-item").css("left", slStart + "px");
								$("#slider-text").html(slStart);
							}
						}
					});

					function turnCards()
					{
						$.post("/game/view_cards", {"game": $("#gameID").val()}, function(pCards){
							$("#c1b").css("background", "url(/i/cards/" + pCards.pile[pCards.cards.c1].file + ".png) no-repeat");
							$("#c2b").css("background", "url(/i/cards/" + pCards.pile[pCards.cards.c2].file + ".png) no-repeat");
							$("#c3b").css("background", "url(/i/cards/" + pCards.pile[pCards.cards.c3].file + ".png) no-repeat");
							$("#sum").remove();
							$("#game-table").append("<div id='sum'>" + pCards.cards.sum + "</div>");
							$("#btn_dark").fadeOut("fast").css("disabled", "disabled");
						}, "json");
					}

					$("#btn_view").click(function(){
						turnCards();

						game = $('#gameID').val();
						t = $(this);
							t.fadeOut();
							$("#btn_dark").fadeOut();
							$(".bets").attr("disabled", "disabled");
							for(var i in playerButtons)
							{
								if (playerButtons[i].name)
								{
									if (playerButtons[i].name === "rslider")
									{
										$.post("/game/break_dark", {"game": game}, function(data){
											if (data.bet > 0)
											{
												$("#slider-item").css("left", ((data.bet/money)*100) + "px");
												$("#slider-text").html(data.bet);
											}
										}, "json");

										if (playerButtons[i].start > 0)
											start_ = playerButtons[i].start;
										else
											start_ = $("#minBet").val();

										slStart = start_;

										$("#slider-item").css("left", start_ + "px");
										$("#slider-text").html(start_);
										money = playerButtons[i].end;
										$("#slider").fadeIn();
									}
									else if (playerButtons[i].name == "enter")
									{
										$("#btn_enter").val("Влез с " + playerButtons[i].val).attr("disabled", "").attr("bet", playerButtons[i].val).fadeIn();
									}

								}
								else if (playerButtons[i] != "btn_dark" && playerButtons[i] != "btn_view" && !playerButtons[i].name)
								{
									$("#" + playerButtons[i]).attr('disabled', '');
									$("#" + playerButtons[i]).fadeIn();
								}
							}
					});

					$("#btn_open").click(function(){
						$.post("/game/open_cards", {"game": $("#gameID").val()}, function(){
							;
						});
					});

					$("#leave-game").click(function(){
						if (confirm('Сигурни ли сте, че желаете да напуснете играта?'))
						{
							leave();
						}
					});

					function leave()
					{
						$.post("/game/player_leave", {"game": $("#gameID").val(), "player": $("#myID").val()}, function(data){
//							$("#player" + $("#myPosition").val()).css("visibility", "hidden");
//							window.location = '/game/tables';
							;
						}, "text");
					}

					// Visuals (some)

					$("#leave-game").mouseover(function(){
						$(this).css("background-color", "#DBD9DA");
					});

					$("#leave-game").mouseout(function(){
						$(this).css("background-color", "#cdcdcf");
					});
				});
			</script>
		{/literal}
<!--		<div id="table-lightbox">-->
<!---->
<!--		</div>-->

		<div id="end-round-lightbox">
			<div id="end-round-text"><span id="end-round-winner"></span> е победител в рунда с <span id="end-round-points"></span> точки</div>
			<p id="end-round-timer"></p>
			<input type="button" id="viewed-end" value="OK" />
		</div>

		<div id="invite-player-cont">
			<div class="top">
				<input type="text" id="invite-search" value="" />
				<input type="button" value="Търси" id="search-btn" />
				<span id="close-invite" class="pointer"></span>
				<div class="clear"><!--  --></div>
			</div>
			<div class="cont main-cont">
				<div class="invite-section folded friends-list" id="invite-all">&gt;&gt;Всички</div>
				<div id="invite-all-cont" class="hidden">
					<div class="cont"></div>
				</div>
				<div class="invite-section unfolded" id="invite-frs">&gt;&gt;Приятели</div>
				<div id="invite-fr-cont">
					<div class="cont"></div>
				</div>
			</div>
		</div>
		<div class="content" id="game-content">
			<div id="leave-game" class="pointer" >напусни играта</div>
<!--			<div id="timeout-counter" ></div>-->
			<input type="hidden" id="gameID" value="{$gameID}" />
			<input type="hidden" id="minBet" value="{if !isset($gameData.min_entry)}10{else}{$gameData.min_entry}{/if}" />
			<input type="hidden" id="host" value="{$host}" />
			<input type="hidden" id="myID" value="{$smarty.session.UserID}" />
			<input type="hidden" id="myPosition" value="{$gameData.position}" />
			<input type="hidden" id="playersCap" value="{$gameData.players_cap}" />
			<input type="hidden" id="deck" value='{$cardArr}' />
			<input type="hidden" id="round" value="{$round}" />
			<div id="game-table" class="left">
				<div class="row1 col1 left players" id="player2">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 2}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player2" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="clear"><!--  --></div>
					<div class="open-cards" id="open-p2">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>

				<div class="row1 col2 left players" id="player3">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 3}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player3" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p3">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>
				<div class="row1 col3 left players" id="player4">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 4}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player4" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p4">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>
				<div class="row1 col4 left players" id="player5">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 5}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player5" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p5">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>

				<div class="clear"><!--  --></div>

				<div class="row2 col1 left players" id="player1">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 1}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player1" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p1">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>
				<div class="row2 col2 left" id="table-middle">

				</div>
				<div class="row2 col4 left players" id="player6">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 6}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player6" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p6">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>
				<div class="clear"><!--  --></div>
				<div class="row3 col1 left players" id="player9">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 9}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player9" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p9">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>
				<div class="row3 col2 left players" id="player8">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 8}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player8" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>
					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p8">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>

				<div class="row3 col4 left players" id="player7">
					<div class="player-name alpha07 pointer">
						<div class="top"></div>
						<div class="cont">
							<img src="/i/design/name-preloader.gif" alt=".." />
							{foreach from=$gameData.players_list item=g}
								{if $g.position == 7}
									{$g.name}
								{/if}
							{/foreach}
						</div>
						<div class="bottom"></div>
					</div>

					<div class="avatar pointer">
						<img src="/i/design/no-avatar.png" alt="player7" />
						<div class="avatar-mask"></div>
					</div>
					<div class="time-counter">
						<div class="ct1 cs"></div>
						<div class="ct2 cs"></div>
						<div class="ct3 cs"></div>
						<div class="ct4 cs"></div>
						<div class="ct5 cs"></div>
						<div class="ct6 cs"></div>
						<div class="ct7 cs"></div>
						<div class="ct8 cs"></div>
						<div class="ct9 cs"></div>
						<div class="ct10 cs"></div>
					</div>

					<div class="player-cards">

					</div>
					<div class="open-cards" id="open-p7">
						<div class="c1" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c2" style="background: url(/i/cards/face-downs.png)"></div>
						<div class="c3" style="background: url(/i/cards/face-downs.png)"></div>
					</div>
				</div>

				<div class="clear"><!--  --></div>
			</div>
			<div id="chat-holder" class="content_chat_wrapper left">
				<div class="content_chat">
					<div id="table-controls">
						<div id="slider-text" class="left"></div>

						<div id="slider" class="bets left">
							<div id="slider-box" class="left pointer"><div id="slider-item" class="pointer"></div></div>
							<div class="clear"><!--  --></div>
						</div>

						{if $host}
							<input type="button" class="pointer left" id="start-game" value="Стартирай играта" />
						{/if}
						<input type="button" class="pointer bets left" id="btn_bet" value="Заложи" />
						<input type="button" class="pointer bets left" id="btn_answer" value="Отговори" />
						<input type="button" class="pointer bets left" id="btn_enter" value="" />
						<input type="button" class="pointer bets left" id="btn_fold" value="Бягай" />
						<input type="button" class="pointer bets left" id="btn_dark" value="На тъмно" />
						<input type="button" class="pointer bets left" id="btn_view" value="Виж картите" disabled="disabled" />
						<input type="button" class="pointer bets left" id="btn_open" value="Отворени карти" />
					</div>
					<div class="clear"><!--  --></div>
					<div class="chat_wrapper">
						<div class="chat">

							<div class="title">
								<h2>Чат</h2>
							</div>

							<div class="chat_text" id="chat_text">

							</div>

							<div class="chat_write_msg">
								<div class="write_field">
									<input type="text" name="chat_txt" class="chat_txt" id="chat-text" />
									<input type="button" class="input-submit chat-submit pointer" id="chat-submit" value="" />
								</div>
							</div>
						</div>
					</div>
					<div class="chat_users_list">
					</div>
					<div class="clear"><!--  --></div>
				</div>
			</div>
			<div class="clear"><!--  --></div>
		</div>