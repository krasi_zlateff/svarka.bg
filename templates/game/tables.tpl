{literal}
<script id="open-table-template" type="text/x-jquery-tmpl">
	<div id="open-table-form">
		<div class="left">
			Максимален брой играчи:
			<select id="players-cap">
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9" selected="selected">9</option>
			</select>
		</div>
		<div class="left">
			Вход:
			<input type="text" value="10" id="enter-value" />
		</div>
		<div class="left">
			Видима за всички:
			<input type="checkbox" value="1" checked="checked" id="visible-all" />
			<input type="button" id="open" value="Отвори" />
		</div>
		<div class="clear"><!-- --></div>
	</div>
</script>
<script type="text/javascript">
	$(function(){

		function isInt(x) {
			   var y=parseInt(x);
			   if (isNaN(y)) return false;
			   return x==y && x.toString()==y.toString();
			 }

		$("#open-table").toggle(function(){
			$(this).after($("#open-table-template").tmpl());
		}, function(){
			$(this).next().remove();
		});

		$("#open").live("click", function(){
			cap = $("#players-cap").val();
			priv = $("#visible-all").is(":checked")?0:1;
			enter = $("#enter-value").val();

			if (isNaN(enter) || !isInt(enter))
			{
				alert('Минималният вход трябва да е цяло число!');
			}
			else
			{
				$.post("/game/open_table", {"players_cap": cap, "private": priv, "val": enter}, function(data){
					window.location='/game/play/' + data;
				});
			}
		});

		$("#games-list .pointer").mouseover(function(){
			$(this).children('.item').css('background-color', '#F7FCF8');
		});

		$("#games-list .pointer").mouseout(function(){
			$(this).children('.item').css('background-color', '#E8E8E8');
		});

		$("#games-list .pointer").click(function(){
			window.location = '/game/play/' + $(this).children('.game-id').html();
		});
	});
</script>
{/literal}
<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">

					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Маси</h2>
							</div>
							<div class="real_content long_input">
								<span id="open-table" class="pointer">Отвори маса</span>
									<div class="game-item table-title">
										<div class="game-id left left-coll item">Маса</div>
										<div class="game-host left left-coll item">Хост</div>
										<div class="game-players left left-coll item">Играчи/Места</div>
										<div class="game-status left left-coll item">Статус</div>
										<div class="game-id left left-coll item">Вход</div>
										<div class="game-type left item">Публична</div>
										<div class="clear"><!--  --></div>
									</div>
								<div id="games-list">
									<div class="left" id="game-list-table-div">

											{foreach from=$games item=g key=gk}
												<div class="game-item pointer">
													<div class="game-id left left-coll item">{$g.id}</div>
													<div class="game-host left left-coll item">{$g.host}</div>
													<div class="game-players left left-coll item">{$g.pl_count}/{$g.players_cap}</div>
													<div class="game-status left left-coll item">{if $g.state == 1}отворена{elseif $g.state==2}започнала{else}завършила{/if}</div>
													<div class="game-id left left-coll item">{$g.min_entry}</div>
													<div class="game-type left item">{if $g.private == 0}публична{else}лична{/if}</div>
													<div class="clear"><!--  --></div>
												</div>
											{/foreach}
									</div>
									<div class="left" id="players-list">

									</div>
									<div class="clear"><!--  --></div>
								</div>
							</div>
						</div>
					</div>

					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>