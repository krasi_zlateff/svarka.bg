<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<li class="{if $selected EQ 'index'}ui-tabs-selected ui-state-active{/if} ui-state-default ui-corner-top"><a class="" href="/admin/static">Списък статични страници</a></li>
	<li class="{if $selected EQ 'add'}ui-tabs-selected ui-state-active{/if} ui-corner-top ui-state-default"><a href="/admin/static/add">Добавяне - статична страница</a></li>
	{if $selected EQ 'edit'}<li class="ui-tabs-selected ui-state-active ui-corner-top ui-state-default"><a href="#">Редактиране - статична страница</a></li>{/if}	
</ul>