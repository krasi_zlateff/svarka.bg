{include file='static/admin/submenu.tpl'}
<form method="post" action="">
	<table {if $static_pages|@is_array && $static_pages|@count GT 0}id="logtable"{/if} class="tablesorter"> 
	<thead> 
	<tr> 
	    <th><input type="checkbox"  safari=1 class="jquery-safari-checkbox" value="" onclick="checkUncheck('ids[]')" /></th> 
	    <th>id</th> 
	    <th>име</th>
	    <th>съдържание</th> 
	    <th>дата на създаване</th>
	    <th></th>
	</tr> 
	</thead> 
	<tbody> 
	{foreach from=$static_pages item=i key=k}
		<tr>
		<td><input type="checkbox" name="ids[]" safari=1 class="jquery-safari-checkbox" value="{$i.id}"/></td>
		<td>{$i.id}</td>
		<td><a href="/static/{$i.id}" target="_blank">{$i.title|escape}</a></td>
		<td>{$i.content|truncate:150}</td>
		<td>{$i.date_entered|date_format:'%d.%m.%Y %H:%M:%S'}</td>
		<td align="center"><a href="/admin/static/edit/{$i.id}" class="link">редактирай</a></td>
		</tr>
	{/foreach} 
	</tbody>
	</table>

	<div class="floatRight">с избраните: <input type="submit" name="delete" value="Изтрий" class="button" onclick="return confirm('Сигурни ли сте?')" /></div>
</form>
<div class="pages">{$pages}</div>