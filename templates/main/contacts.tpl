<script type="text/javascript" src="/js/jquery.validate.js"></script>

		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Контакти</h2>
							</div>
							<div class="real_content long_input">
								<form method="post" action="" id="account">
								{if !($smarty.session.UserID GT 0)}
									<div class="form_row"><label class="label" for="name">Вашето име <span>*</span></label><input class="input-text" id="name" value="{$smarty.post.name}" type="text" name="name"/></div>
									<div class="form_row"><label class="label" for="email">Email <span>*</span></label><input class="input-text" id="email" value="{$smarty.post.email}" type="text" name="email" /></div>
								{/if}
									<div class="form_row"><label class="label" for="phone">Телефон </label><input class="input-text" id="phone" type="text" value="{$smarty.post.phone}" name="phone" /></div>
									<div class="form_row"><label class="label" for="description">Съдържание <span>*</span></label><textarea class="textarea input-text" name="description" id="description">{$smarty.post.description}</textarea></div>
									<div class="form_row"><label class="label">&nbsp;</label><img src="/root/captcha/visual-captcha.php" alt="Visual CAPTCHA" width="200" height="60"></div>
									<div class="form_row"><label class="label" for="kod">Код <span>*</span></label><input class="input-text" id="kod" type="text" name="kod" /></div>
									<div class="form_row"><label class="label">&nbsp;</label><input class="input-submit input-submit-long" type="submit" name="send" value="Изпрати" /></div>
								</form>
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>
{if !($smarty.session.UserID GT 0)}
{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$("#account").validate({
			rules:{
				name:{
					required: true
				},
				email:{
					required: true,
					email: true
				},
				description:{
					required: true
				},
				kod:{
					required: true
				}
			},
			messages: {
				name: "Не сте въвели вашето име!",
				email: "Не сте въвели имейл!",
				description: "Не сте въвели съдържание!",
				kod: "Не сте въвели код!"
			},
			errorClass : "errormsg"
		});
	});
</script>
{/literal}
{else}
{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$("#account").validate({
			rules:{
				description:{
					required: true
				},
				kod:{
					required: true
				}
			},
			messages: {
				description: "Не сте въвели съдържание!",
				kod: "Не сте въвели код!"
			},
			errorClass : "errormsg"
		});
	});
</script>
{/literal}
{/if}