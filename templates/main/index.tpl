<script type="text/javascript" src="/js/main_index.js"></script>
		<div class="content">
			<div class="content_part_1">
				<h3>Играй Сварка с приятелите си</h3>
				<div class="text_part_1">
					Печелят само картите, които имат най-голям сбор в комбинациите.
				</div>
				<div class="register_btn">
					<input type="button" name="register" onclick="document.location='/users/register'" value="РЕГИСТРИРАЙ СЕ В ИГРАТА"/>
				</div>
			</div>
			<div class="content_part_2">
				<div class="arrow_links">
					<a href="/users/register" title="Регистрирай се!"><img src="/i/design/reg-arrow.png" alt="Регистрирай се!" /></a>
				</div>
				<div class="arrow_links">
					<a href="#" title="Играй сварка"><img src="/i/design/play-arrow.png" alt="Играй сварка" /></a>
				</div>
				<div class="arrow_links arrow_links_last">
					<a href="#" title="Печели награди"><img src="/i/design/win-arrow.png" alt="Печели награди" /></a>
				</div>
			</div>
		</div>

		<div class="content_part_3">
			<div class="box">
				<div class="box_inner">

					<div class="content_text">
						<div class="title">
							<h2>Играйте сварка с нас, забавлявайте се с приятели!</h2>
						</div>
						<div class="content_left">
							Сварката е игра на карти, която подхожда на темперамента на балканските народи. <br/>
							Изключително развлекателна и емоциална игра, <br/>
							в която целта е сборът от картите на един играч да е по-голям от този на другите играчи, за да спечели.<br/>
							Блъфовете са препоръчителни. Така се изразява чувството за хумор на играчите и емоцията е 100%.<br/>
							 Играе се сравнително лесно, което я прави и толкова популярна. <br/>
						</div>
					</div>

					<div class="content_right">
						<div class="svarka-1"><img src="/i/design/svarka-1.png" alt="" /></div>
						<div class="svarka-2"><img src="/i/design/svarka-2.png" alt="" /></div>
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</div>