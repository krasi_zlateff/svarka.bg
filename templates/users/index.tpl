		{literal}
		<script type="text/javascript">
		$(function(){
			$(".add-friend").click(function(){
				t = $(this);
				id = t.attr("id").replace("add-", "");

				$.post("/ajax/add_friend", {"id": id}, function(){
					t.parent().html('Поканата за приятелство е изпратена успешно.');					
				});

				return false;
			});
		});			
		</script>
		{/literal}
		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Профил на {$user_data.username}</h2>
							</div>
							<div class="real_content long_input">
								<div class="profile">
									{if isset($user_data.avatar)}
										<div class="img left"><img src="{$USER_IMG_PATH}{$user_data.id/1000|int}/l_{$user_data.avatar}.jpg" alt="{$user_data.username}" /></div>
									{/if}
									<div class="data left">
										<p><span>Име:</span> {$user_data.name}</p>
										<p><span>Град:</span> {$user_data.city_name}</p>
										<div class="link">											
											{if $user_data.id != $smarty.session.UserID}
												<p><a href="/messages/send/0/{$user_data.id}" title="Изпрати съобщение">Изпрати съобщение</a></p>
												<p>
													{if isset($smarty.session.UserID)}
														{if !$is_friend}
															<a href="#" id="add-{$user_data.id}" class="add-friend" title="Добави за приятел">Добави за приятел</a>
														{else}
															{$user_data.name} е ваш приятел.
														{/if}
													{/if}
												</p>
											{/if}
										</div>
									</div>
									<div class="clear"><!--  --></div>
								</div>
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>