{literal}
<script type="text/javascript">
	$(function(){
		$(".add-friend").click(function(){
			t = $(this);
			id = t.attr("id").replace("add-", "");

			$.post("/ajax/add_friend", {"id": id}, function(){
				t.parent().html('Поканата за приятелство е изпратена успешно.');
			});

			return false;
		});
	});
</script>
{/literal}
<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">

					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Потребители</h2>
							</div>
							<div class="real_content long_input">
							{foreach from=$users item=f}
								{if $f.username != 'admin'}
									<div class="profile">
										<a href="/users/profile/{$f.id}" title="{$f.username}">
											{if isset($f.avatar) && $f.avatar != ''}
												<div class="img left"><img src="{$USER_IMG_PATH}{$f.id/1000|int}/l_{$f.avatar}.jpg" alt="{$f.username}" /></div>
											{else}
												<div class="img left"><img src="/i/design/logo.png" alt="{$f.username}" /></div>
											{/if}
										</a>
										<div class="data left">
											<p><span>Потребител:</span> <a href="/users/profile/{$f.id}" title="{$f.username}">{$f.username}</a></p>
											<p><span>Име:</span> {$f.name}</p>
											<p><span>Град:</span> {$f.city_name}</p>
											<div class="link">
												{if $smarty.session.UserID && $smarty.session.UserID != $f.id}
<!--													<p><a href="/" class="send-game-invitation" id="invite-{$f.id}">test</a>-->
													<p><a href="/messages/send/0/{$f.id}" title="Изпрати съобщение">Изпрати съобщение</a></p>
													{if !isset($f.fr) || $f.fr != 1}<p><a href="/" id="add-{$f.id}" class="add-friend" title="Добави приятел">Добави приятел</a></p>{/if}
												{/if}
											</div>
										</div>
										<div class="clear"><!--  --></div>
									</div>
								{/if}
							{/foreach}
							</div>
						</div>
					</div>

					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>