{if $i.id NEQ ''}<tr><th>ID</th><td>{$i.id}</td></tr>{/if}
<tr><th>Потребителско име*</th><td><input type="text" class="input big_input" name="username" value="{$i.username|escape}" /></td></tr>
<tr><th>Е-mail*</th><td><input type="text" class="input big_input" name="email" value="{$i.email|escape}" onblur="checkEmail('mail')" id="mail" /></td></tr>
{if $selected EQ 'add'}<tr><th>Парола*</th><td><input type="password" class="input big_input" name="password" id="pass" onkeyup="$('#pass_2').show();"/></td></tr>{/if}
{if $selected EQ 'edit'}<tr><th>Нова парола</th><td><input type="password" class="input big_input" name="password" id="pass" onkeyup="$('#pass_2').show();" /></td></tr>{/if}
<tr id="pass_2"><th>Повтори парола*</th><td><input type="password" class="input big_input" name="password_2" onblur="if(document.getElementById('pass').value != this.value) alert('Двете пароли не съвпадат')" /></td></tr>
<tr><th>Администратор</th><td><select name="usertype">
								<option value="2" {if $i.usertype EQ '2'}selected{/if}>Да</option>
								<option value="1" {if $i.usertype EQ '1'}selected{/if}>Не</option>
							  </select>
							</td>
</tr>
{if $i.avatar NEQ ""}
<tr>
	<th>Снимка</th>
	<td><img src="{$USER_IMG_PATH}{$i.id/1000|int}/l_{$i.avatar}.jpg" alt="{$i.username}" /></td>
</tr>
<tr>
	<th>&nbsp;</th>
	<td>
		<input class="checkbox" type="hidden" value="{$i.avatar}" name="photo_name"/>
		<input class="checkbox" type="checkbox" value="1" name="del_photo" id="del_photo"/><label for="del_photo">Изтрий снимката</label>
	</td>
</tr>
				{/if}
{if $tournaments|@is_array && $tournaments|@count GT 0}
<tr>
	<th>Участва в турнири</th>
	<td>
		{foreach from=$tournaments item=i}
			<input {if $i.selected EQ 1}checked="checked"{/if} type="checkbox" name="tournaments[]" value="{$i.id}" id="tourn_{$i.id}"/><label for="tourn_{$i.id}">{$i.name}</label><br />
		{/foreach}
	</td>
</tr>
{/if}
{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$('#pass_2').hide();
	});
</script>
{/literal}