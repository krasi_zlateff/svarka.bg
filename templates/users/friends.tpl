		{literal}
		<script type="text/javascript">
		$(function(){
			$(".del-friend").click(function(){
				t = $(this);
				id = t.attr("id").replace("del-", "");

				if (confirm('Сигурни ли сте, че желаете да изтриете потребителя от списъка Ви с приятели?'))
				{
					$.post("/ajax/del_friend", {"id": id}, function(){
						t.parent().html('Потребителят е премахнат от списъка с приятелите Ви.').parents(".profile").fadeOut(2000);
					});
				}

				return false;
			});
		});
		</script>
		{/literal}
		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">

					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Приятели</h2>
							</div>
							<div class="real_content long_input">
							{foreach from=$friends item=f}
								<div class="profile">
									{if isset($f.avatar)}
										<div class="img left">
											{if isset($f.avatar) && $f.avatar != ''}
												<div class="img left"><img src="{$USER_IMG_PATH}{$f.id/1000|int}/l_{$f.avatar}.jpg" alt="{$f.username}" /></div>
											{else}
												<div class="img left"><img src="/i/design/logo.png" alt="{$f.username}" /></div>
											{/if}
										</div>
									{/if}
									<div class="data left">
										<p><span>Потребител:</span> <a href="/users/profile/{$f.id}" title="{$f.username}">{$f.username}</a></p>
										<p><span>Име:</span> {$f.name}</p>
										<p><span>Град:</span> {$f.city_name}</p>
										<div class="link">
											<p><a href="/messages/send/0/{$f.id}" title="Изпрати съобщение">Изпрати съобщение</a></p>
											{if $f.id != $smarty.session.UserID}
												<p>
													<a href="#" id="del-{$f.id}" class="del-friend" title="Изтрий приятел">Изтрий от приятелите</a>
												</p>
											{/if}
										</div>
									</div>
									<div class="clear"><!--  --></div>
								</div>
								{foreachelse}
									Нямате добавени приятели.
								{/foreach}
							</div>
						</div>
					</div>

					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>