	<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Забравена парола</h2>
							</div>
							<div class="real_content long_input">
								<p>
									<b>
									{if $OK EQ false}
										Грешен код за промяна на вашата парола!
									{else}
										Моля посетете отново вашия имейл адрес, на който изпратихме новата ви парола!
									{/if}
									</b>
								</p>
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>