<script type="text/javascript" src="/js/ajax.js"></script>
<script type="text/javascript" src="/js/jquery.validate.js"></script>
<script type="text/javascript" src="/js/validate_registration.js"></script>
		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Регистрация</h2>
							</div>
							<div class="real_content long_input">
								<form method="post" id="registration" action="">
								     <div class="form_row"><label class="label" for="username">Потребителско име: <span>*</span></label> <input class="input-text" id="username" maxlength="18" onblur="post_ajax('/users/isalreadyin', {literal}{{/literal}isAjax:1, no_smarty: 1, field : 'username', username : $(this).val(){literal}}{/literal}, '#usernamevalid');" name="username" size="35" value="{$smarty.post.username|escape}" type="text" />
								     	<div class ="errormsg" id="usernamevalid" style="display:none;">&nbsp;</div>
								     </div>
									 
								     <div class="form_row"><label class="label" for="email">Имейл: <span>*</span></label> <input class="input-text" onblur="post_ajax('/users/isalreadyin', {literal}{{/literal}isAjax:1, no_smarty: 1, field: 'email',  email : $(this).val(){literal}}{/literal}, '#emailvalid');" name="email" size="35" value="{$smarty.post.email|escape}" type="text" />
								     	<div class ="errormsg" id="emailvalid" style="display: none;"></div>
								     </div>
								     
								     <div class="form_row"><label class="label" for="password">Парола: <span>*</span></label> <input class="input-text" id="password" name="password" size="35" value="" type="password" /></div>
								     
									 <div class="form_row"><label class="label" for="password1">Повтори парола: <span>*</span></label> <input class="input-text" id="password1" name="password1" size="35" value="" type="password" /></div>
									 
									 <div class="form_row"><label class="label">Код: </label><img src="/captcha/visual-captcha.php" alt="Visual CAPTCHA" width="200" height="60" /></div>
								     <div class="form_row"><label class="label" for="code">Код от картинката: <span>*</span></label> <input class="input-text" type="text" name="code" id="code" value="" /></div>
								     
								      <div class="form_row"><label class="label">&nbsp;</label><input class="input-submit input-submit-long" type="submit" name="register" value="Регистрирай" /></div>
								</form>
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>