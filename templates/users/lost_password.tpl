<script type="text/javascript" src="/js/jquery.validate.js"></script>


		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Забравена парола</h2>
							</div>
							<div class="real_content long_input">
								<form method="post" action="" id="lost_password">
									<div class="form_row"><label class="label" for="mail">Вашият имейл <span>*</span></label><input type="text" class="input-text" name="mail" id="mail" /></div>
									<div class="form_row"><label class="label">Код </label><img src="/captcha/visual-captcha.php" alt="Visual CAPTCHA" width="200" height="60"></div>
								    <div class="form_row"><label class="label" for="code">Код от картинката <span>*</span></label> <input class="input-text" type="text" name="code" id="code" value="" /></div>
									<div class="form_row"><label class="label">&nbsp;</label><input class="input-submit input-submit-long" type="submit" value="изпрати" /></div>
									<input type="hidden" name="changePassword" value="1" />
								</form>
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>

{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$("#lost_password").validate({
			rules: {
				mail: {
					required: true,
					email: true
				},
				code: {
					required: true
				}
			},
			messages: {
				mail: "Не сте въвели емайл или е грешен!",
				code: "Не сте въвели код!"
			},
			errorClass : "errormsg"
		});
	});
</script>
{/literal}