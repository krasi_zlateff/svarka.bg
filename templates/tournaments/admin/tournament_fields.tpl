{if $i.id NEQ ''}<tr><th>ID</th><td>{$i.id}</td></tr>{/if}
<tr><th>Име*:</th><td><input type="text" class="input big_input" name="name" value="{$i.name|escape}" /></td></tr>
<tr><th>Начало*:</th><td><input type="text" class="date_time input big_input" name="start_date" value="{$i.start_date}" /></td></tr>
<tr><th>Край*:</th><td><input type="text" class="date_time input big_input" name="end_date" value="{$i.end_date}" /></td></tr>
<tr><th>Описание на турнира</th><td><textarea class="input big_input" name="description">{$i.description|escape}</textarea></td></tr>

{literal}
<script type="text/javascript">
<!--
$(document).ready(function(){
	$('.date_time').datetimepicker({dateFormat: 'yy-mm-dd', timeFormat : "hh:ii:00"});
});
//-->
</script>
{/literal}