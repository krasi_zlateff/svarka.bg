{include file='tournaments/admin/submenu.tpl'}
<form method="POST">
	<table {if $tournaments|@is_array && $tournaments|@count GT 0}id="logtable"{/if} class="tablesorter"> 
	<thead> 
	<tr> 
	    <th><input type="checkbox"  safari=1 class="jquery-safari-checkbox" value="" onclick="checkUncheck('ids[]')" /></th> 
	    <th>id</th> 
	    <th>име</th>
	    <th>описание</th> 
	    <th>дата на създаване</th>
	    <th>начална дата</th>
	    <th>крайна дата</th>
	    <th></th>
	</tr> 
	</thead> 
	<tbody> 
	{foreach from=$tournaments item=i key=k}
		<tr>
		<td><input type="checkbox" name="ids[]" safari=1 class="jquery-safari-checkbox" value="{$i.id}"/></td>
		<td>{$i.id}</td>
		<td><a href="/admin/tournaments/view_members/{$i.id}">{$i.name|escape}</a></td>
		<td>{$i.description|truncate:150}</td>
		<td>{$i.date_entered|date_format:'%d.%m.%Y %H:%M:%S'}</td>
		<td>{$i.start_date|date_format:'%d.%m.%Y %H:%M:%S'}</td>
		<td>{$i.end_date|date_format:'%d.%m.%Y %H:%M:%S'}</td>
		<td align="center"><a href="/admin/tournaments/edit/{$i.id}" class="link">редактирай</a></td>
		</tr>
	{/foreach} 
	</tbody>
	</table>

	<div class="floatRight">с избраните: <input type="submit" name="delete" value="Изтрий" class="button" onclick="return confirm('Сигурни ли сте?')" /></div>
</form>
<div class="pages">{$pages}</div>