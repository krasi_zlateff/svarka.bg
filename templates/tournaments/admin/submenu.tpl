<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<li class="{if $selected EQ 'index'}ui-tabs-selected ui-state-active{/if} ui-state-default ui-corner-top"><a class="" href="/admin/tournaments">Списък турнири</a></li>
	<li class="{if $selected EQ 'approve'}ui-tabs-selected ui-state-active{/if} ui-state-default ui-corner-top"><a class="" href="/admin/tournaments/approve">Чакащи одобрение</a></li>
	<li class="{if $selected EQ 'add_tournament'}ui-tabs-selected ui-state-active{/if} ui-corner-top  ui-state-default"><a class="" href="/admin/tournaments/add">Добави турнир</a></li>
	{if $selected EQ 'edit_tournament'}<li class="ui-tabs-selected ui-state-active ui-corner-top ui-state-default"><a href="#">Редактиране - турнир</a></li>{/if}
	{if $selected EQ 'view_members'}<li class="ui-tabs-selected ui-state-active ui-corner-top ui-state-default"><a href="#">Членове на {$tournament}</a></li>{/if}
	
</ul>