{include file='tournaments/admin/submenu.tpl'}
<form method="POST">
	<table {if $users|@is_array && $users|@count GT 0}id="logtable"{/if} class="tablesorter"> 
	<thead> 
	<tr> 
	    <th><input type="checkbox"  safari=1 class="jquery-safari-checkbox" value="" onclick="checkUncheck('ids[]')" /></th> 
	    <th>id</th>
	    <th>снимка</th> 
	    <th>потребителско име</th>
	    <th>име</th>
	    <th>иска членство в</th>
	    <th></th>
	</tr> 
	</thead> 
	<tbody> 
	{foreach from=$users item=i key=k}
		<tr>
		<td><input type="checkbox" name="ids[]" safari=1 class="jquery-safari-checkbox" value="{$i.id}"/></td>
		<td>{if $i.avatar NEQ ""}<a href="/users/profile/{$i.user_id}" target="_blank"><img src="{$USER_IMG_PATH}{$i.user_id/1000|int}/s_{$i.avatar}.jpg" alt="{$i.username}" /></a>{/if}</td>
		<td>{$i.id}</td>
		<td><a href="/users/profile/{$i.user_id}" target="_blank">{$i.username}</a></td>
		<td>{$i.name}</td>
		<td>{$i.tourn_name}</td>
		<td align="center"><a href="/admin/tournaments/approve_user/{$i.id}" class="link">Одобри</a></td>
		</tr>
	{/foreach} 
	</tbody>
	</table>

	<div class="floatRight">с избраните: <input type="submit" name="approve" value="Одобри" class="button" onclick="return confirm('Сигурни ли сте?')" /> <input type="submit" name="delete" value="Откажи одобрение" class="button" onclick="return confirm('Сигурни ли сте?')" /></div>
</form>
<div class="pages">{$pages}</div>