{literal}
<script type="text/javascript">
	$(function(){
		$(".join-tourn").click(function(){
			t = $(this);
			id = t.attr("id").replace("join-", "");
			$.post("/ajax/tournaments_member/join", {"id": id}, function(data){
				t.parent().html('Вашата молба за участие е изпратена.');
			});
			
			return false;
		});

		$(".part-tourn").click(function(){
			t = $(this);
			id = t.attr("id").replace("part-", "");
			$.post("/ajax/tournaments_member/part", {"id": id}, function(data){
				t.parent().html('Вашия отказ от участие е приет.');
			});

			return false;
		});
	});
</script>
{/literal}
<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Турнирни правила</h2>
							</div>
							<div class="real_content long_input">
							{foreach from=$tournaments item=f}
								<div class="profile">
									{if isset($f.avatar)}
										<div class="img left"><img src="{$USER_IMG_PATH}{$f.id/1000|int}/l_{$f.avatar}.jpg" alt="{$f.username}" /></div>
									{/if}
									<div class="data left">										
										<!--<p><span>Име:</span> {$f.name}</p>
										<p><span>Добавен:</span> {$f.date_entered}</p>
										<p><span>Начало:</span> {$f.start_date}</p>
										<p><span>Край:</span> {$f.end_date}</p>
										<p><span>Участници:</span> {$f.members}</p>
										{if $smarty.session.UserID}
											{if $f.member == -1}
												<p><span><a href="/" id="join-{$f.id}" class="join-tourn" >Включи се</a></span></p>
											{elseif $f.member == 1}
												<p><span><a href="/" id="part-{$f.id}" class="part-tourn">Откажи се</a></span></p>
											{else}
												<p>Вашата молба за участие чака одобрение.</p>
											{/if}
										{else}
											<p>За да се включите в турнира трябва да сте регистриран потребител.</p>-->
											<div style="padding:0 15px 0 15px;">
											<p>
											 &nbsp;</p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm;">
											 <font size="4" style="font-weight:bold;">Тези правила важат за турнирите в Сварка БГ. </font></p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4" style="font-weight:bold;">Общи условия</font></p>
											<ol>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0cm">
											   <font size="4">Всички турнири ще започват точно в началния час, посочен на турнирното табло. Сварка БГ си запазва правото да отлага или отменя турнири без предизвестие.</font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0.49cm">
											   <font size="4">Важната информация за всеки турнир, включително структура на залаганията, продължителността на игрите и информация за почивките, можете да откриете, като натиснете бутона &quot;Информация за турнира&quot; на турнирното табло. Сварка БГ си запазва правото да променя параметрите на всеки турнир по всяко време и без предизвестие.</font></p>
											 </li>
											</ol>
											<ul>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											   <font size="4">Местата се разпределят на случаен принцип. Не се допуска сменяне на мястото. </font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											   <font size="4">За да започне играта, бутонът ще бъде поставен в Място 1. </font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0.49cm">
											   <font size="4">При турнирите е разрешен три вида залог: обикновен, тъмно и светло. </font></p>
											 </li>
											</ul>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 1.27cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4">А. Обикновен залог</font></p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 1.27cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4">Фиксиран вход, който се сменя на определено време от компютъра, в зависимост от участниците в турнира. </font></p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 1.27cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4">Б. Залог на тъмно</font></p>
											<p align="JUSTIFY" style="margin-left: 1.27cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4"><span lang="bg-BG">преди да си види картите, първият играч вляво от раздаващия има право да заложи определена сума </span></font><font size="4"><span lang="bg-BG"><i>на тъмно</i></span></font><font size="4"><span lang="bg-BG">; всеки следващ вляво от него има право да притъмнява, което значи двойно или тройно или четворно залагане на всеки следващ играч;</span></font></p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 1.25cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4">В. Залог на светло </font></p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 1.25cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4">залагане, при което един или повече играчи са останали с малко пари и не могат да влезат да играят на масата. </font></p>
											<ul>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0cm">
											   <font size="4">Наградите ще се присъждат както е посочено в турнирното табло.</font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											   <font size="4">За да можете да участвате в турнир с материални награди е нужно да захраните вашата сметка с определената сума за участие. Играчите с недостатъчно средства по сметката си ще бъдат елиминирани от турнира. Сметката се зарежда със SMS.</font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" style="margin-bottom: 0cm">
											   <font size="4"><span lang="bg-BG"><b>Отписване</b></span></font><font size="4"><span lang="bg-BG">: Повечето турнири разрешават отписване до няколко минути преди началото на турнира. Точното време, в което приключва възможността за отписване, е посочено в турнирната информация за всеки турнир и може да се различава при различните турнири. Играчите, които спечелят мястото си през сателит, могат да се отпишат ако е позволено в съответния турнир. В заманя на мястото си обаче те ще получат турнирни пари (T-Пари), пари за събития (W-Пари) или входен билет. T-Парите и W-Парите са кредити, които могат да се използват за закупуване на входа за други турнири на Сварка БГ. За повече информация за Т-Парите и W-Парите, натиснете </span></font><font color="#000080"><span lang="zxx"><u><a class="western" href="http://www.pokerstars.com/bg/poker/tournaments/#tdollars"><span lang="bg-BG">тук</span></a></u></span></font><font size="4"><span lang="bg-BG">.</span></font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0.49cm">
											   <font size="4">Обърнете внимание: Времето за регистрация и отписване може да е различно при различните типове турнири. Не всички турнири предлагат възможността за отписване - подобни турнири обикновено ще бъдат обозначени към момента на регистрация. Вижте турнирното табло и прозореца с информация за турнира за точните детайли около регистрацията за всеки турнир. Сварка БГ си запазва правото да променя времето за регистрация и отписване без предизвестие.</font></p>
											 </li>
											</ul>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4" style="font-weight:bold;">Елиминации</font></p>
											<ol start="11">
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0cm">
											   <font size="4">Турнирът приключва, когато един играч събере всички чипове в турнира. </font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											   <font size="4">При някой видове турнири, когато всички играчи получат еднаква награда, например, ако даден турнир присъжда шест еднакви награди, и няма ранглиста, турнирът може да приключи, когато останат шестима играча. .</font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											   <font size="4">Сварка БГ използва правилото за придвижване на бутона напред за своите турнири само за определяне на началното раздаване в турнира. </font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											   <font size="4">В останалите случаи бутонът остава при играча, който е спечелил предната игра. </font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											   <font size="4">В края на всяка ръка бутонът се мести по посока на часовниковата стрелка и отива при играча, който е спечелил раздаването.</font></p>
											 </li>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0.49cm">
											   <font size="4">С елиминирането на играчите от турнира, софтуерът ще &quot;разваля&quot; масите, за да запълни наличните празни места. Разпределянето на играчите на новите маси ще се извършва на случаен принцип. Понякога софтуерът ще балансира масите, за да се осигури еднакъв (или възможно най-близък до еднакъв) брой играчи на всички маси. Когато бъдат елиминирани достатъчно играчи, всички оцелели играчи ще бъдат преместени на &quot;финалната маса&quot;.</font></p>
											 </li>
											</ol>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4" style="font-weight:bold;">Почивки</font></p>
											<ol start="17">
											 <li>
											  <p align="JUSTIFY" style="margin-top: 0.49cm; margin-bottom: 0cm">
											   <font size="4"><span lang="bg-BG">Графикът на почивките за даден турн</span></font><font size="4"><span lang="bg-BG">ир може да бъде намерен в прозореца с информация за съответния турнир, който можете да отворите от турнирното табло. Съветваме играчите да се запознаят с графика на почивките за всеки турнир, който желаят да играят, тъй като продължителността и началото на почивките може да се различават при различните турнири. Сварка БГ предлага два варианта за почивките: </span></font></p>
											  <ol>
											   <li>
											    <p align="JUSTIFY" style="margin-bottom: 0.49cm">
											     <font size="4"><span lang="bg-BG">Турнирите със </span></font><font size="4"><span lang="bg-BG"><b>синхронизирани</b></span></font><font size="4"><span lang="bg-BG"> почивки ще излизат в почивка 55 минути след всеки кръгъл час. Например, ако даден турнир започва в 07:25, почивките в него ще започват от 07:55, 08:55, 09:55 и така през един час до края на турнира.</span></font></p>
											   </li>
											  </ol>
											 </li>
											</ol>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 2.54cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4">Този тип турнири са обозначени, като в турнирното табло, в прозореца с турнирна информация и понякога в съобщението в турнирното табло това е опоменато.</font></p>
											<ol start="17">
											 <li>
											  <ol start="2">
											   <li>
											    <p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											     <font size="4">Другите турнири ще излизат в почивка на всеки кръгъл час от началото на турнира. Например, ако даден турнир започва в 07:25, първата почивка ще започне в 08:25.</font></p>
											   </li>
											  </ol>
											 </li>
											</ol>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4">При всички случаи турнирите ще изчакат приключването на ръцете на всички маси, преди да започне почивката. Това означава, че на някои маси почивката ще продължи малко по-дълго, отколкото на други.</font><br />
											 <br />
											 &nbsp;</p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4" style="font-weight:bold;">Прекъсване на връзката и отсъствие</font></p>
											<ul>
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0cm">
											   <font size="4">С участието си в турнир всеки играч приема риска от прекъсване на връзката с интеренет, дължащо се на проблеми с връзката между неговия компютър и сървъра, бавна връзка, забиване или други проблем с компютъра на играча или с интернет връзката. </font></p>
											  <ul>
											   <li>
											    <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											     <font size="4">Сварка БГ не носи отговорност за прекъсване връзката на някой играчи, освен в случай на проблем със сървъра. </font></p>
											   </li>
											   <li>
											    <p align="JUSTIFY" style="margin-bottom: 0cm">
											     <font size="4"><span lang="bg-BG">Въпреки че всеки играч е отговорен за връзката си с интернет, Сварка БГ се старае да защити играчите, когато връзката им прекъсне във финалните фази на турнир с истински пари, като им дава допълнително време, за да се свържат. Правилата за активирането на </span></font><font color="#000080"><span lang="zxx"><u><a class="western" href="http://www.pokerstars.com/bg/poker/tournaments/rules/disconnect/"><span lang="bg-BG">допълнителното време при прекъсване на връзката</span></a></u></span></font><font size="4"><span lang="bg-BG"> са описани </span></font><font color="#000080"><span lang="zxx"><u><a class="western" href="http://www.pokerstars.com/bg/poker/tournaments/rules/disconnect/"><span lang="bg-BG">тук</span></a></u></span></font><font size="4"><span lang="bg-BG">. </span></font></p>
											   </li>
											   <li>
											    <p align="JUSTIFY" lang="bg-BG" style="margin-bottom: 0cm">
											     <font size="4">Ако даден играч не играе навреме, независимо дали е свързан или не, счита се, че се е отказал и пропуска раздаването.</font></p>
											   </li>
											   <li>
											    <p align="JUSTIFY" style="margin-bottom: 0.49cm">
											     <font size="4"><span lang="bg-BG">Ако даден играч не е свързан преди началото на ръка, той ще получи карти и ще даде вход за играта. Няма правило срещу възможността някой играч да реши да отсъства - в подобен случай той ще продължи да прави залози и да получава карти. Двама или повече играчи </span></font><font size="4"><span lang="bg-BG"><b>нямат</b></span></font><font size="4"><span lang="bg-BG"> право да се уговорят да отсъстват едновременно, независимо дали са на еднакви маси или не.</span></font></p>
											   </li>
											  </ul>
											 </li>
											</ul>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4" style="font-weight:bold;">Чат</font></p>
											<p align="JUSTIFY" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4"><span lang="bg-BG">Това правило цели да допълни, </span></font><font color="#000080"><span lang="zxx"><u><a class="western" href="http://www.pokerstars.com/bg/poker/room/policies/"><span lang="bg-BG">правилата на залата</span></a></u></span></font><font size="4"><span lang="bg-BG">, а не да ги замести. В тях ще намерите повече информация за приемлив и неприемлив чат.</span></font></p>
											<ol start="19">
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											   <font size="4">Играчите, без значение дали са в ръката или не, нямат право да обсъждат ръцете, докато действието не приключи. Играчите са длъжни да пазят останалите играчи в турнира по всяко време. Обсъждането на раздадените карти и вероятностите за ръцете не е разрешено. За обсъждането на ръка по време на игра може да бъде наложено наказание.</font></p>
											 </li>
											</ol>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 1.27cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <br />
											 <br />
											 &nbsp;</p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4" style="font-weight:bold;">Неетична игра</font></p>
											<ol start="20">
											 <li>
											  <p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											   <font size="4">Сварката е индивидуална, а не отборна игра. Всяко действие или чат, целящо да помогне на друг играч, е неетично и е забранено.</font></p>
											 </li>
											</ol>
											<p align="JUSTIFY" lang="bg-BG" style="margin-left: 1.27cm; margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <br />
											 &nbsp;</p>
											<p align="JUSTIFY" lang="bg-BG" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4" style="font-weight:bold;">Проблеми със сървъра</font></p>
											<ol start="21">
											 <li>
											  <p align="JUSTIFY" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											   <font size="4"><span lang="bg-BG">В случай на проблем със сървъра, текущите ръце ще бъдат отменени. Сумата на всеки играч ще бъде върната до размера, който е бил към началото на ръката. При определени обстоятелства, когато даден турнир трябва да бъде отменен поради проблем или друга причина, играчите ще бъдат компенсирани според </span></font><font color="#000080"><span lang="zxx"><u><a class="western" href="http://www.pokerstars.com/bg/poker/tournaments/rules/cancellations/"><span lang="bg-BG">политиката на Сварка БГ за отменяне на турнири</span></a></u></span></font><font size="4"><span lang="bg-BG">. </span></font></p>
											 </li>
											</ol>
											<p align="JUSTIFY" style="margin-top: 0.49cm; margin-bottom: 0.49cm">
											 <font size="4"><span lang="bg-BG">В случай на спорове, решението на ръководството на Сварка БГ ще бъде окончателно.</span></font>
											 </p>
											 </div>
										{/if}
									</div>
									<div class="clear"><!--  --></div>
								</div>
								{foreachelse}
									Нямате добавени приятели.
								{/foreach}						
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>