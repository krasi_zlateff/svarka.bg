		{literal}
		<script type="text/javascript">
			$(function(){
				$("#messages textarea").focus();

				$("#emoticons img").click(function(){
					$("#messages textarea").val($("#messages textarea").val() + $(this).attr("alt"));
				});

				$("#messages .input-submit").click(function(){
					if ($("#messages textarea").val() == '')
					{
						$("#messages textarea").parent().after("<div class='form_row'><p class='error'>Не сте въвели съобщение.</p></div>");
						$("#messages textarea").focus();

						return false;
					}
				});
			});
		</script>
		{/literal}

		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Писане на съобщение</h2>
							</div>
							<div class="real_content" id="messages">
								<form action="" method="post">
									<input type="hidden" name="reciever_id" value="{$user.id}" />
									<div class="form_row"><label class="label" for="email">До: </label> <input class="input-text" disabled="disabled" name="to" value="{$user.username}" type="text" /></div>
								     <div class="form_row"><label class="label" for="password">Относно: </label> <input maxlength="255" class="input-text" id="password" name="title" value="{if isset($reply_msg)}Re: {$reply_msg.title}{/if}" type="text" /></div>
								     <div class="form_row">
								     	<label class="label" for="password">Текст: </label>
								     	<textarea class="input-text textarea" rows="20" cols="40" name="text" >{if isset($reply_msg)}
От: {$user.username}
Изпратено на: {$reply_msg.date_sent}
								     	{/if}</textarea>
								     </div>
								     <div class="from_row">
								     	{include file='layout/includes/emoticons.tpl'}
								     </div>
								     <div class="form_row"><label class="label">&nbsp;</label><input class="input-submit input-submit-long pointer" type="submit" name="enter" value="Изпрати" /></div>
								</form>
							</div>
						</div>
					</div>

					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>