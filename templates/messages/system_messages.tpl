	<div id="flash_message" class="popup_wrapper_msg" align="left">
		<div class="title">
			<h2 class="{if $message.Type EQ 1}red{else}green{/if}">{if $message.Type EQ 1}Грешка{else}Успех{/if}</h2>
		</div>
		<div class="real_content">
			<div class="sys_msg">{$message.Text}</div>
			<div class="ok_button"><input id="ok_btn" type="button" class="input-button input-submit input-submit-long" value="напред" onclick="remove_dim_for_msg({$container});" /></div>
		</div>
	</div>

{literal}
<script type="text/javascript">
	$(document).ready(function(){
		add_dim_for_msg();
		$(document).keyup(function(e){

			if (e == null) { // ie
			    keycode = event.keyCode;
			} else { // mozilla
			    keycode = e.which;
			}

			if(keycode == 13 || keycode == 27 || keycode == 32){
				$('#ok_btn').click();
			}

		});
	});
</script>
{/literal}