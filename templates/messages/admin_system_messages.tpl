{if $message.Text NEQ ''}
				{if $message.Type EQ '1'} {* error *}
					{assign var=classes value='ui-state-error ui-corner-all'}
					{assign var=classes_p value='ui-icon ui-icon-alert'}
					{assign var=txt value='Грешка'}	
				{elseif $message.Type EQ '0'} {* success *}
					{assign var=classes value='ui-state-highlight ui-corner-all'}
					{assign var=classes_p value='ui-icon ui-icon-info'}
					{assign var=txt value='Успех'}
				{/if}
	<div class="ui-widget" id="core_msg">		
		<div class="{$classes}" style="padding: 1em .7em;"> 
			<p><span class="{$classes_p}" style="float: left; margin-right: .3em;"></span> 
			<strong>{$txt}:</strong> {$message.Text}</p>
		</div>
		<br />
	</div>
{/if}