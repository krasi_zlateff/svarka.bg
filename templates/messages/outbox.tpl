		{literal}
		<script id="msg-template" type="text/x-jquery-tmpl">
			<div class="msg_text">
				<div class="header">
					{{if avatar}}
						<div class="img left"><img src="${avatar}" alt="${user}" /></div>
					{{/if}}
					<div class="data left">
						<p><span>До:</span>{{if sender_id > 0}} <a href="/users/profile/${reciever_id}" title="${user}">${user}</a>{{else}}${user}{{/if}}</p>
						<p><span>Относно:</span> ${title}</p>
						<p><span>Изпратено:</span> ${date_sent}</p>
					</div>
					<div class="clear"></div>
					<div class="text">
						{{html text}}
					</div>
				</div>				
			</div>
		</script>
		<script type="text/javascript">
			$(function(){
				var maxHeight = 0;
				$("#msg_list .row").children().each(function(i){
					if ($(this).height() > maxHeight)
							maxHeight = $(this).height();
				});

				$("#msg_list .row").children().not(".clear").height(maxHeight);

				$("#msg_list .row").bind("mouseover mouseout", function(){
					$(this).toggleClass("hover");
				});

				$("#msg_list .row").click(function(){					
					$("#msg_holder").html('<img class="circle-loading" src="/i/design/circle-loader.gif" alt="Зареждане..." />')
					id = $(this).attr("id").replace("read-", "");

					$.post("/ajax/read_msg", {"id": id}, function(data){
						$("#msg_holder").html($("#msg-template").tmpl(data)).attr("tabindex",-1).focus();
					}, "json");					
				});
			});			
		</script>
		{/literal}
		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Изходящи съобщения</h2>
							</div>
							<div class="real_content long_input">
								<div id="msg_list">
									{foreach from=$msgs item=m key=k}
										<div class="row pointer {if $m.read == 0}read{/if}" id="read-{$m.id}">
											<div class="col-checkbox left">
												<input type="checkbox" id="del-{$m.id}" value="1" />
											</div>
											<div class="col-username left">
												{if $m.sender_id > 0}
													{$m.username}
												{else}
													Администрация
												{/if}
											</div>
											<div class="col-title left">
												{$m.title}
											</div>
											<div class="col-date left">
												{$m.date_sent}
											</div>
											<div class="clear"><!--  --></div>
										</div>
									{foreachelse}
										<p>Няма съобщения в тази секция.</p>
									{/foreach}
																			
								</div>
								<div id="msg_holder"></div>							
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>