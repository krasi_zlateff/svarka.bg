<script type="text/javascript" src="/js/jquery.validate.js"></script>
<script type="text/javascript" src="/js/jquery.jcrop.min.js"></script>
<link rel="stylesheet" href="/css/jquery.jcrop.css" type="text/css" />
{literal}
<script type="text/javascript">
	$(function(){
		$("#cropper").Jcrop({
	        "bgColor":    "black",
	        "bgOpacity":   0.4,
	        "aspectRatio": 1,
	        "minSize":     [60, 60],
	        "maxSize":     [60, 60],
	        "setSelect":   [0, 0, 60, 60],
	        "onSelect":    setCropVals
		});

		$("#edit-avatar").click(function(){
			left = ( parseInt($(document).width()) - parseInt($("#cropper").width()))/2;
			$("#crop-box").css("left", left + "px").css("visibility", "visible");
		});

		$("#close-crop").click(function(){
			$("#crop-box").css("visibility", "hidden");
		});
	});

	function setCropVals(c)
	{
		$("#x-coord").val(c.x);
		$("#y-coord").val(c.y);
		$("#w-crop").val(c.w);
		$("#h-crop").val(c.h);
	}
</script>
{/literal}
		<div id="crop-box">
			<img id="cropper" src="{$USER_IMG_PATH}{$user.id/1000|int}/l_{$user.avatar}.jpg" alt="{$user.username}" />
			<form action="" method="POST">
				<input type="hidden" id="crop-path" name="path" value="{$USER_IMG_PATH}{$user.id/1000|int}/l_{$user.avatar}.jpg" />
				<input type="hidden" id="crop-name" name="img_name" value="{$user.avatar}" />
				<input type="hidden" id="x-coord" name="x" value="0" />
				<input type="hidden" id="y-coord" name="y" value="0" />
				<input type="hidden" id="w-crop" name="w" value="100" />
				<input type="hidden" id="h-crop" name="h" value="100" />
				<input type="submit" id="submit-crop" name="crop" value="Изпрати" />
				<input type="button" id="close-crop" name="crop" value="Затвори" />
			</form>
		</div>
		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">

					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Лични данни</h2>
							</div>
							<div class="real_content long_input">
								<form method="post" action="" id="account" enctype="multipart/form-data">
									<div class="form_row"><label class="label" for="name">Потребителско име </label><b>{$user.username|escape}</b></div>
									<div class="form_row"><label class="label" for="name">Име </label><input class="input-text" id="name" type="text" name="name" value="{$user.name|escape}"/></div>
									<div class="form_row"><label class="label" for="family">Фамилия </label><input class="input-text" id="family" type="text" name="family" value="{$user.family|escape}"/></div>
									<div class="form_row"><label class="label" for="email">Имейл <font color="red">*</font></label><input class="input-text" id="email" type="text" name="email" value="{$user.email|escape}"/></div>
									<div class="form_row"><label class="label">Пол</label>
										<input type="radio" class="input-radio" name="sex" value="1" id="man" {if $user.sex EQ "1"}checked="checked"{/if} /><label class="label-radio" for="man"> Mъж</label>
										<input type="radio" class="input-radio" name="sex" value="2" id="woman" {if $user.sex EQ "2"}checked="checked"{/if} /><label class="label-radio" for="woman"> Жена</label>
									</div>
									<div class="form_row">
										<label class="label" for="city">Град </label>
											<select name="city_id" id="city" class="">
												<option value="0">-</option>
												{html_options options=$cities selected=$user.city_id}
											</select>
									</div>
									<div class="form_row">
										<label class="label">Дата на раждане </label>
										{html_select_date all_empty='ден' selected_day=$user.b_day start_day="1" end_day="31" display_years=false display_months=false  style="width: 45px" class="" name="day"}
										&nbsp;{html_select_date all_empty='месец'  selected_month=$user.b_month start_month="1" end_month="12" display_years=false display_days=false  style="width: 82px" class="" name="month"}
										&nbsp;{html_select_date all_empty='година' selected_year=$user.b_year start_year="1950" reverse_years=true end_year=$allowed_year display_months=false display_days=false  style="width: 52px" class="" name="year"}
									</div>
									{if $user.avatar NEQ ""}
									<div class="form_row">
										<label class="label">Снимка </label><img src="{$USER_IMG_PATH}{$user.id/1000|int}/l_{$user.avatar}.jpg" alt="{$user.username}" />
									</div>
									<div class="form_row"><label class="label">&nbsp;</label><input class="checkbox" type="checkbox" value="1" name="del_photo" id="del_photo"/><label for="del_photo">Изтрий снимката</label></div>
									<div class="form_row">
										<label class="label">Аватар </label><img src="{$USER_IMG_PATH}{$user.id/1000|int}/av_{$user.avatar}.jpg?a={$smarty.now}" alt="{$user.username}" />
										<span class="pointer" id="edit-avatar">Редактирай</span>
									</div>
									{/if}
									<div class="form_row"><label class="label" for="photo">Смени снимка </label><input class="input-text file" id="photo" type="file" name="avatar" value=""/></div>
									<div class="form_row"><label class="label" for="for_me">За вас </label><textarea name="for_me" class="textarea input-text" id="for_me">{$user.for_me|escape}</textarea></div>
									<div class="form_row"><label class="label" for="icq">Icq номер </label><input class="input-text" id="icq" type="text" name="icq" value="{$user.icq|escape}"/></div>
									<div class="form_row"><label class="label" for="skype">Skype име </label><input class="input-text" id="skype" type="text" name="skype" value="{$user.skype|escape}"/></div>
									<div class="form_row"><label class="label" for="msn">MSN номер </label><input class="input-text" id="msn" type="text" name="msn" value="{$user.msn|escape}"/></div>
									<div class="form_row"><label class="label" for="yahoo">Yahoo номер </label><input class="input-text" id="yahoo" type="text" name="yahoo" value="{$user.yahoo|escape}"/></div>
									<div class="form_row"><label class="label" for="google">Google толк </label><input class="input-text" id="google" type="text" name="google_talk" value="{$user.google_talk|escape}"/></div>
									<div class="form_row"><label class="label">&nbsp;</label><input class="input-submit input-submit-long" type="submit" name="account" value="запази промените" /></div>
								</form>
							</div>
						</div>
					</div>

					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>


{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$("#account").validate({
			rules:{
				email:{
					required: true,
					email: true
				}
			},
			messages: {
				email: "Не сте въвели имейл!"
			},
			errorClass : "errormsg"
		});
	});
</script>
{/literal}