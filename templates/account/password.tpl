<script type="text/javascript" src="/js/jquery.validate.js"></script>

		<div class="content_part_3 content_part_3_inner">
			<div class="box">
				<div class="box_inner">
					
					<div class="content_text content_text_inner">
						<div class="content_inner">
							<div class="title">
								<h2>Смяна на парола</h2>
							</div>
							<div class="real_content long_input">
								<form method="post" id="change_password" action="">
									<div class="form_row"><label class="label" for="password_current">Сегашна парола<span>*</span> </label><input type="password" class="input-text" name="password_current" id="password_current" /></div>
									<div class="form_row"><label class="label" for="password_new">Нова парола<span>*</span> </label><input type="password" class="input-text" name="password_new" id="password_new" /></div>
									<div class="form_row"><label class="label" for="password_new1">Повтори нова парола<span>*</span> </label><input type="password" class="input-text" name="password_new1" id="password_new1" /></div>
									<div class="form_row"><label class="label">&nbsp;</label><input type="submit" value="запамети" name="update_password" class="input-submit input-submit-long" /></div>
								</form>
							</div>
						</div>
					</div>
					
					{include file='layout/includes/right_inner.tpl'}
					<div class="clear box_border_down">&nbsp;</div>
				</div>
			</div>
		</div>


{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$("#change_password").validate({
			rules:{
				password_current:{
					required: true
				},
				password_new:{
					required: true
				},
				password_new1:{
					required: true,
					equalTo: "#password_new"
				}
			},
			messages: {
				password_current: "Не сте въвели сегашна парола!",
				password_new: "Не сте въвели нова парола!",
				password_new1: "Двете пароли не съвпадат!"
			},
			errorClass : "errormsg"
		});
	});
</script>
{/literal}