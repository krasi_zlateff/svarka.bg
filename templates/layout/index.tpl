<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{$PAGE_TITLE|strip_tags:truncate:90}</title>
<base href="{$WEBPATH}" />
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<link rel="stylesheet" href="/css/svarka.css" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="/js/ie6png.js"></script>
<![endif]-->
<script type="text/javascript" src="/js/jquery-1.5.min.js"></script>
<script type="text/javascript" src="/js/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="/js/jquery.corner.js"></script>
{if !($smarty.session.UserID GT 0)}
<script type="text/javascript" src="/js/login.js"></script>
{/if}
<script type="text/javascript" src="/js/online-arrow.js"></script>
{if $message.Text NEQ ''}
<script type="text/javascript" src="/js/sys_message.js"></script>
{/if}
{if $smarty.session.UserID GT 0}
<script type="text/javascript" src="/js/personal_menu.js"></script>
{/if}

{literal}
<script type="text/javascript">
/*
 * Data messages model
 *
 * Flags:
 * 	"0" - instant message
 * 	"1" - game invitation
 * 	"2" - tables list update ???
 *
 *
 */
$(function(){
//	var mainListener;

	function addTopMessagesCont()
	{
//		$("#top-messages-cont").fadeOut(function(){$(this).remove();});
		div = "<div id='top-messages-cont'></div>";
		$("body").append(div);
	}

	$(".close-msg").live("click", function(){
		$(this).parent().fadeOut(2000, function(){$(this).remove();})
	});

	function getMessages()
	{
		$.post("/game/idle",{}, function(data){
			if (data.count > 0)
			{
				if (!$("#top-messages-cont").is(":visible"))
					addTopMessagesCont();
				for(var i in data.messages)
				{
					if (data.messages[i].type == 1)
					{
						$("#top-messages-cont").prepend("<div>" + data.messages[i].data + "<div class='close-msg pointer'></div></div>");

						if ($("#top-messages-cont").children().length > 5)
						{
							$("#top-messages-cont").children().each(function(j){
								if (j >= 7)
									$(this).fadeOut(4000, function(){$(this).remove();});
							});
						}
					}
					else if (data.messages[i].type == 2)
					{
						;
					}
				}

				if (data.last)
				{
					$.post('/game/set_read', {"time": data.last.time});
				}
			}
		}, "json");
	}

	mainListener = setInterval(getMessages, 2000);

	$(".send-game-invitation").click(function(){
		id = $(this).attr("id").replace("invite-", "");
		$.post('/game/send_game_invitation', {"reciever": id, "game_id": 1}, function(){
			;
		});
		return false;
	});
});
</script>
{/literal}
</head>

<!--[if IE 6]>
{literal}
<style type="text/css">
.user_label{
	width: 100px;
	text-align: center;
}
.user_label_arrow{
	width: 130px;
}
{/literal}
</style>
<![endif]-->


<body>

<div class="wrapper">

	{include file='layout/includes/header.tpl'}

	<div class="content_wrapper" align="center">
		{$body}
	</div>

	{include file='layout/includes/footer.tpl'}


</div><!--/wrapper-->

<!--[if IE 6]>
{literal}
<script type="text/javascript">
$(document).ready(function(){

	try{
		DD_belatedPNG.fix('.logo img');
  		DD_belatedPNG.fix('.user_label_arrow');
		DD_belatedPNG.fix('#overlay');
		DD_belatedPNG.fix('.header_content');
		DD_belatedPNG.fix('.close_menu');
	}catch(err){

	}
});
</script>
{/literal}
<![endif]-->

</body>
</html>
