	<div class="footer_wrapper" align="center">
		<div class="content_footer">
			<div class="copyright">
				Copyright © 2010 {$SITENAME}  All rights reserved.&nbsp;&nbsp;
				<a href="#" title="RSS {$SITENAME}"><img class="rss" src="/i/design/rss.png" alt="RSS {$SITENAME}" /> RSS {$SITENAME}</a>
			</div>
			<div class="made_by">Made by: <a href="http://tara-soft.net" target="_blank" title="TaraSoft">TaraSoft</a></div>
		</div>
	</div>