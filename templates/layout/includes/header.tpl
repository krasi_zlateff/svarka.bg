	<div id="overlay" class="overlay">&nbsp;</div>
	<div class="header_wrapper" align="center">
			<div class="header_content">
				<div class="logo"><a href="/" title="{$SITENAME}"><img src="i/design/logo2.png" alt="{$SITENAME}" /></a></div>

				<div class="login_users_online" align="right">
					{if !($controllerRun EQ "users" && $methodRun EQ "login")}
					<div class="login_wrapper">
						{if $smarty.session.UserID GT 0}
						<div class="user_welcome" align="center">
							Здравейте, <a href="/users/profile/{$smarty.session.UserID}" title="{$smarty.session.username}">{$smarty.session.username}</a><span class="toggle"> | <a href="javascript:;" id="settings" title="настройки">настройки</a>(<a href="/messages/inbox" title="Входна кутия">{$countInbox}</a>)</span>
						</div>

						<div class="settings_wrapper">
							<div class="close_menu">&nbsp;</div>
							<div class="settings">
								<div class="settings_option">
									<a href="/account/edit">лични данни</a>
								</div>
								<div class="settings_option">
									<a href="/account/password">смяна на парола</a>
								</div>
								<div class="settings_option">
									<a href="/messages/inbox">Входящи съобщения({$countInbox})</a>
								</div>
								<div class="settings_option">
									<a href="/messages/outbox">Изходящи съобщения</a>
								</div>
								<div class="settings_option">
									<a href="/users/friends">Приятели</a>
								</div>
								<div class="settings_option">
									<a href="/users/logout">изход</a>
								</div>
							</div>
						</div>
						{else}
						<form method="post" action="/users/login">
							<div class="login_field"><input type="text" name="email" class="input-text" value="  e-mail..." onfocus="if($(this).val() == '  e-mail...') $(this).val('');" onblur="if($(this).val() == '') $(this).val('  e-mail...');" id="email"/></div>
							<div class="login_field">
								<input type="text" class="input-text" value="  парола..." name="label_pass" id="label_pass" />
								<input type="password" class="input-text" name="password" id="login_pass" style="display:none;"  />
								<br/><a id="register-link" href="/users/register/">регистрация</a> <a href="/account/lost_password/">забравена парола</a>
							</div>
							<div class="login_submit"><input class="input-submit enter" type="submit" value="" /></div>
						</form>
						{/if}
					</div>
					{else}
						<div class="login_wrapper no_bckg">&nbsp;</div>
					{/if}

					{if $users_with_avatar|@is_array && $users_with_avatar|@count GT 0}
					<div class="users_wrapper">
						{foreach from=$users_with_avatar item=i key=k name=n}
						<div class="user_avatar{if $smarty.foreach.n.last} user_avatar_last{/if}">

							<div class="user_label_wrapper user_label_wrapper_{$smarty.foreach.n.iteration}">
								<div class="user_label">{$i.username|escape} - 250$</div>
								<div class="user_label_arrow"></div>
							</div>

							<div class="s_align">
								<a href="/users/profile/{$i.id}" title="{$i.username|escape} - 250$">
									<img src="{$USER_IMG_PATH}{$i.id/1000|int}/s_{$i.avatar}.jpg" alt="{$i.username|escape} - 250$" />
								</a>
							</div>
						</div>
						{/foreach}
					</div>
					{/if}
				</div>

				{include file='layout/includes/navigation.tpl'}

			</div>
	</div>

	<div class="msg_container" align="center">
		<div class="popup_container">
			{if $message.Text NEQ ''}
				{include file='messages/system_messages.tpl'}
			{/if}
		</div>
	</div>