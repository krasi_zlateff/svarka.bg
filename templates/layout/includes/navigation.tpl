			<div class="navigation_wrapper">
				<div class="navigation">

					<div class="{if $controllerRun EQ "main" && $methodRun EQ "index"}nav_btn_active{else}nav_btn{/if}">
						{if $controllerRun EQ "main" && $methodRun EQ "index"}
						<div class="nav_btn_left">&nbsp;</div>
						<div class="nav_btn_top">
						{/if}
							<a href="/" title="Начало">Начало</a>
						{if $controllerRun EQ "main" && $methodRun EQ "index"}
						</div>
						<div class="nav_btn_right">&nbsp;</div>
						{/if}
					</div>
					{if !($controllerRun EQ "main" && $methodRun EQ "index")}
					<div class="spatia">&nbsp;</div>
					{/if}

					<div class="{if $controllerRun EQ "game" && $methodRun EQ "tables"}nav_btn_active{else}nav_btn{/if}">
						{if $controllerRun EQ "game" && $methodRun EQ "tables"}
						<div class="nav_btn_left">&nbsp;</div>
						<div class="nav_btn_top">
						{/if}
							<a href="/game/tables" title="Маси">Маси</a>
						{if $controllerRun EQ "game" && $methodRun EQ "tables"}
						</div>
						<div class="nav_btn_right">&nbsp;</div>
						{/if}
					</div>
					{if !($controllerRun EQ "game" && $methodRun EQ "tables" )}
					<div class="spatia">&nbsp;</div>
					{/if}


					<div class="{if $controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 1}nav_btn_active{else}nav_btn{/if}">
						{if $controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 1}
						<div class="nav_btn_left">&nbsp;</div>
						<div class="nav_btn_top">
						{/if}
							<a href="/static/index/1" title="Правила на играта">Правила на играта</a>
						{if $controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 1}
						</div>
						<div class="nav_btn_right">&nbsp;</div>
						{/if}
					</div>
					{if !($controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 1)}
					<div class="spatia">&nbsp;</div>
					{/if}

					<div class="{if $controllerRun EQ "tournaments"}nav_btn_active{else}nav_btn{/if}">
						{if $controllerRun EQ "tournaments"}
							<div class="nav_btn_left">&nbsp;</div>
							<div class="nav_btn_top">
						{/if}
						<a href="/tournaments">Турнири<!--({$tournamentsCount})--></a>
						{if $controllerRun EQ "tournaments"}
							</div>
							<div class="nav_btn_right">&nbsp;</div>
						{/if}
					</div>
					{if !($controllerRun EQ "tournaments")}
						<div class="spatia">&nbsp;</div>
					{/if}

					<div class="{if $controllerRun EQ "users"}nav_btn_active{else}nav_btn{/if}">
						{if $controllerRun EQ "users"}
							<div class="nav_btn_left">&nbsp;</div>
							<div class="nav_btn_top">
						{/if}
						<a href="/users/all">Потребители</a>
						{if $controllerRun EQ "users"}
							</div>
							<div class="nav_btn_right">&nbsp;</div>
						{/if}
					</div>
					{if !($controllerRun EQ "users")}
						<div class="spatia">&nbsp;</div>
					{/if}

					<div class="{if $controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 3}nav_btn_active{else}nav_btn{/if}">
						{if $controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 3}
						<div class="nav_btn_left">&nbsp;</div>
						<div class="nav_btn_top">
						{/if}
							<a href="/static/index/3" title="За нас">За нас</a>
						{if $controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 3}
						</div>
						<div class="nav_btn_right">&nbsp;</div>
						{/if}
					</div>
					{if !($controllerRun EQ "static" && $methodRun EQ "index" && $static.id EQ 3)}
					<div class="spatia">&nbsp;</div>
					{/if}

					<div class="{if $controllerRun EQ "main" && $methodRun EQ "contacts"}nav_btn_active{else}nav_btn{/if}">
						{if $controllerRun EQ "main" && $methodRun EQ "contacts"}
						<div class="nav_btn_left">&nbsp;</div>
						<div class="nav_btn_top">
						{/if}
							<a href="/main/contacts" title="Контакти">Контакти</a>
						{if $controllerRun EQ "main" && $methodRun EQ "contacts"}
						</div>
						<div class="nav_btn_right">&nbsp;</div>
						{/if}
					</div>
					{if !($controllerRun EQ "main" && $methodRun EQ "contacts") && !($smarty.session.UserID GT 0)}
					<div class="spatia">&nbsp;</div>
					{/if}
				</div>
			</div>