<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="/css/admin/table.css" type="text/css" media="print, projection, screen" />
	<link type="text/css" rel="stylesheet" href="/css/admin/checkbox/jquery.safari-checkbox.css" /> 
	<link type="text/css" rel="stylesheet" href="/css/admin/admin.css" />
	<link type="text/css" rel="stylesheet" href="/css/admin/redmond/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="/js/common_functions.js"></script>
	<script type="text/javascript" src="/js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="/js/admin/checkbox/jquery.checkbox.js"></script>
	<script type="text/javascript" src="/js/admin/checkbox/jquery.checkboxes.pack.js"></script>
	<script type="text/javascript" src="/js/admin/jquery.tablesort.js"></script>
	<script type="text/javascript" src="/js/admin/jquery.tablesorter.pager.js"></script>
	<script type="text/javascript" src="/js/admin/datetimepicker.js"></script>
<title>Aдминистрация {$SITENAME}</title>
</head>

<body>
	<h1>Администрация {$SITENAME}</h1>
	<a class="logout" href="/admin/main/logout">изход</a>
	<div class="clear small_height">&nbsp;</div>
	{include file='messages/admin_system_messages.tpl'}
	<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="tabs">
		<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
			<li class="{if $controllerRun EQ 'admin_main'}ui-tabs-selected ui-state-active{/if} ui-state-default ui-corner-top "><a class="" href="/admin/">Начало</a></li>
			<li class="{if $controllerRun EQ 'admin_users'}ui-tabs-selected ui-state-active{/if} ui-corner-top ui-state-default"><a class="" href="/admin/users">Потребители</a></li>
			<li class="{if $controllerRun EQ 'admin_tournaments'}ui-tabs-selected ui-state-active{/if} ui-corner-top ui-state-default"><a class="" href="/admin/tournaments">Турнири</a></li>
			<li class="{if $controllerRun EQ 'admin_static'}ui-tabs-selected ui-state-active{/if} ui-corner-top ui-state-default"><a class="" href="/admin/static">Статични страници</a></li>
		</ul>
		
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ui-tabs-4"></div>
		
		<div id="body">
			{$body}	
		</div>
		
		<div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="ui-tabs-9">
			<br />
		</div>
	</div>
	
<script type="text/javascript">
{literal}
	$(function(){
	  $('input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
	})
{/literal}	
</script> 

</body>

</html>

